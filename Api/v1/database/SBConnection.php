<?
	
	function SBConnection(){

		$HOST = "localhost";
		
		$USER = "root";
		$PASSWORD = "";
		$DATABASE = "safebeat_db1";

		// $USER = "safebeat";
		// $PASSWORD = "Mcintyrest22";	
		// $DATABASE = "zadmin_safebeat";

		$DATABASE_CHARSET = "utf8";

		try {
		 
		  	# MySQL with PDO_MYSQL
		  	$sbdb = new PDO("mysql:host=$HOST;dbname=$DATABASE", $USER, $PASSWORD);

		 	$sbdb->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT );
			$sbdb->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
			$sbdb->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
			$sbdb->setAttribute( PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES utf8' );
			
		  	return $sbdb;
		 
		}
		catch(PDOException $e) {
		    echo $e->getMessage();
		}

		return;

	}

?>