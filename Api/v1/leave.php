<? 

/** 
	Method that retrieve all leave by user Id
*/
function GetLeaveByUser(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		//Fetch leave that are from 30 days ago and future
		$sql = "SELECT * FROM onLeave
				LEFT JOIN onLeaveType ON onLeave.leaveTypeId = onLeaveType.leaveTypeId
				LEFT JOIN entity ON onLeaveType.entityId = entity.entityId						
				WHERE timeTo > (UNIX_TIMESTAMP() - (86400 * 30)) AND onLeave.active = 1 
				ORDER BY onLeave.leaveId DESC";

		$query = sbexeculteQuery($sql);
		$exist = $query->rowCount();

		if($exist != 0){

			$entityList = getUserEntity($credential['userId']);
			$loop = array();
			$i = 0;
			
			while($fetch = $query->fetch()){
				
				$isPart = false;
				
				foreach ($entityList as $value) {
					if ($fetch->userId == $credential['userId'] || (is_null($fetch->userId) && $value['entityId'] == $fetch->entityId)) {
						$isPart= true;
						break;
					}
				}

				//If is part of entity
				if ($isPart) {
					
					//Entity info
					$arrayEntity = array(
									"entityId" => $fetch->entityId,
									"name" => $fetch->name,
									"adminId" => $fetch->adminId,
									"active" => $fetch->active
									);

					$loop[$i]['leaveId'] = $fetch->leaveId;
					$loop[$i]['userId'] = $fetch->userId;
					$loop[$i]['desc'] = $fetch->description;
					$loop[$i]['leaveType'] = $fetch->leaveTypeId;
					$loop[$i]['leaveTypeName'] = $fetch->leaveTypeName;
					$loop[$i]['timeFrom'] = $fetch->timeFrom;
					$loop[$i]['timeTo'] = $fetch->timeTo;
					$loop[$i]['timezone'] = $fetch->timezone;
					$loop[$i]['entityInfo'] = $arrayEntity;

					$i++;
				}
				
			}

			if (count($loop) != 0) {
				
				$array = array(
					"status" => '1',
					"msgStatus" => "success",
					"message" => "$msgLeaveFetchSuccess",
					"WSResponseCode" => "$WSCodeLeaveFetchSuccess",
					"leave" => $loop
				);	

				echo json_encode($array, JSON_PRETTY_PRINT);

			}else{
				$array = array(
					"status" => '1',
					"msgStatus" => "success",
					"message" => "$msgLeaveFetchNoLeave",
					"WSResponseCode" => "$WSCodeLeaveFetchNoLeave"
				);	

				echo json_encode($array, JSON_PRETTY_PRINT);
			}
					
		}else{

			$array = array(
				"status" => '1',
				"msgStatus" => "success",
				"message" => "$msgLeaveFetchNoLeave",
				"WSResponseCode" => "$WSCodeLeaveFetchNoLeave"
			);	

			echo json_encode($array, JSON_PRETTY_PRINT);	
		}

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}	
}

/** 
	Method that retrieve all leave by user Id
*/
function GetLeaveTypeByEntity(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$entityId = $_POST['entityId'];
		
		$sql = "SELECT * FROM onLeaveType WHERE entityId = '$entityId'";

		$query = sbexeculteQuery($sql);
		$exist = $query->rowCount();

		if($exist != 0){

			$loop = array();
			$i = 0;

			while($fetch = $query->fetch()){
			
				$loop[$i]['leaveTypeId'] = $fetch->leaveTypeId;
				$loop[$i]['leaveTypeName'] = $fetch->leaveTypeName;
				$loop[$i]['entityId'] = $fetch->entityId;

				$i++;				
			}

			$array = array(
				"status" => '1',
				"msgStatus" => "success",
				"message" => "$msgLeaveTypeFetchSuccess",
				"WSResponseCode" => "$WSCodeLeaveTypeFetchSuccess",
				"leaveType" => $loop

			);	

			echo json_encode($array, JSON_PRETTY_PRINT);

					
		}else{
			
			$array = array(
				"status" => '1',
				"msgStatus" => "success",
				"message" => "$msgLeaveTypeFetchNoLeaveType",
				"WSResponseCode" => "$WSCodeLeaveTypeFetchNoLeaveType"
			);	

			echo json_encode($array, JSON_PRETTY_PRINT);	
		}

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}	
}


/** 
	Method to save leave
*/
function SaveLeave(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$data = array(
			"userId" => $_POST['userId'],
			"description" => $_POST['description'],
			"leaveType" => $_POST['leaveType'],
			"timeFrom" => $_POST['timeFrom'],
			"timeTo" => $_POST['timeTo'],
			"timezone" => $_POST['tz']
			);
		
		$sql = "INSERT INTO onLeave (userId,description,leaveTypeId,timeFrom,timeTo,timezone) 
				VALUES (:userId,:description,:leaveType,:timeFrom,:timeTo,:timezone)";
		$query = sbexeculteQueryWithData($sql,$data);
		
		if ($query) {
			
			$array = array(
				"status" => '1',
				"msgStatus" => "success",
				"message" => "$msgLeaveSaveSuccess",
				"WSResponseCode" => "$WSCodeLeaveSaveSuccess"
			);
		
			echo json_encode($array, JSON_PRETTY_PRINT);		
		
		}else{
			
			$array = array(
				"status" => '0',
				"msgStatus" => "fail",
				"message" => "$msgLeaveSaveFail",
				"WSResponseCode" => "$WSCodeLeaveSaveFail"
			);

			echo json_encode($array, JSON_PRETTY_PRINT);	
		}

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}	
}


/** 
	Method to update leave
*/
function UpdateLeave(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$data = array(
			"leaveId" => $_POST['leaveId'],
			"description" => $_POST['description'],
			"leaveType" => $_POST['leaveType'],
			"timeFrom" => $_POST['timeFrom'],
			"timeTo" => $_POST['timeTo'],
			"timezone" => $_POST['tz']
			);
		
		$sql = "UPDATE onLeave SET description = :description, leaveTypeId = :leaveType, timeFrom = :timeFrom, timeTo = :timeTo
				WHERE leaveId = :leaveId";
		$query = sbexeculteQueryWithData($sql,$data);
		
		if ($query) {
			
			$array = array(
				"status" => '1',
				"msgStatus" => "success",
				"message" => "$msgLeaveUpdateSuccess",
				"WSResponseCode" => "$WSCodeLeaveUpdateSuccess"
			);
		
			echo json_encode($array, JSON_PRETTY_PRINT);		

		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => "fail",
				"message" => "$msgLeaveUpdateFail",
				"WSResponseCode" => "$WSCodeLeaveUpdateFail"
			);

			echo json_encode($array, JSON_PRETTY_PRINT);	
		}

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}	
}


/** 
	Method to update leave
*/
function DeleteLeave(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$leaveId = $_POST['leaveId'];

		$sql = "UPDATE onLeave SET active = 0 WHERE leaveId = '$leaveId'";
		$query = sbexeculteQuery($sql);
		
		if ($query) {
			
			$array = array(
				"status" => '1',
				"msgStatus" => "success",
				"message" => "$msgLeaveDeleteSuccess",
				"WSResponseCode" => "$WSCodeLeaveDeleteSuccess"
			);
		
			echo json_encode($array, JSON_PRETTY_PRINT);		
		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => "fail",
				"message" => "$msgLeaveDeleteFail",
				"WSResponseCode" => "$WSCodeLeaveDeleteFail"
			);

			echo json_encode($array, JSON_PRETTY_PRINT);	
		}

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}	
}

?>