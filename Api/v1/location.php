<?php

/** 
Method that will checkin user
*/
function SaveLocation(){

	require 'lang.php';
	require "WSResponseCode.php";

	//Load required files
	require '../notification_v1/notification.php';
	require '../notification_v1/functions.php';

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$entityId = $_POST['entityId'];

		$data = array(
			"userId" => $_POST['userId'],
			"userStatus" => $_POST['userStatus'],
			"actionId" => $_POST['actionId'],
			"siteId" => $_POST['siteId'],
			"lat" => $_POST['lat'],
			"lng" => $_POST['lng'],
			"batteryLevel" => $_POST['bl'],
			"time" => GetCurrentTimeStamp(),
			"timezone" => $_POST['tz'],
			"leftSite" => $_POST['leftSite']
			);
			
		$sql = "INSERT INTO location (userId,userStatus,actionId,siteId,lat,lng,batteryLevel,time,timezone,leftSite) 
				VALUES (:userId,:userStatus,:actionId,:siteId,:lat,:lng,:batteryLevel,:time,:timezone,:leftSite)";
		
		$query = sbexeculteQueryWithDataReturnId($sql,$data);

		//////////////////////// BATTERY HANDLER ////////////////////////
		//If battery level is less than 10 percent send push to user only
		//If battery level is less than 8 percent send push to user and team leaders
		if ($data['batteryLevel'] < 11 && $data['batteryLevel'] > 7) {
			
			//Check if push hasn't been sent to user before to avoid sending multiple alert to user with same battery level
			$lastPush = GetUserLastPushForCategory($data['actionId'],6);
			
			if (!$lastPush) {
				
				$userDetail = GetUserInfo($data['userId']);
				$message = $userDetail->firstName . " please perform an action to ensure you are safe, your battery is bellow 10%.";

				//Send push to user only alerting about low battery
				PushLocationBatteryBellow10($userDetail,$message,$data['actionId'],$entityId,$data['time'],$data['timezone']);	
			}

		}else if ($data['batteryLevel'] < 8) {

			//Check if push hasn't been sent to user before to avoid sending multiple alert to user with same battery level
			// 6 - represent the notification category
			// 41 - represent the notiTypeId
			$lastPush = GetUserLastPushForCategoryAndNotiType($data['actionId'],6,41);
			
			if (!$lastPush) {
				
				$userDetail = GetUserInfo($data['userId']);
				$message = $userDetail->firstName . " please perform an action to ensure you are safe, your battery is bellow 8%.";

				//Send push to user and his team leader alerting about low battery
				PushLocationBatteryBellow8($userDetail,$message,$data['actionId'],$entityId,$data['time'],$data['timezone']);
			}
			

		}

		//////////////////////// SITE RADIUS HANDLER ////////////////////////
		//Handle user left site radius without checkin out
		//If user left site, the variable onSite will be false and send push to user asking him to checkout
		if ($data['leftSite'] == 1) {
			
			//Check if push hasn't been sent to user before to avoid multiple alerts
			$lastPush = GetUserLastPushForCategory($data['actionId'],6);
			
			if (!$lastPush) {
				
				if (!$userDetail) {
					$userDetail = GetUserInfo($data['userId']);
				}
				
				$message = $userDetail->firstName . " it seems that you left the site radius, please perform a check out";

				//Send push to user only alerting about him left the site wihtout checkout
				PushLocationLeftSiteRadius($userDetail,$message,$data['actionId'],$entityId,$data['time'],$data['timezone']);
			}

		}

		if($query){
								
			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgLocationSaveSuccess",
				"WSResponseCode" => "$WSCodeLocationSaveSuccess"
			);
			
		}else{

			$array = array(
				"status" => '0',
				"msgStatus" => "fail",
				"message" => "$msgLocationSaveFail",
				"WSResponseCode" => "$WSCodeLocationSaveFail"
			);

		}	
		
	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => "fail",
			"message" => "$msgLocationSaveFail",
			"WSResponseCode" => "$WSCodeLocationSaveFail"
		);

	}

	echo json_encode($array, JSON_PRETTY_PRINT);

}


?>