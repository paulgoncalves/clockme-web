<?


/** 
	Method that retrieve all notification for user
*/
function GetUserNotification(){
		
	require 'lang.php';
	require "WSResponseCode.php";
	
	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$entityId = $_POST['entityId'];
		
		if (isset($_POST['notificationId'])) {
			
			$notificationId = $_POST['notificationId'];

			//Get notifications that are not message and man down
			//The union will fetch all man down alarm made in the last 7 days
			//REMOVED: GROUP BY nf.notiRef
			$sql = "SELECT noti.notiAlertId,noti.userId,noti.notificationId,noti.message,noti.success,
					nf.notificationId,nf.resolved,nf.notiType,nf.notiRef,nf.entityId,nf.time,nf.timezone,nf.resolved,nf.resolvedBy,nf.resolvedTime,nf.resolvedTimezone,
					nt.notiTypeId,nt.category
					FROM (SELECT ntg.notiAlertId,ntg.userId,ntg.notificationId,ntg.message,ntg.success FROM notiAlert AS ntg
							INNER JOIN notification AS nfj ON nfj.notificationId = ntg.notificationId
							WHERE ntg.userId = ". $credential['userId']." AND ntg.notificationId < '$notificationId'
							AND (nfj.notiType <> 60 AND nfj.notiType <> 61 AND nfj.notiType <> 90 AND nfj.notiType <> 91) 
							ORDER BY ntg.notiAlertId DESC) as noti
					LEFT JOIN notification AS nf ON noti.notificationId = nf.notificationId
					LEFT JOIN notiType AS nt ON nf.notiType = nt.notiTypeId
					ORDER BY noti.notiAlertId DESC
					LIMIT 20";

		}else{
			
			//Get notifications that are not message and man down
			//The union will fetch all man down alarm made in the last 7 days
			//REMOVED: GROUP BY nf.notiRef
			$sql = "(SELECT noti.notiAlertId,noti.userId,noti.notificationId,noti.message,noti.success,
					nf.notificationId,nf.resolved,nf.notiType,nf.notiRef,nf.entityId,nf.time,nf.timezone,nf.resolved,nf.resolvedBy,nf.resolvedTime,nf.resolvedTimezone,
					nt.notiTypeId,nt.category
						FROM (SELECT ntg.notiAlertId,ntg.userId,ntg.notificationId,ntg.message,ntg.success FROM notiAlert AS ntg
								INNER JOIN notification AS nfj ON nfj.notificationId = ntg.notificationId
								WHERE ntg.userId = ".$credential['userId']." AND nfj.resolved = 1
								AND (nfj.notiType <> 60 AND nfj.notiType <> 61 AND nfj.notiType <> 90 AND nfj.notiType <> 91) 
								ORDER BY ntg.notiAlertId DESC) as noti
						LEFT JOIN notification AS nf ON noti.notificationId = nf.notificationId
						LEFT JOIN notiType AS nt ON nf.notiType = nt.notiTypeId
						ORDER BY noti.notiAlertId DESC
						LIMIT 20
					)

					UNION

					(SELECT noti1.notiAlertId,noti1.userId,noti1.notificationId,noti1.message,noti1.success,
					nf1.notificationId,nf1.resolved,nf1.notiType,nf1.notiRef,nf1.entityId,nf1.time,nf1.timezone,nf1.resolved,nf1.resolvedBy,nf1.resolvedTime,nf1.resolvedTimezone,
					nt1.notiTypeId,nt1.category FROM notiAlert as noti1 
						LEFT JOIN notification AS nf1 ON noti1.notificationId = nf1.notificationId
						LEFT JOIN notiType AS nt1 ON nf1.notiType = nt1.notiTypeId
						WHERE noti1.userId = ".$credential['userId']." AND (nf1.notiType = 90 OR nf1.notiType = 91) AND (nf1.time > (UNIX_TIMESTAMP() - 604800))
						ORDER BY noti1.notiAlertId DESC
					)

					UNION

					(SELECT noti.notiAlertId,noti.userId,noti.notificationId,noti.message,noti.success,
					nf.notificationId,nf.resolved,nf.notiType,nf.notiRef,nf.entityId,nf.time,nf.timezone,nf.resolved,nf.resolvedBy,nf.resolvedTime,nf.resolvedTimezone,
					nt.notiTypeId,nt.category
						FROM (SELECT ntg.notiAlertId,ntg.userId,ntg.notificationId,ntg.message,ntg.success FROM notiAlert AS ntg
								INNER JOIN notification AS nfj ON nfj.notificationId = ntg.notificationId
								WHERE ntg.userId = ".$credential['userId']." AND nfj.resolved = 0
								AND (nfj.notiType <> 60 AND nfj.notiType <> 61 AND nfj.notiType <> 90 AND nfj.notiType <> 91) 
								ORDER BY ntg.notiAlertId DESC) as noti
						LEFT JOIN notification AS nf ON noti.notificationId = nf.notificationId
						LEFT JOIN notiType AS nt ON nf.notiType = nt.notiTypeId
						ORDER BY noti.notiAlertId DESC
					)";
		}
	
		$query = sbexeculteQuery($sql);
		$row = $query->rowCount();	
		
		if($row != 0){

			$i = 0;
			$loop = array();
			$arrayNotiRef = array();

			while ($fetchNoti = $query->fetch()) {
				
				//Loop to allow only 2 push with same notiRef
				$notiRefCount = 0;
				foreach ($arrayNotiRef as $notiRefValue) {
					if ($notiRefValue == $fetchNoti->notiRef || $fetchNoti->notiRef == 0) {
						$notiRefCount++;
					}
				}

				//Only let populate the values load twice
				if ($notiRefCount < 2) {
					
					//Push notiRef to array
					//array_push($arrayNotiRef, $fetchNoti->notiRef);

					$queryNoti = getUserNotificationByCategory($credential['userId'],$fetchNoti->notiAlertId,$fetchNoti->category);
					$fetch = $queryNoti->fetch();

					//If Morning note
					if ($fetchNoti->notiType == 20) {
						
						$loop[$i]['siteName'] = $fetch->entityName;
						$loop[$i]['userName'] = "Morning notification";
						$loop[$i]['entityId'] = $fetch->idEntity;
					
					}else if($fetchNoti->category == 9){

						//If category is hazard or man down there is no link with site name
						$loop[$i]['siteName'] = $fetch->entityName;
						$loop[$i]['userName'] = $fetch->firstName .' '. $fetch->lastName;
						$loop[$i]['entityId'] = $fetch->entityId;

					}else if($fetchNoti->category == 5){

						//Hazard related to action
						if ($fetchNoti->notiType == 31 || $fetchNoti->notiType == 33) {
							$loop[$i]['siteName'] = $fetch->entityName;						
						}else{
							$loop[$i]['siteName'] = $fetch->siteName;	
						}

						$loop[$i]['userName'] = $fetch->firstName .' '. $fetch->lastName;
						$loop[$i]['entityId'] = $fetch->entityId;

					}else{

						$loop[$i]['siteName'] = $fetch->siteName;
						$loop[$i]['userName'] = $fetch->firstName .' '. $fetch->lastName;
						$loop[$i]['entityId'] = $fetch->entityId;

					}

					if ($fetchNoti->category != 5) {
						$loop[$i]['notiRef'] = $fetch->notiRef;
					}else{
						$loop[$i]['notiRef'] = $fetch->hazardId;
					}

					//VERY IMPORTANT, this will ignore the push
					//If broken reference, ignore push
					if (!$loop[$i]['notiRef']) {
						continue;
					}

					$loop[$i]['notificationId'] = $fetch->notificationId;
					$loop[$i]['notiAlertId'] = $fetch->notiAlertId;
					$loop[$i]['userId'] = $fetch->notiUser;
					$loop[$i]['userEmail'] = $fetch->email;
					$loop[$i]['userStatus'] = $fetch->status;
					$loop[$i]['mobile'] = $fetch->mobile;
					$loop[$i]['time'] = $fetch->notiTime;
					$loop[$i]['lastReport'] = $fetch->lastReport;
					$loop[$i]['notiType'] = $fetch->notiType;
					$loop[$i]['message'] = $fetch->message;
					$loop[$i]['description'] = $fetch->description;
					$loop[$i]['type'] = $fetch->type;
					$loop[$i]['pushSuccess'] = $fetch->success;
					$loop[$i]['entityName'] = $fetch->entityName;
					$loop[$i]['timezone'] = $fetch->timezone;

					if (isset($fetch->batteryLevel)) {
						$loop[$i]['batteryLevel'] = $fetch->batteryLevel;	
					}

					if ($fetchNoti->notiType != 20) {

						if ($fetchNoti->resolved == 1) {
							
							//Get the person that resolved the notification detail
							$queryResolve = GetUserInfo($fetchNoti->resolvedBy);
							
							if ($queryResolve) {
								$resolvedBy = array(
										"userId" => $fetch->resolvedBy,
										"firstName" => $queryResolve->firstName,
										"lastName" => $queryResolve->lastName,
										"email" => $queryResolve->email,
										"mobile" => $queryResolve->mobile,
										"status" => $queryResolve->status,
										"active" => $queryResolve->active
									);	

								$loop[$i]['userDetail'] = $resolvedBy;
							}
							
							if ($fetchNoti->notiType != 90 && $fetchNoti->notiType != 91) {
								$loop[$i]['time'] = $fetch->lastReport;	
							}
							
							$loop[$i]['resolved'] = $fetch->resolved;
							$loop[$i]['resolvedBy'] = $fetch->resolvedBy;
							$loop[$i]['resolvedTime'] = $fetch->resolvedTime;
							$loop[$i]['resolvedTimezone'] = $fetch->resolvedTimezone;

						}else{
							
							//Not resolved
							$loop[$i]['resolved'] = "0";
							$loop[$i]['resolvedBy'] = "";
							$loop[$i]['resolvedTime'] = "";
							$loop[$i]['resolvedTimezone'] = "";

						}

					}else{

						$loop[$i]['time'] = $fetch->notiTime;
						$loop[$i]['resolved'] = $fetch->resolved;
						$loop[$i]['resolvedBy'] = $fetch->resolvedBy;
						$loop[$i]['resolvedTime'] = $fetch->resolvedTime;
						$loop[$i]['resolvedTimezone'] = $fetch->resolvedTimezone;
							
					}

					$i++;

				}
				
			}				
		}

		if (isset($entityId)) {
			$hazard = GetHazardByEntity($entityId);
		}

		if (count($hazard) != 0) {
			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgNotificationSuccess",
				"WSResponseCode" => "$WSCodeNotificationSuccess",
				"notification" => $loop,
				"hazard" => $hazard
			);	

			echo json_encode($array, JSON_PRETTY_PRINT);

		}else if(count($loop)){

			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgNotificationSuccess",
				"WSResponseCode" => "$WSCodeNotificationSuccess",
				"notification" => $loop,
			);

			echo json_encode($array, JSON_PRETTY_PRINT);		

		}else{
			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgNotificationFail",
				"WSResponseCode" => "$WSCodeNotificationFail",
			);

			echo json_encode($array, JSON_PRETTY_PRINT);
		}
			
	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}

/**
	Method to clean all notification for user
*/
function ClearAllNotification(){

	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {
		
		$sql = "UPDATE notiAlert SET success = 1 WHERE userId = ".$credential['userId']."";
		$query = sbexeculteQuery($sql); 
		
		if ($query) {
			
			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgNotificationClearAllSuccess",
				"WSResponseCode" => "$WSCodeNotificationClearAllSuccess"
			);
			
			echo json_encode($array, JSON_PRETTY_PRINT);	
			
		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgNotificationClearAllFail",
				"WSResponseCode" => "$WSCodeNotificationClearAllFail"
			);

			echo json_encode($array, JSON_PRETTY_PRINT);	
		}

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}

/**
	Method to clean all notification for user
*/
function ClearAllNotificationMessage(){

	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {
		
		$sql = "UPDATE notiAlert 
				INNER JOIN notification on notiAlert.notificationId = notification.notificationId
				INNER JOIN notiType on notification.notiType = notiType.notiTypeId
				SET success = 1
				WHERE notiAlert.userId = ".$credential['userId']." AND notiType.category = 7";
		$query = sbexeculteQuery($sql); 
		
		if ($query) {
			
			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgNotificationClearAllSuccess",
				"WSResponseCode" => "$WSCodeNotificationClearAllSuccess"
			);
			
			echo json_encode($array, JSON_PRETTY_PRINT);	
			
		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgNotificationClearAllFail",
				"WSResponseCode" => "$WSCodeNotificationClearAllFail"
			);

			echo json_encode($array, JSON_PRETTY_PRINT);	
		}

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}

/**
	Method to clear expecific notification
*/
function ClearNotification(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$notiAlertId = $_POST['notiAlertId'];
		
		$sql = "UPDATE notiAlert SET success = 1 WHERE userId = ".$credential['userId']." AND notiAlertId = '$notiAlertId'";
		$query = sbexeculteQuery($sql);

		if ($query) {
		
			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgNotificationClearSuccess",
				"WSResponseCode" => "$WSCodeNotificationClearSuccess"
			);
			
			echo json_encode($array, JSON_PRETTY_PRINT);	
			
		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgNotificationClearFail",
				"WSResponseCode" => "$WSCodeNotificationClearFail"
			);

			echo json_encode($array, JSON_PRETTY_PRINT);	
		}

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}

}

function getUserNotificationByCategory($userId,$notiAlertId,$category){


	if ($category == 1) {
		
		//Connections
		$sql = "SELECT *,
				user.userId AS notiUser,
				entity.name AS entityName,
				notification.time as notiTime,
				entity.name AS siteName,
				notification.time as lastReport,
				notification.time as notiTime,
				notification.entityId as idEntity
				FROM notiAlert
				LEFT JOIN notification on notiAlert.notificationId = notification.notificationId
				LEFT JOIN notiType on notification.notiType = notiType.notiTypeId
				LEFT JOIN entity ON notification.entityId = entity.entityId 
				LEFT JOIN reportTo ON notification.notiRef = reportTo.reportToId
				LEFT JOIN user ON user.userId = 
					CASE
						WHEN notification.notiType <> 10 
							THEN notification.userId
						ELSE notiAlert.userId
					END
				WHERE notiAlert.notiAlertId = '$notiAlertId'";

	}else if($category == 2){

		//Push
		$sql = "SELECT *,
				user.userId AS notiUser,
				entity.name AS entityName,
				site.name as siteName,
				checkin.time as lastReport,
				notification.time as notiTime,
				notification.entityId as idEntity
				FROM notiAlert
				LEFT JOIN notification on notiAlert.notificationId = notification.notificationId
				LEFT JOIN notiType on notification.notiType = notiType.notiTypeId
				LEFT JOIN entity ON notification.entityId = entity.entityId 
				LEFT JOIN checkin ON notification.notiRef = checkin.checkinId
				LEFT JOIN site on checkin.siteId = site.siteId
				LEFT JOIN user ON user.userId = 
					CASE
						WHEN notification.notiType <> 10 
							THEN notification.userId
						ELSE notiAlert.userId
					END
				WHERE notiAlert.notiAlertId = '$notiAlertId'";

	}else if($category == 3){

		//Sunrise
		$sql = "SELECT *,
				user.userId AS notiUser,
				entity.name AS entityName,
				site.name as siteName,
				checkin.time as lastReport,
				notification.time as notiTime,
				notification.entityId as idEntity
				FROM notiAlert
				LEFT JOIN notification on notiAlert.notificationId = notification.notificationId
				LEFT JOIN notiType on notification.notiType = notiType.notiTypeId
				LEFT JOIN entity ON notification.entityId = entity.entityId 
				LEFT JOIN checkin ON notification.notiRef = checkin.checkinId
				LEFT JOIN site on checkin.siteId = site.siteId
				LEFT JOIN user ON user.userId = 
					CASE
						WHEN notification.notiType <> 10 
							THEN notification.userId
						ELSE notiAlert.userId
					END
				WHERE notiAlert.notiAlertId = '$notiAlertId'";
		
	}else if($category == 4){

		//Broadcast
		$sql = "SELECT *,
				user.userId AS notiUser,
				entity.name AS entityName,
				site.name as siteName,
				checkin.time as lastReport,
				notification.time as notiTime,
				notification.entityId as idEntity
				FROM notiAlert
				LEFT JOIN notification on notiAlert.notificationId = notification.notificationId
				LEFT JOIN notiType on notification.notiType = notiType.notiTypeId
				LEFT JOIN entity ON notification.entityId = entity.entityId 
				LEFT JOIN checkin ON notification.notiRef = checkin.checkinId
				LEFT JOIN site on checkin.siteId = site.siteId
				LEFT JOIN user ON user.userId = 
					CASE
						WHEN notification.notiType <> 10 
							THEN notification.userId
						ELSE notiAlert.userId
					END
				WHERE notiAlert.notiAlertId = '$notiAlertId'";
		
	}else if($category == 5){

		//Hazard
		$sql = "SELECT *,
				user.userId AS notiUser,
				entity.name AS entityName,
				site.name AS siteName,
				hazard.time AS lastReport,
				notification.time AS notiTime,
				notification.entityId AS idEntity
				FROM notiAlert
				LEFT JOIN notification ON notiAlert.notificationId = notification.notificationId
				LEFT JOIN notiType ON notification.notiType = notiType.notiTypeId
				LEFT JOIN hazardAction ON hazardAction.hazardActionId = notiAlert.notiRef
				LEFT JOIN hazard ON hazard.hazardId = 
					CASE
						WHEN notiType.notiTypeId = 30 OR notiType.notiTypeId = 32
							THEN notiAlert.notiRef
						ELSE hazardAction.hazardId
					END
				LEFT JOIN entity ON notification.entityId = entity.entityId 
				LEFT JOIN site ON hazard.siteId = site.siteId
				LEFT JOIN user ON user.userId = 
					CASE
						WHEN notification.notiType <> 10 
							THEN notification.userId
						ELSE notiAlert.userId
					END
				WHERE notiAlert.notiAlertId = '$notiAlertId'";
		
	}else if($category == 6){

		//Location background
		$sql = "SELECT *,
				user.userId AS notiUser,
				entity.name AS entityName,
				site.name as siteName,
				checkin.time as lastReport,
				notification.time as notiTime,
				notification.entityId as idEntity
				FROM notiAlert
				LEFT JOIN notification on notiAlert.notificationId = notification.notificationId
				LEFT JOIN notiType on notification.notiType = notiType.notiTypeId
				LEFT JOIN entity ON notification.entityId = entity.entityId 
				LEFT JOIN checkin ON notification.notiRef = checkin.checkinId
				LEFT JOIN site on checkin.siteId = site.siteId
				LEFT JOIN user ON user.userId = 
					CASE
						WHEN notification.notiType <> 10 
							THEN notification.userId
						ELSE notiAlert.userId
					END
				WHERE notiAlert.notiAlertId = '$notiAlertId'";
		
	}else if($category == 7){

		//Message
		$sql = "SELECT *,
				user.userId AS notiUser,
				entity.name AS entityName,
				message.createdTime as lastReport,
				notification.time as notiTime,
				notification.entityId as idEntity
				FROM notiAlert
				LEFT JOIN notification on notiAlert.notificationId = notification.notificationId
				LEFT JOIN notiType on notification.notiType = notiType.notiTypeId
				LEFT JOIN entity ON notification.entityId = entity.entityId 
				LEFT JOIN message ON notification.notiRef = message.messageId				
				LEFT JOIN user ON user.userId = 
					CASE
						WHEN notification.notiType <> 10 
							THEN notification.userId
						ELSE notiAlert.userId
					END
				WHERE notiAlert.notiAlertId = '$notiAlertId'";
		
	}else if($category == 9){

		//Man down
		$sql = "SELECT *,
				user.userId AS notiUser,
				entity.name AS entityName,
				manDown.time as lastReport,
				entity.name as siteName,
				notification.time as notiTime,
				notification.entityId as idEntity
				FROM notiAlert
				LEFT JOIN notification on notiAlert.notificationId = notification.notificationId
				LEFT JOIN notiType on notification.notiType = notiType.notiTypeId
				LEFT JOIN entity ON notification.entityId = entity.entityId 
				LEFT JOIN manDown ON notification.notiRef = manDown.manDownId
				LEFT JOIN user ON user.userId = 
					CASE
						WHEN notification.notiType <> 10 
							THEN notification.userId
						ELSE notiAlert.userId
					END
				WHERE notiAlert.notiAlertId = '$notiAlertId'";
		
	}else{

		//Other
		$sql = "SELECT *,
				user.userId AS notiUser,
				entity.name AS entityName,
				site.name as siteName,
				checkin.time as lastReport,
				notification.time as notiTime,
				notification.entityId as idEntity
				FROM notiAlert
				LEFT JOIN notification on notiAlert.notificationId = notification.notificationId
				LEFT JOIN notiType on notification.notiType = notiType.notiTypeId
				LEFT JOIN entity ON notification.entityId = entity.entityId 
				LEFT JOIN checkin ON notification.notiRef = checkin.checkinId
				LEFT JOIN site on checkin.siteId = site.siteId
				LEFT JOIN user ON user.userId = 
					CASE
						WHEN notification.notiType <> 10 
							THEN notification.userId
						ELSE notiAlert.userId
					END
				WHERE notiAlert.notiAlertId = '$notiAlertId'";
	}

	$query = sbexeculteQuery($sql);
	
	return $query;

}
?>