<?

/**
	Method get Questions
*/
function Question() {

	require 'lang.php';
	require "WSResponseCode.php";
	
	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$siteId = $_POST['siteId'];
		$entityId = $_POST['entityId'];
		
		//Look for questions related to that site
		$sql = "SELECT * FROM questions 
				WHERE siteId = '$siteId' OR entityId = '$entityId'";
		$query = sbexeculteQuery($sql);
		$row = $query->rowCount();
		
		if($row != 0){

			$i = 0;
			$loop = array();
			$currTime = GetCurrentTimeStamp();
			$hasParent = FALSE;

			while($fetch = $query->fetch()){
				
				//Check if user has reply to question on the required length
				if (NeedAnswerQuestion($fetch->questionId,$credential['userId'],$currTime,$fetch->intervalTime)) {
					
					$loop[$i]['questionId'] = $fetch->questionId;
					$loop[$i]['question'] = $fetch->question;
					$loop[$i]['childYes'] = $fetch->childYes;
					$loop[$i]['childNo'] = $fetch->childNo;
					$loop[$i]['safeAnswer'] = $fetch->safeAnswer;
					$loop[$i]['note'] = $fetch->note;
					$loop[$i]['canProceed'] = $fetch->canProceed;
					$loop[$i]['isParent'] = $fetch->isParent;
					$i++; 	

				} 				
			}
			
			//Check if any of the loaded questions includes a parent question
			//If no parent question, the user don't have to answers any question
			foreach ($loop as $arr) {
				if ($arr['isParent'] == 1) {
					$hasParent = TRUE;
				}
			}

			if ($hasParent) {
				
				$array = array(
					"status" => '1',
					"msgStatus" => 'success',
					"message" => "$msgQuestionSuccess",
					"WSResponseCode" => "$WSCodeQuestionSuccess",
					"question" => $loop
				);

				echo json_encode($array, JSON_PRETTY_PRINT);	

			}else{

				$array = array(
					"status" => '1',
					"msgStatus" => 'success',
					"message" => "$msgQuestionAswered",
					"WSResponseCode" => "$WSCodeQuestionAnswered"
				);

				echo json_encode($array, JSON_PRETTY_PRINT);
			}
			
		}else{

			$array = array(
					"status" => '0',
					"msgStatus" => 'fail',
					"message" => "$msgQuestionNoQuestion",
					"WSResponseCode" => "$WSCodeQuestionNoQuestion"
				);
			
			echo json_encode($array, JSON_PRETTY_PRINT);

		}
		
	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}


/**
	Method post all answers
*/
function Answer() {

	require 'lang.php';
	require "WSResponseCode.php";
	
	$inputJSON = file_get_contents('php://input');
	$arrayAnswer = json_decode($inputJSON, TRUE);
	$checkinId = $arrayAnswer['checkinId'];

	$credential = array(
					"userId" => $arrayAnswer['userId'],
					"token" => $arrayAnswer['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		if ($arrayAnswer['answers']) {
			
			//Insert into Answers table
			foreach ($arrayAnswer['answers'] as $obj) {
				$questionId = $obj['questionId'];
				$answer = $obj['answer'];

				sbexeculteQuery("INSERT INTO questionAnswers (questionId,checkinId,answer) VALUES ('$questionId','$checkinId','$answer')");
			}

			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgQuestionAnswerSuccess",
				"WSResponseCode" => "$WSCodeQuestionAnswerSuccess"
			);		
			
		}else{

			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgQuestionAnswerFail",
				"WSResponseCode" => "$WSCodeQuestionAnswerFail"
			);

		}
		
		echo json_encode($array, JSON_PRETTY_PRINT);

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}

?>