<?php

/** 
	Method that retrieve all the entity
*/
function GetAllEntity(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {
		
		$sql = "SELECT * FROM entity WHERE active <> 0";
		$query = sbexeculteQuery($sql);
		$exist = $query->rowCount();

		if($exist != 0){
			
			$i = 0;
			$loop = array();
			
			while($fetch = $query->fetch()){
				
				
				$loop[$i]['entityId'] = $fetch->entityId;
				$loop[$i]['entityName'] = $fetch->name;
				$loop[$i]['code'] = decryptStr($fetch->code);
				$loop[$i]['codeModified'] = $fetch->codeModified;
				$loop[$i]['adminId'] = $fetch->adminId;
				$loop[$i]['defaultGroup'] = $fetch->defaultGroup;
				$loop[$i]['active'] = $fetch->active;
				$i++;
			}

			$array = array(
				"status" => '1',
				"msgStatus" => "success",
				"message" => "$msgEntityFetchSuccess",
				"WSResponseCode" => "$WSCodeEntityFetchSuccess",
				"entity" => $loop
			);
			
			echo json_encode($array, JSON_PRETTY_PRINT);

		}else{

			$array = array(
				"status" => '0',
				"msgStatus" => "fail",
				"message" => "$msgEntityFetchFail",
				"WSResponseCode" => "$WSCodeEntityFetchFail"
			);

			echo json_encode($array, JSON_PRETTY_PRINT);	
		}

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}	
}

/** 
	Method that retrieve all the entity where user is part of
*/
function GetAllEntityUser(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {
		
		$sql = "SELECT *,entityMap.active as userEntityActive FROM entityMap 
				INNER JOIN entity on entityMap.entityId = entity.entityId 
				INNER JOIN setting ON entity.entityId = setting.entityId 
				WHERE entityMap.userId = ".$credential['userId']." AND entity.active <> 0";

		$query = sbexeculteQuery($sql);
		$exist = $query->rowCount();
		
		if($exist != 0){
			$i = 0;
			$loop = array();
			
			while($fetch = $query->fetch()){
				$loop[$i]['entityId'] = $fetch->entityId;
				$loop[$i]['entityName'] = $fetch->name;
				$loop[$i]['accessLevel'] = $fetch->accessLevel;
				$loop[$i]['reportTime'] = $fetch->notify1;
				$loop[$i]['emergencyNumber'] = $fetch->emergencyNumber;
				$loop[$i]['special'] = $fetch->special;
				$loop[$i]['active'] = $fetch->userEntityActive;

				if ($fetch->accessEnd) {
					$loop[$i]['accessEnd'] = $fetch->accessEnd;
				}else{
					$loop[$i]['accessEnd'] = "";
				}

				//Get entity addon
				$loop[$i]['addons'] = getEntityAddon($fetch->entityId);
				//Get entity checkin category
				$loop[$i]['checkinCategories'] = getEntityCheckinCategory($fetch->entityId);
				//Get entity pulses
				$loop[$i]['pulses'] = getEntityPulse($fetch->entityId);

				$i++;
			}
			
			$array = array(
				"status" => '1',
				"msgStatus" => "success",
				"message" => "$msgEntityUserFetchSuccess",
				"WSResponseCode" => "$WSCodeEntityUserFetchSuccess",
				"entity" => $loop
			);
		
			echo json_encode($array, JSON_PRETTY_PRINT);

		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => "fail",
				"message" => "$msgEntityUserFetchFail",
				"WSResponseCode" => "$WSCodeEntityUserFetchFail"
			);

			echo json_encode($array, JSON_PRETTY_PRINT);	
		}

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}	
}

/** 
	Method that retrieve the entity by id
*/
function GetEntityById(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$entityId = $_POST['entityId'];
		
		$sql = "SELECT * FROM entityMap
				INNER JOIN entity ON entityMap.entityId = entity.entityId
				INNER JOIN setting ON entity.entityId = setting.entityId 
				WHERE entityMap.entityId = '$entityId' AND entity.active = '1' AND entityMap.userId = ".$credential['userId']."";
		
		$query = sbexeculteQuery($sql);
		$row = $query->rowCount();
		
		if($row != 0){
			
			$loop = array();
			$fetch = $query->fetch();

			$loop['entityId'] = $fetch->entityId;
			$loop['entityName'] = $fetch->name;
			$loop['accessLevel'] = $fetch->accessLevel;
			$loop['reportTime'] = $fetch->notify1;
			$loop['emergencyNumber'] = $fetch->emergencyNumber;
			$loop['special'] = $fetch->special;

			if ($fetch->accessEnd) {
				$loop['accessEnd'] = $fetch->accessEnd;
			}else{
				$loop['accessEnd'] = "";
			}

			//Get entity addon
			$loop['addons'] = getEntityAddon($entityId);
			//Get entity checkin category
			$loop['checkinCategories'] = getEntityCheckinCategory($entityId);
			//Get entity pulses
			$loop['pulses'] = getEntityPulse($entityId);
						
			$array = array(
				"status" => '1',
				"msgStatus" => "success",
				"message" => "$msgEntityUserFetchSuccess",
				"WSResponseCode" => "$WSCodeEntityUserFetchSuccess",
				"entity" => $loop
			);
		
			echo json_encode($array, JSON_PRETTY_PRINT);		
		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => "fail",
				"message" => "$msgEntityUserFetchFail",
				"WSResponseCode" => "$WSCodeEntityUserFetchFail"
			);

			echo json_encode($array, JSON_PRETTY_PRINT);	
		}

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}	
}


/**
	Method that joing an entity
*/
function JoinEntity(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$timestamp = $_POST['ts'];

		$data = array(
			"entityId" => $_POST['entityId'],
			"userId" => $_POST['userId'],
			"accessLevel" => 3,
			"groupId" => $_POST['groupId']
			);
		
		$canJoin = false;

		$sql = "SELECT * FROM entityMap 
				WHERE entityId = ".$data['entityId']." AND userId = ".$data['userId']." AND (accessEnd > '$timestamp' OR accessEnd IS NULL)";
		
		$query = sbexeculteQuery($sql);
		$row = $query->rowCount();

		if ($row == 0) {
			
			$sqlEntity = "	SELECT * FROM setting 
							INNER JOIN package ON setting.packageId = package.pkgId 
							WHERE setting.entityId = ".$data['entityId']."";

			$queryEntity = sbexeculteQuery($sqlEntity);
			$fetch = $queryEntity->fetch();
			
			//Check entity user limit
			if ($fetch->userLimit == null || EntityUserLimit($data['entityId'],$fetch->userLimit,$timestamp)) {
				
				$canJoin = true;

			}else{			

				//check entity auto update
				if ($fetch->autoUpgrade == 1) {
					
					//Upgrade teh entity
					UpgradeEntity($data['entityId'],$data['userId'],$fetch->pkgOrder + 1);
					$canJoin = true;

					//Save log to describe what happen
					WriteToAccountLog($data['entityId'],$data['userId'],$logEntityUpdateByUser);

				}

			}

			if ($canJoin) {
				
				//The access end for now it's set to never expire, it will be null en accessEnd
				$sqlJoin = "INSERT INTO entityMap (entityId,userId,accessLevel,groupId) 
							VALUES (:entityId,:userId,:accessLevel,:groupId) ";
				$querJoin = sbexeculteQueryWithData($sqlJoin,$data);

				if ($querJoin) {
					
					$array = array(
						"status" => '1',
						"msgStatus" => 'success',
						"message" => "$msgEntityJoinSuccess",
						"WSResponseCode" => "$WSCodeEntityJoinSuccess",
						"entityId" => $fetch->entityId
					);

				}else{

					$array = array(
						"status" => '0',
						"msgStatus" => "fail",
						"message" => "$msgEntityJoinFail",
						"WSResponseCode" => "$WSCodeEntityJoinFail"
					);
				}
					
			}else{
				$array = array(
					"status" => '0',
					"msgStatus" => 'fail',
					"message" => "$msgEntityJoinFailUpgrade",
					"WSResponseCode" => "$WSCodeEntityJoinFailUpgrade"
				);		
			}
			
		}else{
		
			$array = array(
				"status" => '0',
				"msgStatus" => "fail",
				"message" => "$msgEntityJoinFail",
				"WSResponseCode" => "$WSCodeEntityJoinFail"
			);

		}
		
		echo json_encode($array, JSON_PRETTY_PRINT);

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}




/**
	Method tha Delete Entity from user list
*/
function DeleteUserEntity(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$entityId = $_POST['entityId'];
		$timestamp = $_POST['ts'];
		
		$sql = "UPDATE entityMap SET accessEnd = '$timestamp', active = 2 WHERE entityId = '$entityId' AND userId = ".$credential['userId']."";
		$query = sbexeculteQuery($sql);

		if ($query) {
			
			$array = array(
				"status" => '1',
				"msgStatus" => "success",
				"message" => "$msgEntityDeleteSuccess",
				"WSResponseCode" => "$WSCodeEntityDeleteSuccess"
			);

		}else{
		
			$array = array(
				"status" => '0',
				"msgStatus" => "fail",
				"message" => "$msgEntityDeleteFail",
				"WSResponseCode" => "$WSCodeEntityDeleteFail"
			);

		}
		
		echo json_encode($array, JSON_PRETTY_PRINT);

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}


?>
