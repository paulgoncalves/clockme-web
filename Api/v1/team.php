
<?

/**
	Method get my team
*/
function GetMyTeam() {

	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$userId = $_POST['userId'];
		$entityId = $_POST['entityId'];
		$accessLevel = $_POST['accessLevel'];
		
		//Administrator sees everyone on the company and Me as developer with id 2
		if ($accessLevel == 1 || $userId == 2) {
			$sql = "SELECT * FROM entityMap 
					INNER JOIN user ON entityMap.userId = user.userId
					WHERE entityMap.entityId = '$entityId' AND entityMap.userId <> '$userId' AND user.active = 1
					GROUP BY entityMap.userId";
		}else{

			$sql = "SELECT * FROM reportTo
					INNER JOIN user ON reportTo.followerId = user.userId
					INNER JOIN entityMap ON reportTo.followerId = entityMap.userId AND (entityMap.accessEnd IS NULL OR entityMap.accessEnd > UNIX_TIMESTAMP())
					WHERE leaderId = '$userId' AND reportTo.active = 1 AND reportTo.permission = 2
					AND user.active = 1
					GROUP BY followerId";
		}
		
		$query = sbexeculteQuery($sql);
		$row = $query->rowCount();
		
		if($row != 0){

			$loop = array();
			$arrayConnection = array();
			$i = 0;
			$x = 0;

			//Fetch leader id for user
			$sqlConnection = "	SELECT * FROM reportTo 
								WHERE (leaderId = '$userId' OR followerId = '$userId')
								AND active <> 0
								AND entityId = '$entityId' ";

			$queryConnection = sbexeculteQuery($sqlConnection);

			while ($fetchConnection = $queryConnection->fetch()) {
				
				$arrayConnection[$x]['reportToId'] = $fetchConnection->reportToId;
				$arrayConnection[$x]['followerId'] = $fetchConnection->followerId;
				$arrayConnection[$x]['leaderId'] = $fetchConnection->leaderId;
				$arrayConnection[$x]['requestBy'] = $fetchConnection->requestBy;
				$arrayConnection[$x]['permission'] = $fetchConnection->permission;
				$arrayConnection[$x]['active'] = $fetchConnection->active;
				$x++;
			}			

			while($fetch = $query->fetch()){
				
				$connection = array();
				$x = 0;

				//If user is admin or userId is Paul
				if ($accessLevel == 1) {
					
					$loop[$i]['entityId'] = $entityId;
					$loop[$i]['userId'] = $fetch->userId;
					$loop[$i]['email'] = $fetch->email;
					$loop[$i]['firstName'] = $fetch->firstName;
					$loop[$i]['lastName'] = $fetch->lastName;
					$loop[$i]['mobile'] = $fetch->mobile;
					$loop[$i]['accessLevel'] = $fetch->accessLevel;
					$loop[$i]['isConnected'] = "1";

					$connection[$x]['reportToId'] = "";
					$connection[$x]['followerId'] = $fetch->userId;
					$connection[$x]['leaderId'] = $userId;
					$connection[$x]['requestBy'] = $fetch->userId;
					$connection[$x]['permission'] = "2";
					$connection[$x]['active'] = $fetch->active;

					if (count($connection) != 0) {
						//Add connection detail
						$loop[$i]['connectionDetail'] = $connection;
					}
					
				}else{

					//Check if leaderId exist
					foreach ($arrayConnection as $arr) {
						
						if ($fetch->userId == $arr['leaderId'] || $fetch->userId == $arr['followerId']) {

							$connection[$x]['reportToId'] = $arr['reportToId'];
							$connection[$x]['followerId'] = $arr['followerId'];
							$connection[$x]['leaderId'] = $arr['leaderId'];
							$connection[$x]['requestBy'] = $arr['requestBy'];
							$connection[$x]['permission'] = $arr['permission'];
							$connection[$x]['active'] = $arr['active'];
							$x++;
						}
					}
					
					$loop[$i]['entityId'] = $entityId;
					$loop[$i]['userId'] = $fetch->userId;
					$loop[$i]['email'] = $fetch->email;
					$loop[$i]['firstName'] = $fetch->firstName;
					$loop[$i]['lastName'] = $fetch->lastName;
					$loop[$i]['mobile'] = $fetch->mobile;
					$loop[$i]['accessLevel'] = $fetch->accessLevel;
					$loop[$i]['isConnected'] = "1";
					$loop[$i]['pendingRequest'] = "0";

					if (count($connection) != 0) {
						//Add connection detail
						$loop[$i]['connectionDetail'] = $connection;
					}

				}
				
				$lastAction = UserLastLocation($fetch->userId);

				if ($lastAction) {
					
					$userLocation = array();
					
					//If user last action is diffent than entity id assign his status to 0
					if ($entityId == $lastAction->userEntityId && $lastAction->siteId != "") {
						
						if ($lastAction->isPulse == 1) {
								
							$siteName = "Pulse";
							$userLocation[0]['lat'] = "";
							$userLocation[0]['lng'] = "";
							$userLocation[0]['siteLat'] = "";
							$userLocation[0]['siteLng'] = "";

						}else if($lastAction->siteId != 0 || $lastAction->siteId != ""){
								
							$siteName = $lastAction->name;
							$userLocation[0]['lat'] = $lastAction->latitude;
							$userLocation[0]['lng'] = $lastAction->longitude;
							$userLocation[0]['siteLat'] = $lastAction->siteLat;
							$userLocation[0]['siteLng'] = $lastAction->siteLng;

						}

						$loop[$i]['status'] = $fetch->status;
						$userLocation[0]['name'] = $siteName;
						$userLocation[0]['time'] = $lastAction->time;
						$userLocation[0]['batteryLevel'] = $lastAction->batteryLevel;
						$userLocation[0]['checkinTypeId'] = $lastAction->checkinTypeId;
						$userLocation[0]['checkinTypeId'] = $lastAction->checkinTypeId;

					}else{
						
						//Get user last location for entity if his last aciton is not from entity
						$location = UserLastLocationByEntity($fetch->userId,$entityId);
						
						//If user last action is not same entity, set his status to 0
						//This basically mean he has switch entity and not longer need atention for currently entity
						$loop[$i]['status'] = 0;

						if ($location){
							
							if ($location->isPulse == 1) {
								
								$siteName = "Pulse";
								$userLocation[0]['lat'] = "";
								$userLocation[0]['lng'] = "";
								$userLocation[0]['siteLat'] = "";
								$userLocation[0]['siteLng'] = "";

							}else if($location->siteId != 0 || $location->siteId != ""){
								
								$siteName = $location->name;
								$userLocation[0]['lat'] = $location->latitude;
								$userLocation[0]['lng'] = $location->longitude;
								$userLocation[0]['siteLat'] = $location->siteLat;
								$userLocation[0]['siteLng'] = $location->siteLng;

							}

							$userLocation[0]['name'] = $siteName;
							$userLocation[0]['time'] = $location->time;
							$userLocation[0]['batteryLevel'] = $location->batteryLevel;
							$userLocation[0]['checkinTypeId'] = $location->checkinTypeId;
							$userLocation[0]['checkinTypeId'] = $location->checkinTypeId;

						}
					}

					//Add user location info
					$loop[$i]['userLocation'] = $userLocation;

				}

				$i++;

			}	
				
			$array = array(
				"status" => '1',
				"msgStatus" => "success",
				"message" => "$msgMyTeamSuccess",
				"WSResponseCode" => "$WSCodeMyTeamSuccess",
				"team" => $loop
			);

			echo json_encode($array, JSON_PRETTY_PRINT);

		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgMyTeamNoTeam",
				"WSResponseCode" => "$WSCodeMyTeamNoTeam"
			);
			
			echo json_encode($array, JSON_PRETTY_PRINT);
		}

		if (!$query) {
			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgMyTeamFail",
				"WSResponseCode" => "$WSCodeMyTeamFail"
			);
			
			echo json_encode($array, JSON_PRETTY_PRINT);	
		}

		
	}else{
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);
	}
	
}

/** 
	Method that retrieve all team leader by entity id
	Also retrieve my team leader and return if the leader is currently my leader
*/
function GetAllTeamLeader(){
		
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$entityId = $_POST['entityId'];
		$userId = $_POST['userId'];
		
		$sql = "SELECT * FROM entityMap 
				INNER JOIN user on entityMap.userId = user.userId 
				WHERE entityMap.entityId = '$entityId' 
				AND (entityMap.accessLevel = 2 OR entityMap.accessLevel = 1) 
				AND user.active = 1 AND entityMap.userId <> '$userId' 
				AND (accessEnd > UNIX_TIMESTAMP() OR accessEnd IS NULL)";

		$query = sbexeculteQuery($sql);
		$row = $query->rowCount();
		
		if($row != 0){
			
			$arrayLeader = array();
			$loop = array();
			$i = 0;

			//Fetch leader id for user
			$sqlLeader = "	SELECT * FROM reportTo 
							WHERE followerId = '$userId' OR leaderId = '$userId'
							AND (active = 1 OR active = 2)";

			$queryLeader = sbexeculteQuery($sqlLeader);

			while ($fetchLeader = $queryLeader->fetch()) {
				
				if ($fetchLeader->followerId == $userId) {
					$arrayLeader[$i]['leaderId'] = $fetchLeader->leaderId;	
				}else{
					$arrayLeader[$i]['leaderId'] = $fetchLeader->followerId;
				}
				
				$arrayLeader[$i]['permission'] = $fetchLeader->permission;
				$arrayLeader[$i]['active'] = $fetchLeader->active;
				$i++;
			}
			
			$i = 0;
			while($fetch = $query->fetch()){
				$loop[$i]['entityId'] = $fetch->entityId;
				$loop[$i]['userId'] = $fetch->userId;
				$loop[$i]['accessEnd'] = $fetch->accessEnd;
				$loop[$i]['firstName'] = $fetch->firstName;
				$loop[$i]['lastName'] = $fetch->lastName;
				$loop[$i]['email'] = $fetch->email;
				$loop[$i]['mobile'] = $fetch->mobile;
				$loop[$i]['active'] = $fetch->active;
				$loop[$i]['time'] = $fetch->time;
				$loop[$i]['isLeader'] = "0";
				$loop[$i]['pendingRequest'] = "0";

				foreach ($arrayLeader as $arr) {
					
					//Check if leaderId exist
					if ($fetch->userId == $arr['leaderId']) {
						
						//Check if user friend is pending
						if ($arr['active'] == 1) {
							$loop[$i]['pendingRequest'] = "0";					
						}else{
							$loop[$i]['pendingRequest'] = "1";
						}

						$loop[$i]['isLeader'] = "1";
						$loop[$i]['permission'] = $arr['permission'];

						break;	
						
					}else{
						$loop[$i]['isLeader'] = "0";
					}
				}
				

				$i++;
			}

			$array = array(
				"status" => '1',
				"msgStatus" => "success",
				"message" => "$msgMyTeamLeaderFetchSuccess",
				"WSResponseCode" => "$WSCodeMyTeamLeaderFetchSuccess",
				"teamLeader" => $loop
			);
			
			echo json_encode($array, JSON_PRETTY_PRINT);	
			
		}else{
			$array = array(
				"status" => 'fail',
				"msgStatus" => "fail",
				"message" => "$msgMyTeamLeaderNoLeader",
				"WSResponseCode" => "$WSCodeMyTeamLeaderNoLeader"
			);

			echo json_encode($array, JSON_PRETTY_PRINT);	
		}

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => "fail",
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}


/** 
	Method that retrieve all team leader
*/
function GetUserTeamLeader(){
		
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$userId = $_POST['userId'];
		$leaderFor = $_POST['leaderFor'];
		
		//Get who is the user following
		if ($leaderFor) {
			$query = GetFollowing($leaderFor);
		}else{

			$query = GetFollowing($userId);
	
			//Get who is following the user
			$queryFollower = GetFollowers($userId);
			$followers = $queryFollower->rowCount();
			
			//If quantity of followers is diferent then 0 create the array
			if ($followers != 0) {
	
				$loopFollower = array();
				$i = 0;

				while($fetch = $queryFollower->fetch()){
					$loopFollower[$i]['entityId'] = $fetch->entityId;
					$loopFollower[$i]['leaderId'] = $fetch->followerId;
					$loopFollower[$i]['firstName'] = $fetch->firstName;
					$loopFollower[$i]['lastName'] = $fetch->lastName;
					$loopFollower[$i]['email'] = $fetch->email;
					$loopFollower[$i]['mobile'] = $fetch->mobile;
					$loopFollower[$i]['active'] = $fetch->active;
					$loopFollower[$i]['time'] = $fetch->time;
					$loopFollower[$i]['permission'] = $fetch->permission;

					//Check if user friend is pending
					if ($fetch->request == 2) {
						$loopFollower[$i]['pendingRequest'] = "1";					
					}else{
						$loopFollower[$i]['pendingRequest'] = "0";
					}

					$i++;
				}
			}
			
		}	
		
		$row = $query->rowCount();
		
		if($row){
			$i = 0;
			
			$loop = array();
			
			while($fetch = $query->fetch()){
				$loop[$i]['entityId'] = $fetch->entityId;
				$loop[$i]['leaderId'] = $fetch->leaderId;
				$loop[$i]['firstName'] = $fetch->firstName;
				$loop[$i]['lastName'] = $fetch->lastName;
				$loop[$i]['email'] = $fetch->email;
				$loop[$i]['mobile'] = $fetch->mobile;
				$loop[$i]['active'] = $fetch->active;
				$loop[$i]['time'] = $fetch->time;
				$loop[$i]['permission'] = $fetch->permission;

				//Check if user friend is pending
				if ($fetch->request == 2) {
					$loop[$i]['pendingRequest'] = "1";					
				}else{
					$loop[$i]['pendingRequest'] = "0";
				}

				$i++;
			}

			$array = array(
				"status" => '1',
				"msgStatus" => "success",
				"message" => "$msgMyTeamLeaderFetchSuccess",
				"WSResponseCode" => "$WSCodeMyTeamLeaderFetchSuccess",
				"myTeamLeader" => $loop
			);
			
			//Get who is the user following
			if ($followers) {
				$array['myTeamFollower'] = $loopFollower;
				echo json_encode($array, JSON_PRETTY_PRINT);	
			}else{
				echo json_encode($array, JSON_PRETTY_PRINT);
			}
			
		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => "fail",
				"message" => "$msgMyTeamLeaderNoLeader",
				"WSResponseCode" => "$WSCodeMyTeamLeaderNoLeader"
			);

			echo json_encode($array, JSON_PRETTY_PRINT);
		}

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => "fail",
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}


?>