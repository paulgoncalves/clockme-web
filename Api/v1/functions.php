<?

/**
	///////////////////////////// TEAM FUNCTIONS /////////////////////////////
*/

/**
	Method to get user last latitude and longitude, also check if checkinTypeId different then 9
*/
function UserLastLocation($userId){
	
	$sql = "SELECT checkinId,time,timezone,
			checkin.lat as latitude,checkin.lng as longitude,checkin.checkinTypeId,
			site.lat as siteLat,site.lng as siteLng,site.siteId,
			userEntityId,checkinBy,batteryLevel,isPulse,name,userId 
			FROM checkin 
			INNER JOIN checkinType ON checkin.checkinTypeId = checkinType.checkinTypeId
			LEFT JOIN site ON checkin.siteId = site.siteId
			WHERE checkin.userId = '$userId' AND checkin.checkinBy = '$userId'
			ORDER BY checkin.checkinId 
			DESC LIMIT 1";
	
	$query = sbexeculteQuery($sql);

	if ($query) {
		return $query->fetch();
	}else{
		return false;
	}

}

/**
	Method to get user last latitude and longitude, also check if checkinTypeId different then 9
*/
function UserLastLocationByEntity($userId,$entityId){
	
	$sql = "SELECT checkinId,time,timezone,
			checkin.lat as latitude,checkin.lng as longitude,checkin.checkinTypeId,
			site.lat as siteLat,site.lng as siteLng,site.siteId,
			userEntityId,checkinBy,batteryLevel,isPulse,name,userId 
			FROM checkin 
			INNER JOIN checkinType ON checkin.checkinTypeId = checkinType.checkinTypeId
			LEFT JOIN site ON checkin.siteId = site.siteId
			WHERE checkin.userId = '$userId' AND checkin.userEntityId = '$entityId' AND checkin.checkinBy = '$userId'
			ORDER BY checkin.checkinId 
			DESC LIMIT 1";
	
	$query = sbexeculteQuery($sql);

	if ($query) {
		return $query->fetch();
	}else{
		return false;
	}

}

/**
	Method to get who user is following.
*/
function GetFollowing($userId){
	
	$sql = "SELECT *,reportTo.active AS request FROM reportTo 
			INNER JOIN user on reportTo.leaderId = user.userId 
			WHERE reportTo.followerId = '$userId' 
			AND user.active = 1 
			AND (reportTo.active = 1 OR reportTo.active = 2)";

	$query = sbexeculteQuery($sql);
	return $query;
}

/**
	Method to get who is following user.
*/
function GetFollowers($userId){

	$sql = "SELECT *,reportTo.active AS request FROM reportTo 
			INNER JOIN user on reportTo.followerId = user.userId 
			WHERE reportTo.leaderId = '$userId'
			AND user.active = 1 
			AND (reportTo.active = 1 OR reportTo.active = 2)";
	
	$query = sbexeculteQuery($sql);
	return $query;

}



/**
	///////////////////////////// SITE FUNCTIONS /////////////////////////////
*/

/** 
	Method that retrieve the site statistics
*/
function GetProjectStatus($siteId){
		
	$sql = "SELECT * FROM site 
			LEFT JOIN projectStatus ON site.projectStatusId = projectStatus.projectStatusId 
			WHERE site.siteId = $siteId AND projectStatus.active = 1";

	$query = sbexeculteQuery($sql);
	$exist = $query->rowCount();
	$array = array();

	if($exist != 0){
		
		$fetch = $query->fetch();
					
		$array[0] = array(
			"projectStatusId" => $fetch->projectStatusId,
			"projectId" => $fetch->projectId,
			"siteId" => $fetch->siteId,
			"budget" => $fetch->budget,
			"completion" => $fetch->completion,
			"manager" => $fetch->manager,
			"leftOnJob" => $fetch->leftOnJob,
			"variation" => $fetch->variation,
			"active" => $fetch->active,
			"name" => $fetch->name
		);
	}

	return $array;
}


/** 
	Method that retrieve the site hazard that hasn't been cleared
*/
function GetHazardBySiteId($siteId){
		
	$sql = "SELECT *,site.name as siteName FROM hazard 
			INNER JOIN site ON hazard.siteId = site.siteId
			INNER JOIN user ON hazard.userId = user.userId
			WHERE hazard.siteId = $siteId AND resolvedBy IS NULL 
			ORDER BY hazardId DESC";

	$query = sbexeculteQuery($sql);
	$exist = $query->rowCount();
		
	if($exist != 0){
	
		$array = array();
		$i = 0;

		while ($fetch = $query->fetch()) {
			$array[$i]['hazardId'] = $fetch->hazardId;
			$array[$i]['siteId'] = $fetch->siteId;
			$array[$i]['siteName'] = $fetch->siteName;
			$array[$i]['userId'] = $fetch->userId;
			$array[$i]['personCreated'] = $fetch->firstName . ' ' . $fetch->lastName;
			$array[$i]['entityId'] = $fetch->entityId;
			$array[$i]['title'] = $fetch->title;
			$array[$i]['description'] = $fetch->description;
			$array[$i]['precaution'] = $fetch->precaution;
			$array[$i]['preciseLocation'] = $fetch->preciseLocation;
			$array[$i]['hazardRisk'] = $fetch->risk;
			$array[$i]['time'] = $fetch->time;
			$array[$i]['timezone'] = $fetch->timezone;
			
			$array[$i]['resolvedBy'] = $fetch->resolvedBy;
			$array[$i]['resolvedByEntityId'] = $fetch->resolvedByEntityId;
			$array[$i]['resolvedByTime'] = $fetch->resolvedByTime;
			$array[$i]['resolvedByTimezone'] = $fetch->resolvedByTimezone;

			//Get actions related with hazard
			$array[$i]['actions'] = GetHazardAction($fetch->hazardId);				

			$i++; 
		}

		return $array;
	}
}

/**
	Method to check the quantity of users joined the entity
	If quantity of allowed user is less than the quantity joined return true, otherwise return false.
*/
function EntitySiteLimit($entityId,$allowSite){

	$sql = "SELECT * FROM site WHERE entityId = '$entityId' AND active = 1";
	$query = sbexeculteQuery($sql);
	$row = $query->rowCount();

	if ($row <= $allowSite) {
		return true;	
	}else{
		return false;
	}
}

/** 
	Method to insert into siteMap table the grant access to user if it doesnt yet exist
*/
function SetUserForSite($userId,$siteId,$codeId,$entityId){
	
	$sql = "SELECT * FROM siteMap WHERE siteId = '$siteId' AND userId = '$userId'";
	$query = sbexeculteQuery($sql);
	$row = $query->rowCount();
	
	if ($row == 0) {
		$sqlInsert = "INSERT INTO siteMap (siteId,userId,siteCodeId,entityId) VALUES ('$siteId','$userId','$codeId','$entityId')";
		sbexeculteQuery($sqlInsert);
	}
}

/**
	Methdd to get the length the user stayed on the site in the last 30 days
*/
function getUserLengthForSite($checkin){
	
	//Checkout
	$sqlCheckout = "	SELECT * FROM checkin 
						WHERE siteId = '$checkin->siteId' AND userId = '$checkin->userId' AND ( checkinTypeId = 3 || checkinTypeId = 14 ) AND checkinId > '$checkin->checkinId'
						LIMIT 1";
	$queryCheckout = sbexeculteQuery($sqlCheckout);
	$row = $queryCheckout->rowCount();
	
	if ($row) {
		
		$fetchCheckout = $queryCheckout->fetch();

		return $fetchCheckout->time - $checkin->time;	

	}else{
		return GetCurrentTimeStamp() - $checkin->time;
	}
	
}

/**
	///////////////////////////// CONNECTION FUNCTIONS /////////////////////////////
*/

/**
	Method to get who is following user.
*/
function GetAllConnections($userId,$entityId){

	$sql = "SELECT *,reportTo.active AS request FROM reportTo 
			INNER JOIN user ON user.userId = 
				CASE
					WHEN reportTo.followerId = '$userId' THEN reportTo.leaderId
					ELSE reportTo.followerId
				END
			INNER JOIN entityMap ON entityMap.userId = user.userId AND entityMap.entityId = '$entityId' AND (accessEnd > UNIX_TIMESTAMP() OR accessEnd IS NULL)
			WHERE (reportTo.followerId = '$userId' OR reportTo.leaderId = '$userId')
			AND (reportTo.requestBy <> '$userId' OR reportTo.active = 1)
			AND reportTo.active <> 0
			AND reportTo.entityId = '$entityId' AND user.active = 1 ";

	$query = sbexeculteQuery($sql);
	return $query;

}

/**
	Method will retrieve the connection to specific user
*/
function GetConnectionForUser($userId,$connectTo){

	$sql = "SELECT *,reportTo.active AS request FROM reportTo 
			INNER JOIN user ON user.userId = 
				CASE
					WHEN reportTo.followerId = '$userId' 
						THEN reportTo.leaderId 
					ELSE reportTo.followerId 
				END
			WHERE (reportTo.followerId = '$userId' AND reportTo.leaderId = '$connectTo') 
			OR (reportTo.followerId = '$connectTo' AND reportTo.leaderId = '$userId')";

	$query = sbexeculteQuery($sql);

	$i = 0;
	$connection = array();

	while($fetch = $query->fetch()){

		$connection[$i]['reportToId'] = $fetch->reportToId;
		$connection[$i]['followerId'] = $fetch->followerId;
		$connection[$i]['leaderId'] = $fetch->leaderId;
		$connection[$i]['entityId'] = $fetch->entityId;
		$connection[$i]['permission'] = $fetch->permission;
		//$connection[$i]['accessLevel'] = $fetch->accessLevel;
		$connection[$i]['requestBy'] = $fetch->requestBy;
		$connection[$i]['userId'] = $fetch->userId;
		$connection[$i]['firstName'] = $fetch->firstName;
		$connection[$i]['lastName'] = $fetch->lastName;
		$connection[$i]['email'] = $fetch->email;
		$connection[$i]['mobile'] = $fetch->mobile;
		$connection[$i]['active'] = $fetch->active;
		
		if ($fetch->request <> 0) {
			if ($fetch->request == 2) {
				$connection[$i]['pendingRequest'] = "1";
			}else{
				$connection[$i]['pendingRequest'] = "0";
			}	
		}else{
			$connection[$i]['isConnected'] = "0";
		}
		
		$i++;
	}

	return $connection;

}


/**
	///////////////////////////// USER FUNCTIONS /////////////////////////////
*/

/** 
Method to encript password
*/
function encryptStr($str){
	$key = 'alerton Optergy HEARTBEAT';
	$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $str, MCRYPT_MODE_CBC, md5(md5($key))));
	return $encrypted;
}

/** 
Method to decript password
*/	
function decryptStr($str){
	$key = 'alerton Optergy HEARTBEAT';
	$decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($str), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
	return $decrypted;
}

/**
	Method user login
*/
function ReviewPassword() {

	$password = $_POST['password'];
	$encodePass = decryptStr($password);
	echo "Password: " . $encodePass . "\n\r";
}

/**
	Method to add a checkout to user if his last action is a check in or report in
*/
function AdjustUserStatus($userId){
	
	$action = UserLastAction($userId);
	
	//If user last action is active add a checkout
	if ($action && $action->checkinTypeId <> 3) {
		
		$data = array(
			"time" => $action->time,
			"timezone" => $action->timezone,
			"lat" => $action->lat,
			"lng" => $action->lng,
			"userId" => $action->userId,
			"siteId" => $action->siteId,
			"checkinTypeId" => 3,
			"userEntityId" => $action->userEntityId,
			"checkinBy" => 0,
			"batteryLevel" => $action->batteryLevel
			);

		$sql = "INSERT INTO checkin (time,timezone,lat,lng,userId,siteId,checkinTypeId,userEntityId,checkinBy,batteryLevel) 
				VALUES (:time,:timezone,:lat,:lng,:userId,:siteId,:checkinTypeId,:userEntityId,:checkinBy,:batteryLevel)";
		$userActionId = sbexeculteQueryWithDataReturnId($sql,$data);

		//Change user status back to 0
		UpdateUserStatus($userId,0);
	}
}



/**
	///////////////////////////// ENTITY FUNCTIONS /////////////////////////////
*/

/**
	Method to check the quantity of users joined the entity
	If quantity of allowed user is less than the quantity joined return true, otherwise return false.
*/
function EntityUserLimit($entityId,$allowUser,$timestamp){

	$sql = "SELECT * FROM entityMap 
			WHERE entityId = '$entityId' AND (accessEnd > $timestamp OR accessEnd IS NULL) ";

	$query = sbexeculteQuery($sql);
	$row = $query->rowCount();

	if ($row <= $allowUser) {
		return true;	
	}else{
		return false;
	}
}

/**
	Method to upgrade entity to next level when the quantity of user goes above the package quantity.
	It checks if the entity is auto upgrade before calling this method
*/
function UpgradeEntity($entityId,$userId,$pkg){

	sbexeculteQuery("UPDATE setting SET packageId = '$pkg' WHERE entityId = '$entityId'");
		
}


/**
	Method to fetch entity addons
*/
function getEntityAddon($entityId){
	
	$sql = "SELECT * FROM addon 
			INNER JOIN addonType ON addon.addonTypeId = addonType.addonTypeId
			WHERE entityId = $entityId";

	$query = sbexeculteQuery($sql);
	
	$i = 0;
	$loop = array('addon' => array());
		
	while($fetch = $query->fetch()){
		$loop['addon'][$i]['addonId'] = $fetch->addonId;
		$loop['addon'][$i]['addonTypeId'] = $fetch->addonTypeId;
		$loop['addon'][$i]['active'] = $fetch->active;
		$loop['addon'][$i]['name'] = $fetch->name;

		$i++;
	}

	return $loop;
}

/**
	Method to fetch entity addons
*/
function getEntityPulse($entityId){
	
	$sql = "SELECT * FROM checkinType WHERE available = 1";
	$query = sbexeculteQuery($sql);
	
	$i = 0;
	$loop = array('pulse' => array());
	
	if ($query) {
		while($fetch = $query->fetch()){
			$loop['pulse'][$i]['checkinTypeId'] = $fetch->checkinTypeId;
			$loop['pulse'][$i]['desc'] = $fetch->desc;
			$i++;
		}
		
		return $loop;	
	}
	
}

/**
	Method to fetch entity addons
*/
function getEntityCheckinCategory($entityId){
	
	$sql = "SELECT * FROM checkinCategory WHERE entityId = '$entityId' AND active = 1";
	$query = sbexeculteQuery($sql);
	
	$i = 0;
	$loop = array('checkinCategory' => array());
		
	while($fetch = $query->fetch()){
		$loop['checkinCategory'][$i]['checkinCategoryId'] = $fetch->checkinCategoryId;
		$loop['checkinCategory'][$i]['name'] = $fetch->name;
		$loop['checkinCategory'][$i]['active'] = $fetch->active;

		$i++;
	}
	
	return $loop;
}


/**
	///////////////////////////// NOTIFICATION FUNCTIONS /////////////////////////////
*/

/**
	Method to get the push detail sent to user
*/
function PushSuccess($userId,$notificationId){

	$sql = "SELECT success FROM notiAlert WHERE notificationId = '$notificationId' AND userId = '$userId'";
	$query = sbexeculteQuery($sql);
	$fetch = $query->fetch();
	return $fetch->success;	

}

/**
	Method will get user last push based on notiRef and category
*/
function GetUserLastPushForCategory($notiRef,$category){

	$sql = "SELECT * FROM notification 
			INNER JOIN notiType ON notification.notiType = notiType.notiTypeId
			WHERE notiRef = '$notiRef' AND category = '$category' ORDER BY notificationId DESC LIMIT 1";

	$query = sbexeculteQuery($sql);
	$row = $query->rowCount();

	if ($row != 0) {
		return $query->fetch();	
	}else{
		return false;
	}
}

/**
	Method will get user last push based on notiRef and category and type
*/
function GetUserLastPushForCategoryAndNotiType($notiRef,$category,$notiType){

	$sql = "SELECT * FROM notification 
			INNER JOIN notiType ON notification.notiType = notiType.notiTypeId
			WHERE notiRef = '$notiRef' AND category = '$category' AND notiType.notiTypeId = '$notiType' ORDER BY notificationId DESC LIMIT 1";

	$query = sbexeculteQuery($sql);
	$row = $query->rowCount();

	if ($row != 0) {
		return $query->fetch();	
	}else{
		return false;
	}
	
}

/**
	Method to return int with quantity of pending push for user
	Exclude the message notification
*/
function GetQuantityPushPending($userId){
	
	$sql = "SELECT * FROM notiAlert 
			INNER JOIN notification ON notiAlert.notificationId = notification.notificationId
			INNER JOIN notiType ON notification.notiType = notiType.notiTypeId
			WHERE notiAlert.userId = '$userId' AND success = 0 AND notification.resolved = 0 AND (notiType <> 60 AND notiType <> 61) ";
	$query = sbexeculteQuery($sql);

	return $query->rowCount();
}

/**
	Method to return int with quantity of pending message
*/
function GetQuantityMessagePending($userId){
	
	$sql = "SELECT * FROM messageGroup 
			WHERE userId = '$userId' AND active = 1 AND badge <> 0";
	
	$query = sbexeculteQuery($sql);

	return $query->rowCount();
}

/**
	Method to return int with quantity of people in my team on danger
*/
function GetQuantityInDanger($userId,$accessLevel,$entityId){
	
	//If user is team leader of admin check if there is someone in danger on entity and display on banner
	if ($accessLevel == 1 || $accessLevel == 2) {
		
		if ($accessLevel == 1 || $userId == 2) {
			
			$sqlDanger = "SELECT * FROM entityMap 
						  INNER JOIN user ON entityMap.userId = user.userId
						  WHERE entityMap.entityId = $entityId AND entityMap.userId <> $userId AND user.active = 1 AND (user.status > 4 AND user.status < 7 )
						  GROUP BY entityMap.userId";
		}else{

			$sqlDanger = "SELECT * FROM reportTo
						  INNER JOIN user ON reportTo.followerId = user.userId
						  INNER JOIN entityMap ON reportTo.followerId = entityMap.userId AND (entityMap.accessEnd IS NULL OR entityMap.accessEnd > UNIX_TIMESTAMP())
						  WHERE leaderId = $userId AND reportTo.active = 1 AND reportTo.permission = 2 AND entityMap.entityId = $entityId AND (user.status > 4 AND user.status < 7 )
						  AND user.active = 1
						  GROUP BY followerId";
		}

		$queryDanger = sbexeculteQuery($sqlDanger);
		$row = $queryDanger->rowCount();

		return $queryDanger->rowCount();
	}

	return 0;

}

/**
	///////////////////////////// MESSAGE FUNCTIONS /////////////////////////////
*/

/**
	Method to return int with quantity of pending message
*/
function GetQuantityMessagePendingByMessageId($messageId,$userId){
	
	$sql = "SELECT badge FROM messageGroup
			WHERE messageId = '$messageId' AND userId = '$userId'";
	
	$query = sbexeculteQuery($sql);
	$fetch = $query->fetch();
	return $fetch->badge;
}

/**
	Method to return int with quantity of pending message
*/
function UpdateMessageViewed($messageId,$userId){
	
	$sql = "UPDATE messageGroup SET badge = 0
			WHERE messageId = $messageId AND userId = $userId";
	
	sbexeculteQuery($sql);
}

/**
	Method to return the last message sent for chat
*/
function GetLastChatInteraction($messageId){

	$sql = "SELECT sendTime FROM messageChat
			WHERE messageId = '$messageId'
			ORDER BY messageChatId DESC LIMIT 1";
	
	$query = sbexeculteQuery($sql);
	$fetch = $query->fetch();

	if ($fetch) {
		return $fetch->sendTime;	
	}
}

/**
	Method to return if user is part of message chat group
*/
function IsUserPartOfChat($userId,$messageId){
	
	$sqlGroup = "SELECT * FROM messageGroup WHERE messageId = $messageId AND userId = $userId";
	$queryGroup = sbexeculteQuery($sqlGroup);
	$partOf = $queryGroup->rowCount();

	if ($partOf != 0) {
		return true;
	}else{
		return false;
	}
}
	

/**
	///////////////////////////// QUESTIONS FUNCTIONS /////////////////////////////
*/
/** 
	Method to validate if user has answer the questio on the lenght required
*/
function NeedAnswerQuestion($questionId,$userId,$currTime,$length){

	//Fetch last time the user answers the question
	$sql = "SELECT * FROM questionAnswers 
			LEFT JOIN checkin ON checkin.checkinId = questionAnswers.checkinId
			WHERE questionAnswers.questionId = '$questionId' AND checkin.userId = '$userId' AND checkinTypeId = 1
			ORDER BY questionAnswerId DESC LIMIT 1";

	$query = sbexeculteQuery($sql);
	$row = $query->rowCount();

	if ($row != 0) {

		$fetch = $query->fetch();
		$diff = $currTime - $fetch->time;

		if ($diff > $length) {
			return TRUE;
		}else{
			return FALSE;
		}	

	}else{
		return TRUE;
	}
	
}


/**
	///////////////////////////// LEAVE FUNCTIONS /////////////////////////////
*/

/**
	Method return user entities
*/
function getUserEntity($userId){

	$sql = "SELECT * FROM entityMap WHERE userId = '$userId' AND (accessEnd IS NULL OR accessEnd > UNIX_TIMESTAMP())";
	$query = sbexeculteQuery($sql);

	$i = 0;
	$loop = array();
		
	while($fetch = $query->fetch()){
		$loop[$i]['entityMapId'] = $fetch->entityMapId;
		$loop[$i]['entityId'] = $fetch->entityId;
		$loop[$i]['userId'] = $fetch->userId;
		$loop[$i]['accessEnd'] = $fetch->accessEnd;
		$loop[$i]['accessLevel'] = $fetch->accessLevel;
		$loop[$i]['groupId'] = $fetch->groupId;

		$i++;
	}

	return $loop;

}

/**
	///////////////////////////// HAZARD FUNCTIONS /////////////////////////////
*/

/**
	Method to get all hazards/actions per entity
*/
function GetHazardByEntity($entityId) {
	
	$sql = "SELECT *,site.name as siteName,entity.name as entityName,hazard.entityId as idEntity FROM hazard 
			INNER JOIN site ON hazard.siteId = site.siteId
			INNER JOIN entity on hazard.entityId = entity.entityId
			WHERE hazard.entityId = $entityId ORDER BY hazardId DESC";

	$query = sbexeculteQuery($sql);
	$exist = $query->rowCount();
			
	if($exist != 0){

		$i = 0;
		$loop = array();			

		while($fetch = $query->fetch()){

			$loop[$i]['hazardId'] = $fetch->hazardId;
			$loop[$i]['siteId'] = $fetch->siteId;
			$loop[$i]['siteName'] = $fetch->siteName;
			$loop[$i]['userId'] = $fetch->userId;
			$loop[$i]['hazardRisk'] = $fetch->risk;
			$loop[$i]['entityId'] = $fetch->idEntity;
			$loop[$i]['entityName'] = $fetch->entityName;
			$loop[$i]['title'] = $fetch->title;
			$loop[$i]['description'] = $fetch->description;
			$loop[$i]['precaution'] = $fetch->precaution;
			$loop[$i]['preciseLocation'] = $fetch->preciseLocation;
			$loop[$i]['time'] = $fetch->time;
			$loop[$i]['timezone'] = $fetch->timezone;
			
			$loop[$i]['resolvedBy'] = $fetch->resolvedBy;
			$loop[$i]['resolvedByEntityId'] = $fetch->resolvedByEntityId;
			$loop[$i]['resolvedByTime'] = $fetch->resolvedByTime;
			$loop[$i]['resolvedByTimezone'] = $fetch->resolvedByTimezone;

			//Get actions related with hazard
			$loop[$i]['actions'] = GetHazardAction($fetch->hazardId);				

			$i++; 	

		}
		
		return $loop;	
	}
		
}

/**
	Method to get all actions related to hazard
*/
function GetHazardActionOld($hazardId){

	$sql = "SELECT * FROM hazardAction 
			INNER JOIN user ON hazardAction.userId = user.userId
			WHERE hazardId = $hazardId ORDER BY hazardId DESC";
	$query = sbexeculteQuery($sql);
	$row = $query->rowCount();
	$loop = array();

	if ($row != 0) {
		
		$i = 0;
		while($fetch = $query->fetch()){
			
			$loop[$i]['hazardActionId'] = $fetch->hazardActionId;
			$loop[$i]['hazardId'] = $fetch->hazardId;
			$loop[$i]['actionName'] = $fetch->title;
			$loop[$i]['description'] = $fetch->description;
			$loop[$i]['risk'] = $fetch->risk;
			$loop[$i]['userId'] = $fetch->userId;
			$loop[$i]['userResponsible'] = $fetch->firstName . ' ' . $fetch->lastName;
			$loop[$i]['entityId'] = $fetch->entityId;			
			$loop[$i]['time'] = $fetch->time;
			$loop[$i]['timezone'] = $fetch->timezone;

			$loop[$i]['completeBy'] = $fetch->completeBy;
			$loop[$i]['completeByTime'] = $fetch->completeByTime;

			$loop[$i]['resolvedBy'] = $fetch->resolvedBy;
			$loop[$i]['resolvedByEntityId'] = $fetch->resolvedByEntityId;
			$loop[$i]['resolvedByTime'] = $fetch->resolvedByTime;
			$loop[$i]['resolvedByTimezone'] = $fetch->resolvedByTimezone;

			$i++; 	

		}
	}

	return $loop;
}

/**
	Method to get all actions related to hazard
*/
function GetHazardAction($hazardId){

	$sql = "SELECT * FROM hazardAction 
			LEFT JOIN user ON user.userId =
				CASE
					WHEN resolvedBy IS NULL THEN hazardAction.completeBy
				ELSE hazardAction.resolvedBy
				END	
			WHERE hazardId = $hazardId ORDER BY hazardId DESC";

	$query = sbexeculteQuery($sql);
	$row = $query->rowCount();
	$loop = array();

	if ($row != 0) {
		
		$i = 0;	
		while($fetch = $query->fetch()){
			
			$loop[$i]['hazardActionId'] = $fetch->hazardActionId;
			$loop[$i]['hazardId'] = $fetch->hazardId;
			$loop[$i]['actionName'] = $fetch->title;
			$loop[$i]['description'] = $fetch->description;
			$loop[$i]['userId'] = $fetch->userId;
			$loop[$i]['userResponsible'] = $fetch->firstName . ' ' . $fetch->lastName;
			$loop[$i]['entityId'] = $fetch->entityId;			
			$loop[$i]['time'] = $fetch->time;
			$loop[$i]['timezone'] = $fetch->timezone;

			$loop[$i]['completeBy'] = $fetch->completeBy;
			$loop[$i]['completeByTime'] = $fetch->completeByTime;

			$loop[$i]['resolvedBy'] = $fetch->resolvedBy;
			$loop[$i]['resolvedByEntityId'] = $fetch->resolvedByEntityId;
			$loop[$i]['resolvedByTime'] = $fetch->resolvedByTime;
			$loop[$i]['resolvedByTimezone'] = $fetch->resolvedByTimezone;

			$i++; 	

		}
	}

	return $loop;

}

/**
	///////////////////////////// GENERAL FUNCTIONS /////////////////////////////
*/

/**
	Method return current timeStamp
*/
function GetCurrentTimeStamp(){
	$date = new DateTime();
	$timestamp = $date->getTimestamp();
	return $timestamp;
}

/**
	Method will list all the users involved with the notification reference
*/
function GetUserInfo($userId){
	
	$sql = "SELECT * FROM user 
			LEFT JOIN tokenDevice ON user.userId = tokenDevice.userId
			WHERE user.userId = '$userId'";

	$query = sbexeculteQuery($sql);
	return $query->fetch();
}

/**
	Method to get user active status detail
*/
function GetStatusByUser($userId){
	
	//$query = mysql_query("SELECT * FROM checkin WHERE userId = '$userId' ORDER BY checkinId DESC LIMIT 1") or die(mysql_error());
	$sql = "SELECT * FROM checkin 
			INNER JOIN entity on checkin.userEntityId = entity.entityId 
			LEFT JOIN site ON site.siteId = checkin.siteId 
			LEFT JOIN user ON checkin.userId = user.userId
			WHERE checkin.userId = '$userId' 
			ORDER BY checkin.checkinId 
			DESC LIMIT 1";

	$query = sbexeculteQuery($sql);
	return $query->fetch();

}

/** 
	Method to update user status
*/
function UpdateUserStatus($userId,$status){
	
	$sql = "UPDATE user SET status = '$status' WHERE userId = $userId";
	$query = sbexeculteQuery($sql);
	
	if ($query) {
		return true;
	}else{
		return false;
	}
}

/** 
	Method to update notification alert as resolved
*/
function UpdateUserAlert($userId,$notiRef){
	
	$sql = "UPDATE user SET status = '$status' WHERE userId = '$userId'";
	$query = sbexeculteQuery($sql);
	
	if ($query) {
		return true;
	}else{
		return false;
	}
}

/** 
	Save note to checkinNote
*/
function AddCheckinNote($checkinId,$note,$time,$timezone){
	
	sbexeculteQuery("INSERT INTO checkinNote (checkinId,note,time,timezone) VALUES ($checkinId,'$note',$time,'$timezone')");

}

/**
	Method to get user last action
*/
function UserLastAction($userId){
	
	$sql = "SELECT * FROM checkin 
			INNER JOIN site ON checkin.siteId = site.siteId
			WHERE checkin.userId = '$userId' ORDER BY checkin.checkinId DESC LIMIT 1";
	$query = sbexeculteQuery($sql);
	$row = $query->rowCount();

	if ($row) {
		return $query->fetch();
	}else{
		return false;
	}

}
?>