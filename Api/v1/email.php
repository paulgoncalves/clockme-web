<?php

/** 
	Method when user forgot password 
*/
function ForgotPassword() {

	require_once 'lang.php';
	require_once "WSResponseCode.php";
				
	$email = $_POST['email'];
	
	$query = sbexeculteQuery("SELECT * FROM user WHERE email = '$email'");
	$exist = $query->rowCount();
	
	if($exist != 0){

		$fetch = $query->fetch();
		
		//$decriptedPass = decryptStr($fetch->password);
		$decriptedPass = randomString(6);
		$encryptPass = encryptStr($decriptedPass);

		//Update user password in db
		$query = sbexeculteQuery("UPDATE user SET password = '$encryptPass' WHERE email = '$email'");

		//Send email to user
		$subject = "Password reset";
		$message = "<p>Hi $fetch->firstName, you have requested a new password, please use the following as a temporary password and change it when you next log in: <b>$decriptedPass.</b> <p/>";
		$message .= "<p>Stay safe,<br/> SafetyBeat Team <br/></p>";

		//Default header value
		// $headers = 	'From:no-reply@safetybeat.com' . "\r\n" .
		// 			'Reply-To: developer@safetybeat.com' . "\r\n" .
		// 			'X-Mailer: PHP/' . phpversion();

		// mail($email,$subject,$message,$headers);

		$msg = createEmail($message);		
		$headers = getEmailHeaders();		
		$sent = mail($email, $subject, $msg, $headers); 

		$array = array(
			"status" => "1",
			"msgStatus" => 'success',
			"message" => "$msgLoginForgotPassword",
			"WSResponseCode" => "$WSCodeLoginForgotPassword"
		);
		
	}else{
		$array = array(
			"status" => "0",
			"msgStatus" => 'fail',
			"message" => "$msgLoginInvalidEmail",
			"WSResponseCode" => "$WSCodeLoginInvalidEmail"
		);
	}
	
	echo json_encode($array, JSON_PRETTY_PRINT);
}


/** 
	Method when user forgot password 
*/
function EmailHazardShare($hazardDetail) {

	if (isset($hazardDetail)) {
		
		//Send email to user
		$subject = "Hazard Report";
		$message = "Hi ". $hazardDetail['personName'] .", this email is to inform you about the hazard reported at ". $hazardDetail['siteName']. ", plese take a time to review this hazard. \n\r";
		$message .= "Please login into Safetybeat app or use this link to view the hazard: http://www.safetybeat.com/assets/hazard/".$hazardDetail['hazardId']."/".$hazardDetail['hazardId'].".jpg \n\r";
		$message .= "Kind regards \n\r";
		$message .= "The SafetyBeat Team \n\r";

		// //Default header value
		// $headers = 	'From:no-reply@safetybeat.com' . "\r\n" .
		// 			'Reply-To: developer@safetybeat.com' . "\r\n" .
		// 			'X-Mailer: PHP/' . phpversion();
		
		// mail($hazardDetail['personEmail'],$subject,$message,$headers);
		
		$msg = createEmail($message);		
		$headers = getEmailHeaders();		
		$sent = mail($hazardDetail['personEmail'], $subject, $msg, $headers); 

		return true;
	}else{
		return false;
	}
	
	echo json_encode($array, JSON_PRETTY_PRINT);
}


function randomString($len){
  	
  	$key = "";
  	//$keys = array_merge(range(0,9), range("a", "z"), range("A", "Z"));
 	$keys = array_merge(range(0,9), range("a", "z"));
	
	for($i = 0; $i < $len; $i++){
		$key .= $keys[array_rand($keys)];
  	}
  
	return $key;
}

/** 
	Method when user forgot password 
*/
function SendLogToSupport() {

	require_once 'lang.php';
	require_once "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$email = "developer@safetybeat.com";

		//Send email to user
		$subject = $_POST['userName'] . " app log";
		$message = $_POST['log'] . " \n\r";
		$message .= "UserId: " . $credential['userId'] . " \n\r";
		$message .= "Stay safe, SafetyBeat Team \n\r";

		//Default header value
		$headers = 	'From:no-reply@safetybeat.com' . "\r\n" .
					'Reply-To: developer@safetybeat.com' . "\r\n" .
					'X-Mailer: PHP/' . phpversion();
					
		mail($email,$subject,$message,$headers);

	}
}


function createEmail($txt)
	{		
		$imgSrc   = 'http://safetybeat.com/dashboard/public/img/logo_v8@2x.png'; // Change image src to your site specific settings
		$imgDesc  = 'SafetyBeat logo'; // Change Alt tag/image Description to your site specific settings
		$imgTitle = 'SafetyBeat Logo'; // Change Alt Title tag/image title to your site specific settings 
		$alertonSrc = 'http://safetybeat.com/dashboard/public/img/optergy-logo.png';
		$alertonDesc  = 'SafetyBeat logo';
		$alertonTitle = 'SafetyBeat Logo';


			$msg = '<!DOCTYPE html>
					<html lang="en" class="loginHtml">
					<head>
						<meta charset="utf-8">
						<meta http-equiv="X-UA-Compatible" content="IE=edge">
						<meta name="viewport" content="width=device-width, initial-scale=1">
						<title>SafetyBeat</title>
					</head>
					<body style="background-color:#F5F5F5">
					<div style="width: 100%; float: left; clear: both; text-align: center;">
						<div style="margin: 0 auto;">
							<div style="width:50%; height: 100%; min-height:100%; margin-top: 80px; border: 2px solid #428bca; border-radius: 15px; display: block; background-color: #fff;">
								
								<div style="margin: 0 auto;">
									<div style="width: 60%; margin: 0 20%;">
										<img src="http://dashboard.safetybeat.com/img/safetybeat_logo.png" style="max-width: 60%;">
									</div>
								</div>
								<div style="width:90%; height: 100%; float: center; text-align: left; margin: 20px 10px 10px 20px;">';
									$msg .= $txt;
									$msg .= '<br><br>
								</div>
							</div>
						</div><!-- end .row -->
					</div><!-- end .containter -->
					</body>
					</html>';
		
		return $msg;
	}
	
	/**
	 * Get the email headers
	 */
	function getEmailHeaders()
	{
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
		$headers .= 'From: "SafetyBeat" <no-reply@safetybeat.com>' . "\r\n";
		return $headers;
	}

?>
