<?

/** 
	Method to generate token for user and update the database
*/
function generateToken($length) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }

    return $randomString;
}

/**
	Method to update user token
*/
function UpdateToken($token,$userId){

	//Remove any entry of api access for user on database
	sbexeculteQuery("DELETE FROM tokenApi WHERE userId = $userId");	

	$query = sbexeculteQuery("INSERT INTO tokenApi (token,userId) VALUES ('$token',$userId)");
}

/**
	Method to Validate user token
*/
function ValidateAccess($credentials){

	$sql = "SELECT * FROM tokenApi WHERE userId = :userId and token = :token";
	$query = sbexeculteQueryWithData($sql,$credentials);
	$exist = $query->rowCount();
	
	if ($exist != 0) {
		return true;
	}else{
		return false;
	}
}

?>
