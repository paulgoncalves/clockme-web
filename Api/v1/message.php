<? 

/**
	Method to get all messages for user
*/
function GetUserMessage(){

	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {
		
		$sql = "SELECT * FROM messageGroup 
				INNER JOIN message ON messageGroup.messageId = message.messageId
				WHERE messageGroup.userId = ".$credential['userId']."
				AND active = 1 ";
		$query = sbexeculteQuery($sql); 
		$row = $query->rowCount();

		if ($row) {
			
			$i = 0;
			$loop = array();

			while ($fetch = $query->fetch()) {

				$loop[$i]['messageId'] = $fetch->messageId;
				$loop[$i]['messageGroupId'] = $fetch->messageGroupId;
				$loop[$i]['active'] = $fetch->active;
				$loop[$i]['createdBy'] = $fetch->createdBy;
				$loop[$i]['lastModified'] = GetLastChatInteraction($fetch->messageId);
				$loop[$i]['createdTime'] = $fetch->createdTime;
				$loop[$i]['createdTimezone'] = $fetch->createdTimezone;
				$loop[$i]['isGroup'] = $fetch->isGroup;
				$loop[$i]['badge'] = GetQuantityMessagePendingByMessageId($fetch->messageId,$credential['userId']);

				if ($fetch->isGroup == 0) {
					
					$sqlChatWith = "SELECT * FROM messageGroup 
										INNER JOIN user ON messageGroup.userId = user.userId
										WHERE messageGroup.userId <> ".$credential['userId']." AND messageGroup.messageId = $fetch->messageId ";
					
					$queryChatWith = sbexeculteQuery($sqlChatWith);
					$fetchChatWith = $queryChatWith->fetch();

					if ($fetch->createdBy == $credential['userId']) {
						$loop[$i]['name'] = $fetch->name;
						$loop[$i]['userId'] = $fetchChatWith->userId;
					}else{
						$loop[$i]['userId'] = $fetchChatWith->userId;
						$loop[$i]['name'] = $fetchChatWith->firstName . ' ' . $fetchChatWith->lastName;
					}	

				}else{
					$loop[$i]['name'] = $fetch->name;
					$loop[$i]['userId'] = $fetch->userId;
				}
				
				$i++;
			}

			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgMessageUserSuccess",
				"WSResponseCode" => "$WSCodeMessageUserSuccess",
				"messages" => $loop
			);
			
			echo json_encode($array, JSON_PRETTY_PRINT);

		}else{

			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgMessageUserFail",
				"WSResponseCode" => "$WSCodeMessageUserFail"
			);

			echo json_encode($array, JSON_PRETTY_PRINT);

		}

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}


/**
	Method to get all messages for chat
*/
function GetMessageChat(){

	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {
		
		$messageId = $_POST['messageId'];

		//Only proceed if user is part of the message group
		if (IsUserPartOfChat($credential['userId'],$messageId)) {
			
			$sql = "SELECT * FROM messageChat 
				INNER JOIN user ON messageChat.sendById = user.userId
				WHERE messageChat.messageId = '$messageId'
					AND messageChat.active = 1 ";
			$query = sbexeculteQuery($sql); 
			$row = $query->rowCount();

			if ($row) {
				
				//Update message view success
				UpdateMessageViewed($messageId,$credential['userId']);

				$i = 0;
				$loop = array();

				while ($fetch = $query->fetch()) {

					$loop[$i]['messageId'] = $fetch->messageId;
					$loop[$i]['messageChatId'] = $fetch->messageChatId;
					$loop[$i]['sendTime'] = $fetch->sendTime;
					$loop[$i]['sendTimezone'] = $fetch->sendTimezone;
					$loop[$i]['message'] = $fetch->message;
					$loop[$i]['active'] = $fetch->active;

					$arraySend = array(
							"userId" => $fetch->userId,
							"name" => $fetch->firstName . " " . $fetch->lastName,
							"email" => $fetch->email,
							"mobile" => $fetch->mobile,
							"status" => $fetch->status
						);
					
					$loop[$i]['userDetail'] = $arraySend;

					$i++;
				}

				$array = array(
					"status" => '1',
					"msgStatus" => 'success',
					"message" => "$msgMessageSuccess",
					"WSResponseCode" => "$WSCodeMessageSuccess",
					"messageChat" => $loop
				);
				
				echo json_encode($array, JSON_PRETTY_PRINT);

			}else{

				$array = array(
					"status" => '0',
					"msgStatus" => 'fail',
					"message" => "$msgMessageFail",
					"WSResponseCode" => "$WSCodeMessageFail"
				);

				echo json_encode($array, JSON_PRETTY_PRINT);

			}
		
		}else{
			
			$array = array(
					"status" => '0',
					"msgStatus" => 'fail',
					"message" => "$msgMessageFail",
					"WSResponseCode" => "$WSCodeMessageFail"
				);

			echo json_encode($array, JSON_PRETTY_PRINT);

		}

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}


/**
	Method to send message
*/
function MessageSend(){

	require 'lang.php';
	require "WSResponseCode.php";

	//Load required files
	require '../notification_v1/notification.php';
	require '../notification_v1/functions.php';

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {
		
		$data = array(
				"messageId" => $_POST['messageId'],
				"sendById" => $_POST['userId'],
				"sendTime" => $_POST['ts'],
				"sendTimezone" => $_POST['tz'],
				"message" => $_POST['message']
			);

		$sql = "INSERT INTO messageChat (messageId,sendById,sendTime,sendTimezone,message)
				VALUES (:messageId,:sendById,:sendTime,:sendTimezone,:message)";
		$query = sbexeculteQueryWithData($sql,$data);

		if ($query) {
			
			//Update messageGroup badge for all the users involved on the chat
			$sqlUsers = "SELECT * FROM messageGroup WHERE messageId = ".$data['messageId']." AND userId <> ".$credential['userId']." ";
			$queryUsers = sbexeculteQuery($sqlUsers);
			
			if ($queryUsers) {
				while ($fetch = $queryUsers->fetch()) {
			
					//Active user chat if user has left the chat
					if ($fetch->active == 0) {
						sbexeculteQuery("	UPDATE messageGroup set badge = badge + 1, active = 1
											WHERE messageId = $fetch->messageId AND userId = $fetch->userId ");
					}else{
						sbexeculteQuery("	UPDATE messageGroup set badge = badge + 1
											WHERE messageId = $fetch->messageId AND userId = $fetch->userId ");	
					}
				}
			}
			
			//Send push to including users
			$userDetail = GetUserInfo($credential['userId']);
			$message = $userDetail->firstName . " has sent you a new message";
			$entityId = $_POST['entityId'];

			PushMessageSend($userDetail,$message,$data['messageId'],$entityId,$data['sendTime'],$data['sendTimezone']);

			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgMessageSendSuccess",
				"WSResponseCode" => "$WSCodeMessageSendSuccess"
			);
			
			echo json_encode($array, JSON_PRETTY_PRINT);

		}else{

			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgMessageSendFail",
				"WSResponseCode" => "$WSCodeMessageSendFail"
			);

			echo json_encode($array, JSON_PRETTY_PRINT);

		}

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}

/**
	Method to create a new message
*/
function MessageCreate(){

	require 'lang.php';
	require "WSResponseCode.php";
	
	//Load required files
	require '../notification_v1/notification.php';
	require '../notification_v1/functions.php';

	$inputJSON = file_get_contents('php://input');
	$arrayJson = json_decode($inputJSON, TRUE);

	$credential = array(
					"userId" => $arrayJson['userId'],
					"token" => $arrayJson['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$data = array(
				"name" => $arrayJson['name'],
				"createdBy" => $arrayJson['userId'],
				"createdTime" => $arrayJson['ts'],
				"createdTimezone" => $arrayJson['tz'],
				"entityId" => $arrayJson['entityId'],
				"isGroup" => $arrayJson['isGroup']
			);

		$sql = "INSERT INTO message (name,createdBy,createdTime,createdTimezone,entityId,isGroup)
				VALUES (:name,:createdBy,:createdTime,:createdTimezone,:entityId,:isGroup)";
		
		$messageId = sbexeculteQueryWithDataReturnId($sql,$data);

		if ($messageId) {
			
			//Check if groupType exist for pre existent connections
			//Group types:
			//1 = all connections
			//2 = all leaders
			//3 = all followers
			if (isset($arrayJson['groupType'])) {
				
				$groupType = $arrayJson['groupType'];

				//Add myself to the group
				sbexeculteQuery("INSERT INTO messageGroup (messageId,userId) VALUES ('$messageId','".$arrayJson['userId']."')");	

				if ($groupType == 1) {
					$users = sbexeculteQuery("	SELECT followerId,leaderId FROM reportTo 
												WHERE (followerId = ".$arrayJson['userId']." OR leaderId = ".$arrayJson['userId'].") AND active = 1
												GROUP BY 
													CASE
														WHEN followerId = ".$arrayJson['userId']." THEN leaderId
														ELSE followerId
													END ");

					//Insert into messageGroup
					foreach ($users as $user) {
						if ($user->leaderId == $arrayJson['userId']) {
							sbexeculteQuery("INSERT INTO messageGroup (messageId,userId) VALUES ('$messageId','$followerId')");	
						}else{
							sbexeculteQuery("INSERT INTO messageGroup (messageId,userId) VALUES ('$messageId','$leaderId')");
						}
					}	

				}else if ($groupType == 2) {
					$users = sbexeculteQuery("	SELECT leaderId FROM reportTo 
												WHERE followerId = ".$arrayJson['userId']." AND active = 1 AND permission = 2");

					//Insert into messageGroup
					foreach ($users as $user) {
						sbexeculteQuery("INSERT INTO messageGroup (messageId,userId) VALUES ('$messageId','$user->leaderId')");
					}	

				}else if ($groupType == 3) {
					$users = sbexeculteQuery("	SELECT followerId FROM reportTo 
												WHERE leaderId = ".$arrayJson['userId']." AND active = 1 AND permission = 2");

					//Insert into messageGroup
					foreach ($users as $user) {
						sbexeculteQuery("INSERT INTO messageGroup (messageId,userId) VALUES ('$messageId','$user->followerId')");
					}	
				}	
			}else{

				//Insert into messageGroup
				foreach ($arrayJson['users'] as $userId) {
					sbexeculteQuery("INSERT INTO messageGroup (messageId,userId) VALUES ('$messageId','$userId')");
				}	

			}

			//Save first message
			$dataMessage = array(
				"messageId" => $messageId,
				"sendById" => $arrayJson['userId'],
				"sendTime" => $arrayJson['ts'],
				"sendTimezone" => $arrayJson['tz'],
				"message" => $arrayJson['message']
			);

			//Save first message
			$sqlMessage = "	INSERT INTO messageChat (messageId,sendById,sendTime,sendTimezone,message)
							VALUES (:messageId,:sendById,:sendTime,:sendTimezone,:message)";
			sbexeculteQueryWithData($sqlMessage,$dataMessage);

			//Send push to including users
			$userDetail = GetUserInfo($credential['userId']);
			$message = $userDetail->firstName . " has created a message chat with you";

			PushMessageChatCreate($userDetail,$message,$messageId,$data['entityId'],$data['createdTime'],$data['createdTimezone']);

			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgMessageSaveSuccess",
				"WSResponseCode" => "$WSCodeMessageSaveSuccess",
				"messageId" => $messageId
			);
			
			echo json_encode($array, JSON_PRETTY_PRINT);
		
		}else{

			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgMessageSaveFail",
				"WSResponseCode" => "$WSCodeMessageSaveFail"
			);

			echo json_encode($array, JSON_PRETTY_PRINT);

		}

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}


/**
	Method to leave chat message
*/
function MessageLeaveChat(){

	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {
		
		$messageId = $_POST['messageId'];

		$sql = "UPDATE messageGroup SET active = 0
				WHERE messageId = '$messageId' AND userId = ".$credential['userId']." ";
		$query = sbexeculteQuery($sql);

		if ($query) {
					
			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgMessageLeaveChatSuccess",
				"WSResponseCode" => "$WSCodeMessageLeaveChatSuccess"
			);
			
			echo json_encode($array, JSON_PRETTY_PRINT);

		}else{

			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgMessageLeaveChatFail",
				"WSResponseCode" => "$WSCodeMessageLeaveChatFail"
			);

			echo json_encode($array, JSON_PRETTY_PRINT);

		}

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}


?>