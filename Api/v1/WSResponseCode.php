<?

/**
VALIDATE ACCESS
*/
$WSCodeLoginAccessDenied = 500;
$WSCodePDOError = 501;

/**
LOGIN
*/
$WSCodeLoginSuccess = 100;
$WSCodeLoginFail = 101;
$WSCodeLoginNotActive = 102;
$WSCodeLoginForgotPassword = 103;
$WSCodeLoginInvalidEmail = 104;

/**
USER
*/
$WSCodeUserRegisterSuccess = 110;
$WSCodeUserRegisterFail = 111;
$WSCodeUserRegisterExistent = 112;
$WSCodeUserUpdateSuccess = 113;
$WSCodeUserUpdateFail = 114;
$WSCodeUserUpdatePasswordSuccess = 115;
$WSCodeUserUpdatePasswordFail = 116;
$WSCodeUserStatusSuccess = 117;
$WSCodeUserStatusFail = 118;

/**
ENTITY
*/
$WSCodeEntityFetchSuccess = 120;
$WSCodeEntityFetchFail = 121;
$WSCodeEntityUserFetchSuccess = 122;
$WSCodeEntityUserFetchFail = 123;
$WSCodeEntityJoinSuccess = 124;
$WSCodeEntityJoinFail = 125;
$WSCodeEntityJoinFailUpgrade = 126;
$WSCodeEntityDeleteSuccess = 127;
$WSCodeEntityDeleteFail = 128;

/**
SITE
*/
$WSCodeSiteFetchSuccess = 130;
$WSCodeSiteFetchFail = 131;
$WSCodeSiteCreateSuccess = 132;
$WSCodeSiteCreateFail = 133;
$WSCodeSiteCreateFailUpgrade = 134;
$WSCodeSiteUpdateSuccess = 135;
$WSCodeSiteUpdateFail = 136;
$WSCodeSiteDeleteSuccess = 137;
$WSCodeSiteDeleteFail = 138;
$WSCodeSiteEmergencyFetchSuccess = 139;
$WSCodeSiteEmergencyFetchFail = 140;
$WSCodeSiteJoinSuccess = 141;
$WSCodeSiteJoinSameEntity = 142;
$WSCodeSiteJoinNoMatch = 143;
$WSCodeSiteJoinExpired = 144;
$WSCodeSiteJoinFail = 145;
$WSCodeSiteProjectStatusFetchSuccess = 146;
$WSCodeSiteProjectStatusFetchFail = 147;
$WSCodeSiteProjectFetchSuccess = 148;
$WSCodeSiteProjectFetchFail = 149;
$WSCodeSiteLogFetchSuccess = 150;
$WSCodeSiteLogFetchNoLog = 151;
$WSCodeSiteLogFetchFail = 152;
$WSCodeSiteNoteSaveSuccess = 153;
$WSCodeSiteNoteSaveFail = 154;
$WSCodeSiteNoteSuccess = 155;
$WSCodeSiteNoteFail = 156;
$WSCodeSiteNoteNoNote = 157;
$WSCodeSiteEmergencyCreateSuccess = 158;
$WSCodeSiteEmergencyCreateFail = 159;

/**
ACTION PROCESS
*/
$WSCodeActionCheckinSuccess = 160;
$WSCodeActionCheckinFail = 161;
$WSCodeActionReportinSuccess = 162;
$WSCodeActionReportinFail = 163;
$WSCodeActionCheckoutSuccess = 164;
$WSCodeActionCheckoutFail = 165;
$WSCodeActionPulseSuccess = 166;
$WSCodeActionPulseFail = 167;
$WSCodeActionUserSafeSuccess = 168;
$WSCodeActionUserSafeFail = 169;
$WSCodeActionUserIsSafe = 170;

/**
CONNECTIONS
*/
$WSCodeConnectionUserFetchSuccess = 180;
$WSCodeConnectionUserFetchFail = 181;
$WSCodeConnectionUserNoExist = 182;
$WSCodeConnectionFetchSuccess = 183;
$WSCodeConnectionFetchFail = 184;
$WSCodeConnectionFetchNoExist = 185;
$WSCodeConnectionAddSuccess = 186;
$WSCodeConnectionAddFail = 187;
$WSCodeConnectionRemoveSuccess = 188;
$WSCodeConnectionRemoveFail = 189;
$WSCodeConnectionAcceptSuccess = 190;
$WSCodeConnectionAcceptFail = 191;
$WSCodeConnectionRejectSuccess = 192;
$WSCodeConnectionRejectFail = 193;
$WSCodeConnectionPreviousAccepted = 194;
$WSCodeConnectionNoExist = 195;
$WSCodeConnectionUserSingleFetchSuccess = 196;

/**
MY TEAM
*/
$WSCodeMyTeamSuccess = 200;
$WSCodeMyTeamFail = 201;
$WSCodeMyTeamNoTeam = 202;
$WSCodeTeamLeaderFetchSuccess = 203;
$WSCodeMyTeamNoLeader = 204;
$WSCodeMyTeamLeaderFetchSuccess = 205;
$WSCodeMyTeamLeaderNoLeader = 206;

/**
GET USER LOG
*/
$WSCodeLogUserSuccess = 210;
$WSCodeLogUserFail = 211;
$WSCodeLogNoLog = 212;

/**
QUESTIONS
*/
$WSCodeQuestionSuccess = 220;
$WSCodeQuestionAnswered = 221;
$WSCodeQuestionNoQuestion = 222;
$WSCodeQuestionFail = 223;
$WSCodeQuestionAnswerSuccess = 224;
$WSCodeQuestionAnswerFail = 225;

/**
NOTIFICATION
*/
$WSCodeNotificationSuccess = 230;
$WSCodeNotificationFail = 231;
$WSCodeNotificationClearSuccess = 232;
$WSCodeNotificationClearFail = 233;
$WSCodeNotificationClearAllSuccess = 234;
$WSCodeNotificationClearAllFail = 235;

/**
MAN DOWN
*/
$WSCodeManDownSuccess = 240;
$WSCodeManDownFail = 241;
$WSCodeManDownDeactiveSuccess = 242;
$WSCodeManDownDeactiveFail = 243;
$WSCodeManDownDeactiveWrongPassword = 244;

/**
UPLOAD
*/
$WSCodeImageSuccess = 250;
$WSCodeImageFail = 251;
$WSCodeImageInvalid = 252;

/**
LOG MESSAGES
*/
$WSCodeLogSuccess = 260;
$WSCodeLogFail = 261;
$WSCodeLogEntityUpdateByUser = 262;
$WSCodeLogEntityUpdateBySite = 264;

/**
ANALYTICS
*/
$WSCodeAnalyticsUserLauchSuccess = 270;
$WSCodeAnalyticsUserLauchFail = 271;
$WSCodeAnalyticsUserCloseSuccess = 272;
$WSCodeAnalyticsUserCloseFail = 273;

/**
LEAVE
*/
//All leave by userId
$WSCodeLeaveFetchSuccess = 280;
$WSCodeLeaveFetchFail = 281;
$WSCodeLeaveFetchNoLeave=282;
//Save leave
$WSCodeLeaveSaveSuccess = 283;
$WSCodeLeaveSaveFail = 284;
//Uodate leave
$WSCodeLeaveUpdateSuccess = 285;
$WSCodeLeaveUpdateFail = 286;
//Remove leave
$WSCodeLeaveDeleteSuccess = 287;
$WSCodeLeaveDeleteFail = 288;
//Leave type by entity
$WSCodeLeaveTypeFetchSuccess = 289;
$WSCodeLeaveTypeFetchFail = 290;
$WSCodeLeaveTypeFetchNoLeaveType = 291;

/**
HAZARD
*/
//All hazard by siteId
$WSCodeHazardSuccess = 300;
$WSCodeHazardFail = 301;
$WSCodeHazardNoHazard = 302;
//Create hazard
$WSCodeHazardSavedSuccess = 303;
$WSCodeHazardSavedFail = 304;
//Create hazard action
$WSCodeHazardActionSavedSuccess = 305;
$WSCodeHazardActionSavedFail = 306;
//Update hazard
$WSCodeHazardUpdatedSuccess = 307;
$WSCodeHazardUpdatedFail = 308;
//Update hazard action
$WSCodeHazardActionUpdatedSuccess = 309;
$WSCodeHazardActionUpdatedFail = 310;
//Close hazard
$WSCodeHazardClosedSuccess = 311;
$WSCodeHazardClosedFail = 312;
//Close hazard action
$WSCodeHazardActionClosedSuccess = 313;
$WSCodeHazardActionClosedFail = 314;
//Share hazard
$WSCodeHazardShareSuccess = 315;
$WSCodeHazardShareFail = 316;
// Get hazard by id
$WSCodeHazardDetailSuccess = 317;
$WSCodeHazardDetailFail = 318;

/**
LOCATION BACKGROUND
*/
$WSCodeLocationSaveSuccess = 320;
$WSCodeLocationSaveFail = 321;

/**
MESSAGE 
*/
//Get all message by user id
$WSCodeMessageUserSuccess = 330;
$WSCodeMessageUserFail = 331;
//Get all message chat by id
$WSCodeMessageSuccess = 332;
$WSCodeMessageFail = 333;
//Send message
$WSCodeMessageSendSuccess = 334;
$WSCodeMessageSendFail = 335;
//Save message
$WSCodeMessageSaveSuccess = 336;
$WSCodeMessageSaveFail = 337;
//Leave chat message
$WSCodeMessageLeaveChatSuccess = 338;
$WSCodeMessageLeaveChatFail = 339;

/**
DOCUMENTS
*/
$WSCodeDocumentsFetchSuccess = 340;
$WSCodeDocumentsFetchFail = 341;

/**
DEVICE
*/
$WSCodeTokenDeviceUpdateSuccess = 350;
$WSCodeTokenDeviceUpdateFail = 351;
$WSCodeUserVersionUpdateSuccess = 352;
$WSCodeUserVersionUpdateFail = 353;

?>
