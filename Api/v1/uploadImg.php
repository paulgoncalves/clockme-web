<?php

/**
	Method to upload user image to server
*/
function ImageUpload(){

	include "lang.php";
	require "WSResponseCode.php";

	$credentials = explode('&',$_POST['filenames']);
	$credential = array(
					"userId" => $credentials[0],
					"token" => $credentials[1]
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$userId = $credentials[0];
		$token = $credentials[1];
		$folder = $credentials[2];
		
		$file = basename($_FILES['userfile']['name']);

		$checkImgage = getimagesize($_FILES["userfile"]["tmp_name"]);
    	
    	if($checkImgage !== false) {

	    	//DEFINE UPLOAD DIRECTORY BASED ON FOLDER
			if ($folder == "userImage") {
				$uploaddir = '../../assets/user/'.$userId.'/';	
				$newName = $uploaddir . $userId . $file;
				$fileName = $userId;

			}else if ($folder == "hazard") {
				$hazardId = $credentials[3];
				$uploaddir = '../../assets/hazard/'.$hazardId.'/';
				$newName = $uploaddir . $hazardId . $file;
				$fileName = $hazardId;

			}else if ($folder == "hazardAction") {
				$hazardActionId = $credentials[3];
				$uploaddir = '../../assets/hazardAction/'.$hazardActionId.'/';
				$newName = $uploaddir . $hazardActionId . $file;
				$fileName = $hazardActionId;

			}else if($folder == "support"){
				$uploaddir = '../../assets/support/'.$userId.'/';
				$timeStamp = GetCurrentTimeStamp();
				$newName = $uploaddir . $timeStamp . $file;
				$fileName = $timeStamp;
			}

			//Create directory if doesnt exist
			if (!file_exists($uploaddir)) {
	 			mkdir($uploaddir, 0777, true);
		    }

		    //Move file to location	  
			move_uploaded_file($_FILES['userfile']['tmp_name'], $newName);	

			if (file_exists($uploaddir.''.$fileName.'.jpg')) {
			
				$array = array(
					"status" => "1",
					"msgStatus" => 'success',
					"message" => "$msgImageSuccess",
					"WSResponseCode" => "$WSCodeImageSuccess"
				);	

    		}else{

    			//Image not exist
	    		$array = array(
					"status" => "0",
					"msgStatus" => 'fail',
					"message" => "$msgImageFail",
					"WSResponseCode" => "$WSCodeImageFail"
				);	

	    	}

	    }else{

	    	//Not image
			$array = array(
				"status" => "0",
				"msgStatus" => 'fail',
				"message" => "$msgImageFail",
				"WSResponseCode" => "$WSCodeImageFail"
			);	

		}

	}else{

		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	}
	
	echo json_encode($array, JSON_PRETTY_PRINT);

}

?>