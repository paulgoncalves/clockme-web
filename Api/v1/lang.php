<?

/**
VALIDATE ACCESS
*/
$msgLoginAccessDenied = "Access Denied";

/**
LOGIN
*/
//Login to app
$msgLoginSuccess = "Welcome to SafetyBeat";
$msgLoginFail = "Email and password don't match";
$msgLoginNotActive = "Your account is not active, please contact SafetyBeat team";
$msgLoginForgotPassword = "A new temporary password has been sent to your registered email address";
$msgLoginInvalidEmail = "Email not found";

/**
USER
*/
//Register
$msgUserRegisterSucess = "User saved";
$msgUserRegisterFail = "Error occur, user not saved";
$msgUserRegisterExistent = "Email already registered";
//update user
$msgUserUpdateSuccess = "Your information was updated successfully";
$msgUserUpdateFail = "Error occur, update failed";
$msgUserUpdatePasswordSuccess = "Your password was updated successfully";
$msgUserUpdatePasswordFail = "Error occur, current password incorrect";
//user status
$msgUserStatusSuccess = "User status retrieved successfully";
$msgUserStatusFail = "Error occur, user status retrieve failed";
//update device token
$msgTokenDeviceUpdateSuccess = "Device token updated";
$msgTokenDeviceUpdateFail = "Error occur, device token not updated";
//update version
$msgUserVersionUpdateSuccess = "Device version updated";
$msgUserVersionUpdateFail = "Error occur, device version not updated";

/**
ENTITY
*/
//User entity
$msgEntityFetchSuccess = "Entity retrieve successfully";
$msgEntityFetchFail = "No entity available";
//All entity
$msgEntityUserFetchSuccess = "User entity retrieve successfully";
$msgEntityUserFetchFail = "No entity available";
//joing entity
$msgEntityJoinSuccess = "Entity join successfully";
$msgEntityJoinFail = "Error occur, you already join the entity";
$msgEntityJoinFailUpgrade = "Entity reached maximum capacity of user, contact the company";
//Delete joined entity
$msgEntityDeleteSuccess = "Entity deleted successfully";
$msgEntityDeleteFail = "Error occur, entity not deleted";

/**
SITE
*/
//Fetch site
$msgSiteFetchSuccess = "Site retrieve successfully";
$msgSiteFetchFail = "No site available";
//create new site
$msgSiteCreateSuccess = "New site created";
$msgSiteCreateFail = "Error occur, site not created";
$msgSiteCreateFailUpgrade = "Entity reached maximum capacity of site, contact the company";
//update site
$msgSiteUpdateSuccess = "Site updated successfully";
$msgSiteUpdateFail = "Error occur, update failed";
//delete site
$msgSiteDeleteSuccess = "Site deleted successfully";
$msgSiteDeleteFail = "Error occur, delete failed";
//emergency
$msgSiteEmergencyFetchSuccess = "Emergency retrieve successfully";
$msgSiteEmergencyFetchFail = "No emergency available";
//emergency create
$msgSiteEmergencyCreateSuccess = "Site Emergency create successfully";
$msgSiteEmergencyCreateFail = "Error occur, site emergency create failed";
//joing entity
$msgSiteJoinSuccess = "Successfully joined site";
$msgSiteJoinSameEntity = "Error occur, you already have access to this site";
$msgSiteJoinNoMatch = "Site code not valid";
$msgSiteJoinExpired = "Code has expire";
$msgSiteJoinFail = "Error occur, join failed";
//Site status
//$msgSiteProjectStatusFetchSuccess = "Project status retrieve successfully";
//$msgSiteProjectStatusFetchFail = "Error occur, project status not retrieved";
//Site Project 
$msgSiteProjectFetchSuccess = "Site project retrieved successfully";
$msgSiteProjectFetchFail = "Error occur, site project not retrieved";
//Site log
$msgSiteLogFetchSuccess = "Site log retrieve successfully";
$msgSiteLogFetchFail = "Error occur, site log not retrieved";
$msgSiteLogFetchNoLog = "No site log available";
//Site note
$msgSiteNoteSuccess = "Site Note fetch successfully";
$msgSiteNoteFail = "Error occur, Site Note fetch failed";
$msgSiteNoteNoNote = "No Site Note available";

//Site note
$msgSiteNoteSaveSuccess = "Site Note saved successfully";
$msgSiteNoteSaveFail = "Error occur, site note not saved";

/**
CHECK-IN PROCESS
*/
//checkin
$msgActionCheckinSuccess = "Check-in successfully";
$msgActionCheckinFail = "Check-in failed";
//reportin
$msgActionReportinSuccess = "Report-in successfully";
$msgActionReportinFail = "Report-in failed";
//checkout
$msgActionCheckoutSuccess = "Check-out successfully";
$msgActionCheckoutFail = "Check-out failed";
//pulse
$msgActionPulseSuccess = "Pulse successfully";
$msgActionPulseFail = "Pulse failed";
//User safe
$msgActionUserSafeSuccess = "User safety reported successfully";
$msgActionUserSafeFail = "User safety failed";
$msgActionUserIsSafe = "has performed an action and is currently safe.";

/**
CONNECTIONS
*/
//All users for entity
$msgConnectionFetchSuccess = "Entity users retrieve successfully";
$msgConnectionFetchFail = "Fail to retrieve users";
$msgConnectionNoExist = "No users for entity";
//Get all user connections
$msgConnectionUserFetchSuccess = "Users connection retrieve successfully";
$msgConnectionUserSingleFetchSuccess = "Single user connection retrieve successfully";
$msgConnectionUserFetchFail = "Fail to retrieve users connection";
$msgConnectionUserNoExist = "No connection for user for entity";
//Add connection
$msgConnectionAddSuccess = "Connection request sent";
$msgConnectionAddFail = "Fail to add connection";
//remove connection
$msgConnectionRemoveSuccess = "Connection removed successfully";
$msgConnectionRemoveFail = "Fail to remove connection";
//Accept connection request
$msgConnectionAcceptSuccess = "Connection accepted successfully";
$msgConnectionAcceptFail = "Fail to accept connection";
//Reject connection request
$msgConnectionRejectSucess = "Connection rejected successfully";
$msgConnectionRejectFail = "Connection rejected fail";
//Request others
$msgConnectionPreviousAccepted = "Connection previously accepted";
$msgConnectionNoExist = "Connection request dont exist";

/**
MY TEAM
*/
//All My team
$msgMyTeamSuccess = "Team retrieve successfully";
$msgMyTeamFail = "Error occur, retrieve failed";
$msgMyTeamNoTeam = "You currently have no team members, use “Menu:->Connection” to add people to your team.";
//Team leader by entity
$msgTeamLeaderFetchSuccess = "Team leader retrieve successfully";
$msgMyTeamNoLeader = "No Team leader set for this entity";
//My team leader
$msgMyTeamLeaderFetchSuccess = "My Team leader retrieve successfully";
$msgMyTeamLeaderNoLeader = "You have no team leader set";

/**
HAZARD
*/
//Get Hazard by id
$msgHazardDetailSuccess = "Hazard detail retrieve successfully";
$msgHazardDetailFail = "Error occur, hazard detail retrieve failed";
//All hazard by siteId
$msgHazardSuccess = "Hazard retrieve successfully";
$msgHazardFail = "Error occur, hazard retrieve failed";
$msgHazardNoHazard = "No hazard available";
//Create hazard
$msgHazardSavedSuccess = "Hazard created successfully";
$msgHazardSavedFail = "Error ocurr, hazard not created";
//Create hazard action
$msgHazardActionSavedSuccess = "Hazard action created successfully";
$msgHazardActionSavedFail = "Error ocurr, hazard action not created";
//Update hazard
$msgHazardUpdatedSuccess = "Hazard updated successfully";
$msgHazardUpdatedFail = "Error occur, hazard update failed";	
//Update hazard action
$msgHazardActionUpdatedSuccess = "Hazard action updated successfully";
$msgHazardActionUpdatedFail = "Error occur, hazard action update failed";
//Close hazard
$msgHazardClosedSuccess = "Hazard clear successfully";
$msgHazardClosedFail = "Error occur, hazard clear failed";
//Close hazard action
$msgHazardActionClosedSuccess = "Action cleared successfully";
$msgHazardActionClosedFail = "Error occur, action clear failed";
//Share hazard
$msgHazardShareSuccess = "Hazard shared successfully";
$msgHazardShareFail = "Error occur, hazard share failed";

/**
MESSAGE
*/
//Get all messages by userId
$msgMessageUserSuccess = "User Message retrieve successfully";
$msgMessageUserFail = "Error occur, user message retrieve failed";
//Get all message chat by id
$msgMessageSuccess = "Message chat retrieve successfully";
$msgMessageFail = "Error occur, message chat retrieve failed";
//Send message
$msgMessageSendSuccess = "Message send successfully";
$msgMessageSendFail = "Error occur, message send failed";
//Send message
$msgMessageSaveSuccess = "Message save successfully";
$msgMessageSaveFail = "Error occur, message save failed";
//Leave chat message
$msgMessageLeaveChatSuccess = "User leave message successfully";
$msgMessageLeaveChatFail = "Error occur, user leave message failed";

/**
LEAVE
*/
//All leave by userId
$msgLeaveFetchSuccess = "Leave retrieve successfully";
$msgLeaveFetchFail = "Error occur, leave retrieve failed";
$msgLeaveFetchNoLeave = "No leave available";
//All leave type by entity
$msgLeaveTypeFetchSuccess = "Leave type retrieve successfully";
$msgLeaveTypeFetchFail = "Error occur, leave type retrieve failed";
$msgLeaveTypeFetchNoLeaveType = "No leave type available";
//Save leave
$msgLeaveSaveSuccess = "Leave saved successfully";
$msgLeaveSaveFail = "Error occur, leave not saved";
//Uodate leave
$msgLeaveUpdateSuccess = "Leave updated successfully";
$msgLeaveUpdateFail = "Error occur, leave not updated";
//Remove leave
$msgLeaveDeleteSuccess = "Leave removed successfully";
$msgLeaveDeleteFail = "Error occur, leave not remmoved";

/**
DOCUMENTS
*/
//Fetch site
$msgDocumentsFetchSuccess = "Documents retrieve successfully";
$msgDocumentsFetchFail = "No documents available";

/**
GET USER LOG
*/
$msgLogUserSuccess = "Log retrieve successfully";
$msgLogUserFail = "Error occur, retrieve failed";
$msgLogUserNoLog = "No log available";

/**
QUESTIONS
*/
//Send all question
$msgQuestionSuccess = "Questions retrieve successfully";
$msgQuestionAswered = "User has answered the questions";
$msgQuestionNoQuestion = "No questions available";
$msgQuestionFail = "Error occur, retrieve failed";
//Get all the answers
$msgQuestionAnswerSuccess = "Answers received successfully";
$msgQuestionAnswerFail = "Error occur, receiving failed";

/**
NOTIFICATION
*/
$msgNotificationSuccess = "User notification retrieve successfully";
$msgNotificationFail = "Error occur, user notification failed";
//Clear notification
$msgNotificationClearSuccess = "User notification clear successfully";
$msgNotificationClearFail = "Error occur, user notification not clear";
//Clear all notification
$msgNotificationClearAllSuccess = "User all notification clear successfully";
$msgNotificationClearAllFail = "Error occur, user all notification not clear";

/**
MAN DOWN
*/
$msgManDownSuccess = "Man down successfully";
$msgManDownFail = "Error occur, man down failed";
$msgManDownDeactiveSuccess = "Man down deactived successfully";
$msgManDownDeactiveFail = "Error occur, man down deactived failed";
$msgManDownDeactiveWrongPassword = "Error occur, wrong password";

/**
LOCATION BACKGROUND
*/
$msgLocationSaveSuccess = "User location saved successfully";
$msgLocationSaveFail = "Error occur, user location save failed";

/**
UPLOAD
*/

$msgImageSuccess = "File uploaded successfully";
$msgImageFail = "File not uploaded";
$msgImageInvalid = "Invalid file format";

/**
LOG MESSAGES
*/
$logSuccess = "Log saved successfully";
$logFail = "Error occur, log not saved";
$logEntityUpdateByUser = "Entity reached the maximum user capacity and was updated to next package.";
$logEntityUpdateBySite = "Entity reached the maximum site capacity and was updated to next package.";

/**
ANALYTICS
*/
//App launch
$msgAnalyticsUserLauchSuccess = "User lauch app successfully";
$msgAnalyticsUserLauchFail = "Error occur, user lauch app failed";
//App close
$msgAnalyticsUserCloseSuccess = "User close app successfully";
$msgAnalyticsUserCloseFail = "Error occur, user close app failed";

?>
