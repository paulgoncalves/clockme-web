<? 
	
/**
	Method called every time the user lauch app
*/
function UserLauchApp(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {
		
		$time = GetCurrentTimeStamp();
		$entityId = $_POST['entityId'];

		$sql = "INSERT INTO analytics(userId,time,action) VALUES(".$credential['userId'].",'$time',1)";
		$query = sbexeculteQuery($sql);

		if ($query) {
			
			$sqlUser = "SELECT * FROM user 
						INNER JOIN entityMap ON entityMap.userId = ".$credential['userId']." AND entityMap.entityId = $entityId AND (accessEnd > UNIX_TIMESTAMP() OR accessEnd IS NULL)
						WHERE user.userId = ".$credential['userId']."";
			$queryUser = sbexeculteQuery($sqlUser);
			$fethUser = $queryUser->fetch();
			
			if ($fethUser) {
				
				$arrayUser = array(
					"userId" => $fethUser->userId,
					"userStatus" => $fethUser->status,
					"email" => $fethUser->email,
					"active" => $fethUser->active,
					"debug" => $fethUser->debug,
					"badge" => GetQuantityPushPending($fethUser->userId),
					"badgeMessage" => GetQuantityMessagePending($fethUser->userId),
					"badgeInDanger" => GetQuantityInDanger($fethUser->userId,$fethUser->accessLevel,$entityId)
				);

				$array = array(
					"status" => '1',
					"msgStatus" => 'success',
					"message" => "$msgAnalyticsUserLauchSuccess",
					"WSResponseCode" => "$WSCodeAnalyticsUserLauchSuccess",
					"userDetail" => $arrayUser
				);	
			
			}else{
				
				$array = array(
					"status" => '0',
					"msgStatus" => 'fail',
					"message" => "$msgAnalyticsUserLauchFail",
					"WSResponseCode" => "$WSCodeAnalyticsUserLauchFail"
				);

			}
			
		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgAnalyticsUserLauchFail",
				"WSResponseCode" => "$WSCodeAnalyticsUserLauchFail"
			);
		}
				
	}else{

		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	}

	echo json_encode($array, JSON_PRETTY_PRINT);	
}

/**
	Method called every time the user close app
*/
function UserCloseApp(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {
		
		$time = GetCurrentTimeStamp();
		
		$sql = "INSERT INTO analytics(userId,time,action) VALUES(".$credential['userId'].",'$time',0)";
		$query = sbexeculteQuery($sql);

		if ($query) {			
		
			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgAnalyticsUserCloseSuccess",
				"WSResponseCode" => "$WSCodeAnalyticsUserCloseSuccess"
			);

		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgAnalyticsUserCloseFail",
				"WSResponseCode" => "$WSCodeAnalyticsUserCloseFail"
			);
		}
				
	}else{

		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	}

	echo json_encode($array, JSON_PRETTY_PRINT);	
}

?>