<?php

/**
	Method user login
*/
function Login() {

	require 'lang.php';
	require "WSResponseCode.php";
	
	if (isset($_POST['refreshUser'])) {
		
		$refreshUser = $_POST['refreshUser'];
		$sql = "SELECT * FROM user WHERE userId = $refreshUser";
		$query = sbexeculteQuery($sql);
		$row = $query->rowCount();
		
	}else{
		
		$password = $_POST['password'];

		$data = array(
			"email" => $_POST['email'],
			"encodePass" => strval(encryptStr($password))
		);

		$sql = "SELECT * FROM user WHERE email = :email AND password = :encodePass";
		$query = sbexeculteQueryWithData($sql,$data);
		$row = $query->rowCount();
		
	}

	if($row != 0){

		$fetch = $query->fetch();

		//Only adjust if user is not been refresh
		if (!isset($refreshUser)) {
			//Insert into checkin table if user last action is a check in or report in
			AdjustUserStatus($fetch->userId);
		}

		$array = LoginSuccess($fetch,1);
		
	}else{

		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginFail",
			"WSResponseCode" => "$WSCodeLoginFail"
		);	

	}
	
	echo json_encode($array, JSON_PRETTY_PRINT);

}

function LoginSuccess($fetch,$attempt){

	require 'lang.php';
	require "WSResponseCode.php";
		
	$accessLevel = "";
	$entityId = "";
	$name = "";
	$emergencyNumber = "";
	$reportTime = "";
	$special = "0";
	$addons = array('addon' => array());
	$checkinCategories = array('checkinCategory' => array());
	$pulses = array('pulses' => array());

	// Funtion that will try to get the user last entity information to send with webservice
	//Check if entity has been sent
	if (isset($_POST['entityId'])) {

		//In case the entity id has been sent via POST
		$entityId = $_POST['entityId'];
		$sql = "SELECT * FROM entityMap 
				INNER JOIN entity on entityMap.entityId = entity.entityId 
				INNER JOIN setting ON entityMap.entityId = setting.entityId 
				WHERE userId = $fetch->userId and entityMap.entityId = $entityId";

		$queryEntity = sbexeculteQuery($sql);
		$row = $queryEntity->rowCount();

	}else{
					
		//Get the newest entityId the user checkin
		$sql = "SELECT * FROM checkin 
				INNER JOIN entityMap ON checkin.userEntityId = entityMap.entityId AND checkin.userId = entityMap.userId
				INNER JOIN entity on checkin.userEntityId = entity.entityId 
				INNER JOIN setting ON checkin.userEntityId = setting.entityId 
				WHERE checkin.userId = $fetch->userId 
				ORDER BY checkinId DESC LIMIT 1";

		$queryEntity = sbexeculteQuery($sql);
		$row = $queryEntity->rowCount();

		// If user hasn't check-in it will get the newest entity the user has joined
		if ($row == 0) {
						
			$sql = "SELECT * FROM entityMap 
					INNER JOIN entity on entityMap.entityId = entity.entityId AND entityMap.userId = $fetch->userId 
					INNER JOIN setting ON entityMap.entityId = setting.entityId 
					WHERE entityMap.userId = $fetch->userId 
					ORDER BY entityMap.entityMapId DESC LIMIT 1";

			$queryEntity = sbexeculteQuery($sql);
			$row = $queryEntity->rowCount();
		}

	}
	
	if ($row != 0) {
			
		$fetchEntity = $queryEntity->fetch();
		$accessLevel = $fetchEntity->accessLevel;
		$entityId = $fetchEntity->entityId;
		$name = $fetchEntity->name;
		$emergencyNumber = $fetchEntity->emergencyNumber;
		$reportTime = $fetchEntity->notify1;
		$special = $fetchEntity->special;
		$addons = getEntityAddon($fetchEntity->entityId);
		$checkinCategories = getEntityCheckinCategory($fetchEntity->entityId);
		$pulses = getEntityPulse($fetchEntity->entityId);
	}

	//Generate token with length 20
	$token = GenerateToken(20);
	
	//Update user token
	UpdateToken($token,$fetch->userId);
	
	//Check if user status is active or if user has been remove ability to use the app.
	if($fetch->active == 1){

		$array = array(
			"status" => '1',
			"msgStatus" => 'success',
			"message" => "$msgLoginSuccess",
			"WSResponseCode" => "$WSCodeLoginSuccess",
			"attempt" => "$attempt",
			"userId" => $fetch->userId,
			"token" => $token,
			"email" => $fetch->email,
			"firstName" => $fetch->firstName,
			"lastName" => $fetch->lastName,
			"mobile" => $fetch->mobile,
			"mobileEmergency" => isset($fetch->mobileEmergency)?$fetch->mobileEmergency:"",
			"emergencyNumber" => $emergencyNumber,
			"accessLevel" => $accessLevel,
			"entityId" => $entityId,
			"entityName" => $name,
			"reportTime" => $reportTime,
			"special" => $special,
			"addons" => $addons,
			"checkinCategories" => $checkinCategories,
			"pulses" => $pulses
		);

	}else{

		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginFail",
			"WSResponseCode" => "$WSCodeLoginFail"
		);

	}

	return $array;

}

/**
	Method to register user.
	This method also insert a token to user access
*/
function CreateUser() {
	
	require 'lang.php';
	require "WSResponseCode.php";

	$password = $_POST['password'];
	
	if (isset($_POST['mobileEmergency'])) {
		$mobileEmergency = $_POST['mobileEmergency'];
	}else{
		$mobileEmergency = "";
	}

	$data = array(
			"firstName" => $_POST['firstName'],
			"lastName" => $_POST['lastName'],
			"email" => $_POST['email'],
			"encodePass" => encryptStr($password),
			"mobile" => $_POST['mobile'],
			"mobileEmergency" => $mobileEmergency,
			"active" => 1
		);

	$sqlUser = "SELECT * FROM user where email = '".$data['email']."'";
	$queryUser = sbexeculteQuery($sqlUser);
	$row = $queryUser->rowCount();
	
	if($row == 0){
		
		$sql = "INSERT INTO user (firstName,lastName,email,password,mobile,mobileEmergency,active) 
				VALUES (:firstName,:lastName,:email,:encodePass,:mobile,:mobileEmergency,:active)";

		$userId = sbexeculteQueryWithDataReturnId($sql,$data);

		//Generate token id and get userid that was just created
		$token = generateToken(20);

		$sqlToken = "INSERT INTO tokenApi (token,userId) VALUES ('$token','$userId')";
		$queryToken = sbexeculteQuery($sqlToken);

		if($userId){

			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgUserRegisterSucess",
				"WSResponseCode" => "$WSCodeUserRegisterSuccess"
			);

		}else{

			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgUserRegisterFail",
				"WSResponseCode" => "$WSCodeUserRegisterFail"
			);

		}
	}else{
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgUserRegisterExistent",
			"WSResponseCode" => "$WSCodeUserRegisterExistent"
		);
	}
	
	
	echo json_encode($array, JSON_PRETTY_PRINT);
}

/**
	Method to update user
*/
function UpdateUser() {

	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		if (isset($_POST['mobileEmergency'])) {
			$mobileEmergency = $_POST['mobileEmergency'];
		}else{
			$mobileEmergency = "";
		}

		$data = array(
			"userId" => $_POST['userId'],
			"firstName" => $_POST['firstName'],
			"lastName" => $_POST['lastName'],
			"email" => $_POST['email'],
			"mobile" => $_POST['mobile'],
			"mobileEmergency" => $mobileEmergency
		);		

		$sql = "UPDATE user set firstName = :firstName,lastName = :lastName,email = :email,mobile = :mobile, mobileEmergency = :mobileEmergency WHERE userId = :userId";
		$query = sbexeculteQueryWithData($sql,$data);
				
		if($query){
			
			$sqlUser = "SELECT * FROM user where userId = ".$data['userId']."";

			$queryUser = sbexeculteQuery($sqlUser);
			$fetch = $queryUser->fetch();

			if (isset($fetch->mobileEmergency)) {
				$emergencyNumber = $fetch->mobileEmergency;
			}else{
				$emergencyNumber = "";
			}

			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgUserUpdateSuccess",
				"WSResponseCode" => "$WSCodeUserUpdateSuccess",
				"firstName" => $fetch->firstName,
				"lastName" => $fetch->lastName,
				"mobile" => $fetch->mobile,
				"mobileEmergency" => $emergencyNumber
			);

		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgUserUpdateFail",
				"WSResponseCode" => "$WSCodeUserUpdateFail"
			);
		}
		
		echo json_encode($array, JSON_PRETTY_PRINT);

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);
	}
}


/**
	Method to update user password
*/
function UpdateUserPassword() {

	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$userId = $_POST['userId'];
		$password = $_POST['password'];
		$currentPassword = $_POST['currentPassword'];
		$encodePass = encryptStr($currentPassword);	

		$sqlCheck = "SELECT * FROM user WHERE userId = '$userId' AND password = '$encodePass'";
		
		$queryCheck = sbexeculteQuery($sqlCheck);
		$row = $queryCheck->rowCount();

		if ($row != 0) {
			
			//Check to see if necessary to udate password		
			$encodePass = encryptStr($_POST['password']);

			$sqlUpdate = "UPDATE user set password = '$encodePass' WHERE userId = '$userId'";
			$queryUpdate = sbexeculteQuery($sqlUpdate);
			
			if($queryUpdate){
				$array = array(
					"status" => '1',
					"msgStatus" => 'success',
					"message" => "$msgUserUpdatePasswordSuccess",
					"WSResponseCode" => "$WSCodeUserUpdatePasswordSuccess"
				);
			}else{
				$array = array(
					"status" => '0',
					"msgStatus" => 'fail',
					"message" => "$msgUserUpdateFail",
					"WSResponseCode" => "$WSCodeUserUpdateFail"
				);
			}

		}else{
			
			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgUserUpdatePasswordFail",
				"WSResponseCode" => "$WSCodeUserUpdatePasswordFail"
			);	

		}
		
	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	}

	echo json_encode($array, JSON_PRETTY_PRINT);
}

/**
	Method to update device id
*/
function UpdateDevice() {

	require 'lang.php';
	require "WSResponseCode.php";

	$data = array(
		"userId" => $_POST['userId'],
		"deviceId" => $_POST['deviceId'],
		"deviceType" => $_POST['deviceType'],
		"time" => $_POST['dateModified'],
		"timezone" => $_POST['timezone'],
		"version" => $_POST['version']
	);

	//Check to see if necessary to udate password
	$sql = "SELECT * FROM tokenDevice WHERE userId = ".$data['userId']."";

	$query = sbexeculteQuery($sql);
	$row = $query->rowCount();

	//Check if the only token existent is equal to the one sent and update if necessary
	if ($row == 1) {
		
		$fetch = $query->fetch();

		if ($fetch->deviceId != $data['deviceId']) {
			
			$sqlDevice = "UPDATE tokenDevice SET deviceId = :deviceId, deviceType = :deviceType, dateModified = :time, timezone = :timezone, version = :version 
						WHERE userId = :userId";
			$queryDevice = sbexeculteQueryWithData($sqlDevice,$data);

		}else{
			$queryDevice = TRUE;
		}	

	//If more then one token, delete all and insert a new one
	}else if($row > 1){
		
		//Remove any tokenId that matchs with the one sent
		sbexeculteQuery("DELETE FROM tokenDevice WHERE userId = '$userId'");
		//Insert new token device to table
		$sqlDevice = "	INSERT INTO tokenDevice (userId,deviceId,deviceType,dateModified,timezone,version) 
						VALUES (:userId,:deviceId,:deviceType,:time,:timezone,:version)";
		$queryDevice = sbexeculteQueryWithData($sqlDevice,$data);

	//If none, insert a new token
	}else{
		
		$sqlDevice = "	INSERT INTO tokenDevice (userId,deviceId,deviceType,dateModified,timezone,version) 
						VALUES (:userId,:deviceId,:deviceType,:time,:timezone,:version)";
		$queryDevice = sbexeculteQueryWithData($sqlDevice,$data);
	}

	if($queryDevice){
		$array = array(
			"status" => '1',
			"msgStatus" => 'success',
			"message" => "$msgTokenDeviceUpdateSuccess",
			"WSResponseCode" => "$WSCodeTokenDeviceUpdateSuccess"
		);
	}else{
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgTokenDeviceUpdateFail",
			"WSResponseCode" => "$WSCodeTokenDeviceUpdateFail"
		);
	}

	echo json_encode($array, JSON_PRETTY_PRINT);
	
}

/**
	Method to update device id
*/
function UpdateTimezone() {

	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$data = array(
			"userId" => $_POST['userId'],
			"timezone" => $_POST['timezone']
		);
				
		$sqlTimezone = "UPDATE tokenDevice SET timezone = :timezone WHERE userId = :userId";
		$queryTimezone = sbexeculteQueryWithData($sqlTimezone,$data);

		if($queryTimezone){
			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgTokenDeviceUpdateSuccess",
				"WSResponseCode" => "$WSCodeTokenDeviceUpdateSuccess"
			);
		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgTokenDeviceUpdateFail",
				"WSResponseCode" => "$WSCodeTokenDeviceUpdateFail"
			);
		}

		echo json_encode($array, JSON_PRETTY_PRINT);
	
	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);

		echo json_encode($array, JSON_PRETTY_PRINT);
	}
}

/**
	Method to update device id
*/
function UpdateAppVersion() {

	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$userId = $_POST['userId'];
		$version = $_POST['version'];
		$location = $_POST['location'];
		$pushStatus = 0;
		//Temporary use this for previous user
		//Remove after
		if (isset($_POST['push'])) {
			$pushStatus = $_POST['push'];
		}

		$query = sbexeculteQuery("UPDATE tokenDevice SET version = '$version', location = '$location', push = '$pushStatus' WHERE userId = '$userId'");

		if ($query) {
			$array = array(
					"status" => '1',
					"msgStatus" => 'success',
					"message" => "$msgUserVersionUpdateSuccess",
					"WSResponseCode" => "$WSCodeUserVersionUpdateSuccess"
			);

			echo json_encode($array, JSON_PRETTY_PRINT);	
		
		}else{
		
			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgUserVersionUpdateFail",
				"WSResponseCode" => "$WSCodeUserVersionUpdateFail"	
			);
		
			echo json_encode($array, JSON_PRETTY_PRINT);

		}
		
	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);

		echo json_encode($array, JSON_PRETTY_PRINT);
	}
}


/**
	Method get user status.
*/
function GetUserStatus() {

	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {
		
		$entityId = $_POST['entityId'];

		$sql = "SELECT * FROM user 
				INNER JOIN entityMap ON entityMap.entityId = $entityId AND entityMap.userId = ".$credential['userId']."
				WHERE user.userId = ".$credential['userId']."";

		$query = sbexeculteQuery($sql);
		$row = $query->rowCount();
		
		if($row != 0){

			$fetch = $query->fetch();

			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgUserStatusSuccess",
				"WSResponseCode" => "$WSCodeUserStatusSuccess",
				"userId" => $fetch->userId,
				"userStatus" => $fetch->status,
				"email" => $fetch->email,
				"active" => $fetch->active,
				"firstName" => $fetch->firstName,
				"lastName" => $fetch->lastName,
				"mobile" => $fetch->mobile,
				"mobileEmergency" => isset($fetch->mobileEmergency)?$fetch->mobileEmergency:"",
				"debug" => $fetch->debug,
				"badge" => GetQuantityPushPending($credential['userId']),
				"badgeMessage" => GetQuantityMessagePending($credential['userId']),
				"badgeInDanger" => GetQuantityInDanger($credential['userId'],$fetch->accessLevel,$entityId)
			);
			
		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgUserStatusFail",
				"WSResponseCode" => "$WSCodeUserStatusFail"
			);
		}
				
	}else{

		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	}

	echo json_encode($array, JSON_PRETTY_PRINT);
}

?>
