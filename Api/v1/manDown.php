<?

/** 
	Method to alert team leader when user used the function Man Down from the app
*/
function ManDown(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$data = array(
			"userId" => $_POST['userId'],
			"entityId" => $_POST['entityId'],
			"lat" => $_POST['lat'],
			"lng" => $_POST['lng'],
			"batteryLevel" => $_POST['battery'],			
			"time" => GetCurrentTimeStamp(),
			"timezone" => $_POST['timezone']
		);

		//Load required files
		require '../notification_v1/notification.php';
		require '../notification_v1/functions.php';
		
		//CheckinTypeId = 9 means "user safe"
		$sql = "INSERT INTO manDown (userId,entityId,lat,lng,batteryLevel,time,timezone) 
				VALUES (:userId,:entityId,:lat,:lng,:batteryLevel,:time,:timezone)";
		$manDownId = sbexeculteQueryWithDataReturnId($sql,$data);

		//Get user team leader
		if ($manDownId) {
						
			$userDetail = GetUserInfo($data['userId']);
			$exclamation = html_entity_decode('&#10071;',ENT_NOQUOTES,'UTF-8');
			$msg = $exclamation . "EMERGENCY" . $exclamation . $userDetail->firstName . ' ' . $userDetail->lastName . " has triggered the Man Down alarm";
			
			//Send push to user team leaders
			PushManDown($userDetail,$msg,$manDownId,$data['entityId'],$data['time'],$data['timezone']);

			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgManDownSuccess",
				"WSResponseCode" => "$WSCodeManDownSuccess",
				"manDownId" => $manDownId
			);

		}else{

			$array = array(
				"status" => '0',
				"msgStatus" => "fail",
				"message" => "$msgManDownFail",
				"WSResponseCode" => "$WSCodeManDownFail"
			);
		
		}
		
	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	}

	echo json_encode($array, JSON_PRETTY_PRINT);

}

/** 
	Method to alert team leader when user used the function Man Down from the app
*/
function ManDownDeactivate(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$encodePass = strval(encryptStr($_POST['password']));

		$sql = "SELECT * FROM user WHERE userId = ".$credential['userId']." AND password = '$encodePass'";
		$query = sbexeculteQuery($sql);
		$row = $query->rowCount();

		//Only proceed if user has send the correct password
		if ($row != 0) {
			
			$manDownId = $_POST['manDownId'];
			
			//CheckinTypeId = 9 means "user safe"
			$sqlDeactive = "UPDATE manDown SET active = 0 WHERE manDownId = $manDownId AND userId = ".$credential['userId']."";
			$queryDeactivate = sbexeculteQuery($sqlDeactive);
			$rowDeactive = $queryDeactivate->rowCount();

			//Get user team leader
			if ($rowDeactive != 0) {
				
				//Load required files
				require '../notification_v1/notification.php';
				require '../notification_v1/functions.php';

				$entityId = $_POST['entityId'];
				$time = $_POST['ts'];
				$timezone = $_POST['tz'];

				//Set push notification as resolved
				UpdatePushStatus_v1($credential['userId'],$manDownId,$timezone,9);

				$userDetail = GetUserInfo($credential['userId']);
				$msg = $userDetail->firstName . ' ' . $userDetail->lastName . " has deactived the man down alert.";
				
				//Send push to user team leaders
				PushManDownDeactivate($userDetail,$msg,$manDownId,$entityId,$time,$timezone);

				$array = array(
					"status" => '1',
					"msgStatus" => 'success',
					"message" => "$msgManDownDeactiveSuccess",
					"WSResponseCode" => "$WSCodeManDownDeactiveSuccess"
				);

			}else{

				$array = array(
					"status" => '0',
					"msgStatus" => "fail",
					"message" => "$msgManDownDeactiveFail",
					"WSResponseCode" => "$WSCodeManDownDeactiveFail"
				);
			
			}

		}else{

			$array = array(
				"status" => '0',
				"msgStatus" => "fail",
				"message" => "$msgManDownDeactiveWrongPassword",
				"WSResponseCode" => "$WSCodeManDownDeactiveWrongPassword"
			);

		}
		
	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	}

	echo json_encode($array, JSON_PRETTY_PRINT);

}

?>