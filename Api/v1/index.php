<?php

//error_reporting(E_ERROR | E_WARNING | E_PARSE);
//ini_set('display_errors', 1);
//error_reporting(E_ERROR | E_WARNING | E_PARSE);

/**
IMPORT THE IMPORTANT PHP FILES
*/
require 'sbpdo.php';
require 'database/SBConnection.php';
require 'Slim/Slim.php';

require 'user.php';
require 'entity.php';
require 'site.php';
require 'action.php';
require 'question.php';
require 'email.php';
require 'setting.php';
require 'team.php';
require 'leave.php';
require 'notification.php';
require 'connection.php';
require 'hazard.php';
require 'uploadImg.php';
require 'log.php';
require 'analytics.php';
require 'functions.php';
require 'location.php';
require 'message.php';
require 'documents.php';
require 'manDown.php';

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();

/**
USER
*/
//user login
$app->post('/login/', 'Login');
//user forgot password
$app->post('/forgotPassword/', 'ForgotPassword');
//user registration
$app->post('/registerUser/', 'CreateUser');
//user update info
$app->post('/updateUser/', 'UpdateUser');
//user update password
$app->post('/updateUserPassword/', 'UpdateUserPassword');
//user log
$app->post('/userLog/', 'GetUserLog');
//user log by entity
$app->post('/userLogByEntity/', 'GetUserLogByEntity');
//update device
$app->post('/updateDevice/', 'UpdateDevice');
//update version
$app->post('/updateAppVersion/', 'UpdateAppVersion');
//update timezone
$app->post('/updateTimezone/', 'UpdateTimezone');
//Get user status
$app->post('/userStatus/', 'GetUserStatus');

/**
ENTITY
*/
//get all active entity
$app->post('/entity/', 'GetAllEntity');
//get all entity the user is related to
$app->post('/entityUser/', 'GetAllEntityUser');
//get entity by id
$app->post('/entityId/', 'GetEntityById');
//join entity
$app->post('/joinEntity/', 'JoinEntity');
//delete user entity
$app->post('/deleteUserEntity/', 'DeleteUserEntity');

/**
SITE
*/
//get all sites available for user for entity
$app->post('/entitySite/', 'GetAllEntitySite');
//get site by id
$app->post('/site/', 'GetSiteById');
//Create site
$app->post('/createSite/', 'CreateSite');
//update site
$app->post('/updateSite/', 'UpdateSite');
//delte site
$app->post('/deleteSite/', 'DeleteSite');
//site emergency
$app->post('/emergency/', 'GetSiteEmergency');
//site emergency
$app->post('/createSiteEmergency/', 'CreateSiteEmergency');
//join site
$app->post('/joinSite/', 'JoinSite');
//Site Project
$app->post('/siteProject/', 'GetAllProject');
//Site Log
$app->post('/siteLog/', 'GetSiteLog');
//Site Notes
$app->post('/siteNote/', 'SiteNote');
//Note for site by checkin
$app->post('/createSiteNote/', 'CreateSiteNote');

/**
CHECKIN PROCCESS
*/
//checkin
$app->post('/checkin/', 'CheckIn');
//reportin
$app->post('/reportin/', 'ReportIn');
//checkout
$app->post('/checkout/', 'CheckOut');
//pulse
$app->post('/pulse/', 'SendPulse');
//Update status
$app->post('/userSafe/', 'UserSafe');

/**
CONNECTIONS
*/
//Get all users for entity
$app->post('/userEntity/', 'GetAllUsers');
//Get all users connection
$app->post('/userConnection/', 'GetUserConnection');
//Get all users connection
$app->post('/userSingleConnection/', 'GetUserSingleConnection');
//Add user to connection
$app->post('/addConnection/', 'AddConnection');
//Remove user from connection
$app->post('/removeConnection/', 'RemoveConnection');
//Accept user connection request
$app->post('/acceptConnection/', 'AcceptConnection');
//Reject user connection request
$app->post('/rejectConnection/', 'RejectConnection');

/**
MY TEAM
*/
//Get all team for entity
$app->post('/myTeam/', 'GetMyTeam');
//get all team leader by entity
$app->post('/myTeamLeader/', 'GetUserTeamLeader');
//Add team leader
$app->post('/teamLeader/', 'GetAllTeamLeader');

/**
LEAVE
*/
//Get all leave by user
$app->post('/leave/', 'GetLeaveByUser');
//Get all leave type by entityId
$app->post('/leaveType/', 'GetLeaveTypeByEntity');
//Get all leave by user
$app->post('/leaveSave/', 'SaveLeave');
//Get all leave by user
$app->post('/leaveUpdate/', 'UpdateLeave');
//Get all leave by user
$app->post('/leaveDelete/', 'DeleteLeave');

/**
QUESTION
*/
//question
$app->post('/question/', 'Question');
//answer
$app->post('/answer/', 'Answer');

/**
HAZARD
*/
//Get hazard by id
$app->post('/hazardById','GetHazardById');
//Get hazard by site id
$app->post('/hazard','GetHazardBySite');
//Create hazard
$app->post('/hazardCreate','CreateHazard');
//Create hazard action
$app->post('/hazardActionCreate','CreateHazardAction');
//Update hazard
$app->post('/hazardUpdate','UpdateHazard');
//Update hazard action
$app->post('/hazardActionUpdate','UpdateHazardAction');
//Close hazard
$app->post('/hazardClose','CloseHazard');
//Close hazard action
$app->post('/hazardActionClose','CloseHazardAction');
//Share hazard
$app->post('/hazardShare','ShareHazard');

/** 
MESSAGE
*/
//Get all the messages for user
$app->post('/messageUser', 'GetUserMessage');
//Get messages for id
$app->post('/messageChat/', 'GetMessageChat');
//Send message
$app->post('/messageSend/', 'MessageSend');
//Save message
$app->post('/messageCreate/', 'MessageCreate');
//Leave chat message
$app->post('/messageLeaveChat/', 'MessageLeaveChat');

/** 
DOCUMENTS
*/
//Save to actionLog table
$app->post('/documentsByEntity/', 'DocumentByEntity');
$app->post('/documentsByEntity_v1/', 'DocumentByEntity_v1');

/** 
MAN DOWN
*/
//Send mand down alert
$app->post('/manDown/', 'ManDown');
//Deactive man down alert
$app->post('/manDownDeactivate/', 'ManDownDeactivate');

/** 
LOCATION BACKGROUND
*/
$app->post('/locationSave/', 'SaveLocation');


/**
NOTIFICATION
*/
$app->post('/notification/', 'GetUserNotification');
//Clear all notification
$app->post('/clearAllNotification/', 'ClearAllNotification');
//Clear specific notification
$app->post('/clearNotification/', 'ClearNotification');
//Clear all notification messages
$app->post('/clearAllMessageNotification/', 'ClearAllNotificationMessage');

/** 
LOG
*/
//Save log to log.txt
$app->post('/log/', 'WriteLog');
//Save to actionLog table
$app->post('/logAction/', 'WriteToActionLog');
//Send log to support
$app->post('/sendSupportLog/', 'SendLogToSupport');

/** 
UPLOAD
*/
//Save to actionLog table
$app->post('/imgUpload/', 'ImageUpload');

/** 
ANALYTICS
*/
//User lauch app
$app->post('/analyticsLauchApp/', 'UserLauchApp');
//User close app
$app->post('/analyticsCloseApp/', 'UserCloseApp');

/**
TEMPORARY
*/
//Review user password
$app->post('/reviewPassword/', 'ReviewPassword');


/**
EMPTY ACCESS BLOCK
*/
$app->get(
    '/',
    function () {
        $template = <<<EOT
<!DOCTYPE html>
    <html>
        <head>
            <title>SafetyBeat WebService</title>
        </head>
        <body>
			<p>Access not allowed</p> 
        </body>
    </html>
EOT;
        echo $template;
    }
);

$app->run();

?>
