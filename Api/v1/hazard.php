<?

/**
	Method to get hazards by id
*/
function GetHazardById() {

	require_once 'lang.php';
	require_once "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {
	
		$hazardId = $_POST['hazardId'];

		$sql = "SELECT *,site.name as siteName FROM hazard 
				INNER JOIN site ON hazard.siteId = site.siteId
				WHERE hazard.hazardId = '$hazardId'";
		
		$query = sbexeculteQuery($sql);
		$exist = $query->rowCount();
		
		if($exist != 0){

			$hazard = array();			
			$fetch = $query->fetch();
			
			$hazard['hazardId'] = $fetch->hazardId;
			$hazard['siteId'] = $fetch->siteId;
			$hazard['siteName'] = $fetch->siteName;
			$hazard['userId'] = $fetch->userId;
			$hazard['entityId'] = $fetch->entityId;
			$hazard['title'] = $fetch->title;
			$hazard['description'] = $fetch->description;
			$hazard['precaution'] = $fetch->precaution;
			$hazard['preciseLocation'] = $fetch->preciseLocation;
			$hazard['hazardRisk'] = $fetch->risk;
			$hazard['time'] = $fetch->time;
			$hazard['timezone'] = $fetch->timezone;
			
			$hazard['resolvedBy'] = $fetch->resolvedBy;
			$hazard['resolvedByEntityId'] = $fetch->resolvedByEntityId;
			$hazard['resolvedByTime'] = $fetch->resolvedByTime;
			$hazard['resolvedByTimezone'] = $fetch->resolvedByTimezone;

			//Get actions related with hazard
			$hazard['actions'] = GetHazardAction($fetch->hazardId);
			
			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgHazardDetailSuccess",
				"WSResponseCode" => "$WSCodeHazardDetailSuccess",
				"hazard" => $hazard
			);

			echo json_encode($array, JSON_PRETTY_PRINT);
			
		}else{

			$array = array(
					"status" => '0',
					"msgStatus" => 'fail',
					"message" => "$msgHazardDetailFail",
					"WSResponseCode" => "$WSCodeHazardDetailFail"
				);
			
			echo json_encode($array, JSON_PRETTY_PRINT);

		}
		
	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}

/**
	Method to get all hazards/actions per site
*/
function GetHazardBySite() {

	require_once 'lang.php';
	require_once "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {
	
		$siteId = $_POST['siteId'];

		$sql = "SELECT *,site.name as siteName FROM hazard 
				INNER JOIN site ON hazard.siteId = site.siteId
				WHERE hazard.siteId = '$siteId' ORDER BY hazardId DESC ";
		
		$query = sbexeculteQuery($sql);
		$exist = $query->rowCount();
		
		if($exist != 0){

			$i = 0;
			$loop = array();			

			while($fetch = $query->fetch()){

				$loop[$i]['hazardId'] = $fetch->hazardId;
				$loop[$i]['siteId'] = $fetch->siteId;
				$loop[$i]['siteName'] = $fetch->siteName;
				$loop[$i]['userId'] = $fetch->userId;
				$loop[$i]['entityId'] = $fetch->entityId;
				$loop[$i]['title'] = $fetch->title;
				$loop[$i]['description'] = $fetch->description;
				$loop[$i]['precaution'] = $fetch->precaution;
				$loop[$i]['preciseLocation'] = $fetch->preciseLocation;
				$loop[$i]['hazardRisk'] = $fetch->risk;
				$loop[$i]['time'] = $fetch->time;
				$loop[$i]['timezone'] = $fetch->timezone;
				
				$loop[$i]['resolvedBy'] = $fetch->resolvedBy;
				$loop[$i]['resolvedByEntityId'] = $fetch->resolvedByEntityId;
				$loop[$i]['resolvedByTime'] = $fetch->resolvedByTime;
				$loop[$i]['resolvedByTimezone'] = $fetch->resolvedByTimezone;

				//Get actions related with hazard
				$loop[$i]['actions'] = GetHazardAction($fetch->hazardId);				

				$i++; 	

			}
			
				$array = array(
					"status" => '1',
					"msgStatus" => 'success',
					"message" => "$msgHazardSuccess",
					"WSResponseCode" => "$WSCodeHazardSuccess",
					"hazard" => $loop
				);

				echo json_encode($array, JSON_PRETTY_PRINT);
			
		}else{

			$array = array(
					"status" => '0',
					"msgStatus" => 'fail',
					"message" => "$msgHazardNoHazard",
					"WSResponseCode" => "$WSCodeHazardNoHazard"
				);
			
			echo json_encode($array, JSON_PRETTY_PRINT);

		}
		
	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}


/**
	Method to create hazard
*/
function CreateHazard() {

	require_once 'lang.php';
	require_once "WSResponseCode.php";
	
	//Load required files
	require_once '../notification_v1/notification.php';
	require_once '../notification_v1/functions.php';

	$credential = array(
						"userId" => $_POST['userId'],
						"token" => $_POST['token']
						);
	
	//Validate user access
	if (ValidateAccess($credential)) {
		
		$data = array(
						"userId" => $_POST['userId'],
						"siteId" => $_POST['siteId'],
						"entityId" => $_POST['entityId'],
						"title" => $_POST['title'],
						"description" => $_POST['description'],
						"precaution" => $_POST['precaution'],
						"preciseLocation" => $_POST['preciseLocation'],
						"risk" => $_POST['risk'],
						"time" => GetCurrentTimeStamp(),
						"timezone" => $_POST['timezone']
					);
			
		$sql = "INSERT INTO hazard (siteId,userId,entityId,title,description,precaution,preciseLocation,risk,time,timezone) 
				VALUES (:siteId,:userId,:entityId,:title,:description,:precaution,:preciseLocation,:risk,:time,:timezone)";
		$hazardId = sbexeculteQueryWithDataReturnId($sql,$data);
		
		if ($hazardId) {
			
			$selfManaged = $_POST['selfManaged'];
			
			//Send push if hazard is self managed by entity or if hazard risk is high
			if ($selfManaged || strpos($data['risk'], 'H') !== false) {
				
				//Get hazard detail
				$sqlHazard = "	SELECT *,site.name as siteName FROM hazard 
								INNER JOIN site ON hazard.siteId = site.siteId
								INNER JOIN entity ON hazard.entityId = entity.entityId
								WHERE hazard.hazardId = $hazardId ";
				
				$queryHazard = sbexeculteQuery($sqlHazard);
				$fetchHazard = $queryHazard->fetch();
				
				$userDetail = GetUserInfo($credential['userId']);
				
				if (strpos($data['risk'], 'H') !== false) {
					$message = "A high risk Hazard has been reported on the site " .$fetchHazard->siteName .", please take the appropriated actions to clear the hazard";
				}else{
					$message = "A Hazard has been reported on the site " .$fetchHazard->siteName .", please take the appropriated actions to clear the hazard";
				}

				//Send push to team leader and admin if the hazard risk is High
				PushHazardCreate($userDetail,$fetchHazard,$message,$hazardId,$data['entityId'],$data['time'],$data['timezone']);
			}

			$array = array(
					"status" => '1',
					"msgStatus" => 'success',
					"message" => "$msgHazardSavedSuccess",
					"WSResponseCode" => "$WSCodeHazardSavedSuccess",
					"hazardId" => $hazardId
				);

				echo json_encode($array, JSON_PRETTY_PRINT);


		}else{

			$array = array(
					"status" => '0',
					"msgStatus" => 'fail',
					"message" => "$msgHazardSavedFail",
					"WSResponseCode" => "$WSCodeHazardSavedFail"
				);

				echo json_encode($array, JSON_PRETTY_PRINT);

		}
		
	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}

/**
	Method to create hazard actopm
*/
function CreateHazardAction() {

	require_once 'lang.php';
	require_once "WSResponseCode.php";

	//Load required files
	require_once '../notification_v1/notification.php';
	require_once '../notification_v1/functions.php';

	$inputJSON = file_get_contents('php://input');
	$arrayAction = json_decode($inputJSON, TRUE);	

	$credential = array(
						"userId" => $arrayAction['userId'],
						"token" => $arrayAction['token']
						);
	
	//Validate user access
	if (ValidateAccess($credential)) {

		//Insert into Answers table
		foreach ($arrayAction['action'] as $obj) {
			
			$data = array(
					"hazardId" => $arrayAction['hazardId'],
					"userId" => $arrayAction['userId'],
					"title" => $obj['actionName'],
					"description" => $obj['description'],
					"entityId" => $obj['entityId'],
					"time" => GetCurrentTimeStamp(),
					"timezone" => $obj['timezone'],
					"completeBy" => $obj['completeBy'],
					"completeByTime" => $obj['completeByTime']
					);

			$sql = "INSERT INTO hazardAction (hazardId,userId,title,description,entityId,time,timezone,completeBy,completeByTime) 
					VALUES (:hazardId,:userId,:title,:description,:entityId,:time,:timezone,:completeBy,:completeByTime)";
			$hazardActionId = sbexeculteQueryWithDataReturnId($sql,$data);

			$userDetail = GetUserInfo($obj['completeBy']);
			$message = "A hazard has been created and a action was assigned for you to perform";

			//Send push to user that has an action created
			PushHazardActionCreate($userDetail,$message,$hazardActionId,$obj['entityId'],$obj['time'],$obj['timezone']);

		}
		
		if ($hazardActionId) {
			
			$array = array(
					"status" => '1',
					"msgStatus" => 'success',
					"message" => "$msgHazardActionSavedSuccess",
					"WSResponseCode" => "$WSCodeHazardActionSavedSuccess"
				);

				echo json_encode($array, JSON_PRETTY_PRINT);

		}else{

			$array = array(
					"status" => '0',
					"msgStatus" => 'fail',
					"message" => "$msgHazardActionSavedFail",
					"WSResponseCode" => "$WSCodeHazardActionSavedFail"
				);

				echo json_encode($array, JSON_PRETTY_PRINT);

		}
		
	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}

}


/**
	Method to update hazard
*/
function UpdateHazard() {

	require_once 'lang.php';
	require_once "WSResponseCode.php";

	$credential = array(
						"userId" => $_POST['userId'],
						"token" => $_POST['token']
						);
	
	//Validate user access
	if (ValidateAccess($credential)) {
		
		$data = array(
						"hazardId" => $_POST['hazardId'],
						"title" => $_POST['title'],
						"description" => $_POST['description'],
						"precaution" => $_POST['precaution'],
						"preciseLocation" => $_POST['preciseLocation']
					);

		$sql = "UPDATE hazard SET title = :title,description = :description,precaution = :precaution,preciseLocation = :preciseLocation
				WHERE hazardId = :hazardId";
		$query = sbexeculteQueryWithData($sql,$data);
		
		if ($query) {
			
			$array = array(
					"status" => '1',
					"msgStatus" => 'success',
					"message" => "$msgHazardUpdatedSuccess",
					"WSResponseCode" => "$WSCodeHazardUpdatedSuccess"
				);

				echo json_encode($array, JSON_PRETTY_PRINT);

		}else{

			$array = array(
					"status" => '0',
					"msgStatus" => 'fail',
					"message" => "$msgHazardUpdatedFail",
					"WSResponseCode" => "$WSCodeHazardUpdatedFail"
				);

				echo json_encode($array, JSON_PRETTY_PRINT);

		}
		
	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}


/**
	Method to create hazard actopm
*/
function UpdateHazardAction() {

	require_once 'lang.php';
	require_once "WSResponseCode.php";
	
	$credential = array(
						"userId" => $_POST['userId'],
						"token" => $_POST['token']
						);
	
	//Validate user access
	if (ValidateAccess($credential)) {

		$data = array(
					"hazardActionId" => $_POST['hazardActionId'],
					"userId" => $_POST['userId'],
					"title" => $_POST['title'],
					"description" => $_POST['description']
					);
		
		$sql = "UPDATE hazardAction SET title = :title,userId = :userId,description = :userId 
				WHERE hazardActionId = :hazardActionId";
		$query = sbexeculteQueryWithData($sql,$data);
		
		if ($query) {
			
			$array = array(
					"status" => '1',
					"msgStatus" => 'success',
					"message" => "$msgHazardActionUpdatedSuccess",
					"WSResponseCode" => "$WSCodeHazardActionUpdatedSuccess"
				);

				echo json_encode($array, JSON_PRETTY_PRINT);

		}else{

			$array = array(
					"status" => '0',
					"msgStatus" => 'fail',
					"message" => "$msgHazardActionUpdatedFail",
					"WSResponseCode" => "$WSCodeHazardActionUpdatedFail"
				);

				echo json_encode($array, JSON_PRETTY_PRINT);

		}
		
	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}

}

/**
	Method to close hazard
*/
function CloseHazard() {

	require_once 'lang.php';
	require_once "WSResponseCode.php";
	
	//Load required files
	require_once '../notification_v1/notification.php';
	require_once '../notification_v1/functions.php';

	$credential = array(
						"userId" => $_POST['userId'],
						"token" => $_POST['token']
						);
	
	//Validate user access
	if (ValidateAccess($credential)) {

		$data = array(
					"userId" => $_POST['userId'],
					"hazardId" => $_POST['hazardId'],
					"entityId" => $_POST['entityId'],
					"time" => GetCurrentTimeStamp(),
					"timezone" => $_POST['timezone']
					);
		
		$sql = "UPDATE hazard SET resolvedBy = :userId,resolvedByEntityId = :entityId,resolvedByTime = :time, resolvedByTimezone = :timezone 
				WHERE hazardId = :hazardId";
		$query = sbexeculteQueryWithData($sql,$data);
	
		if ($query) {
			
			$hazardId = $data['hazardId'];

			$sqlHazard = "	SELECT *,site.name as siteName FROM hazard 
							INNER JOIN site ON hazard.siteId = site.siteId
							INNER JOIN entity ON hazard.entityId = entity.entityId
							LEFT JOIN user ON hazard.resolvedBy = user.userId
							WHERE hazard.hazardId = $hazardId ";

			$queryHazard = sbexeculteQuery($sqlHazard);
			$fetchHazard = $queryHazard->fetch();
			
			$userDetail = GetUserInfo($data['userId']);

			//Push handler
			$userDetail = GetUserInfo($data['userId']);
			$userName = $fetchHazard->firstName . " " . $fetchHazard->lastName;
			$message = "The hazard " . $fetchHazard->title . " has been cleared by " . $userName . " on site ". $fetchHazard->siteName;

			//Send push to user that has an action created
			PushHazardClean($fetchHazard,$message,$data['hazardId'],$data['entityId'],$data['time'],$data['timezone']);

			//Set resolved the pending push
			UpdatePushStatus($data['userId'],$data['hazardId'],$data['timezone'],5);

			$array = array(
					"status" => '1',
					"msgStatus" => 'success',
					"message" => "$msgHazardClosedSuccess",
					"WSResponseCode" => "$WSCodeHazardClosedSuccess"
				);

			echo json_encode($array, JSON_PRETTY_PRINT);

		}else{

			$array = array(
					"status" => '0',
					"msgStatus" => 'fail',
					"message" => "$msgHazardActionClosedFail",
					"WSResponseCode" => "$WSCodeHazardActionClosedFail"
				);

			echo json_encode($array, JSON_PRETTY_PRINT);

		}
		
	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}

/**
	Method delete hazard
*/
function CloseHazardAction() {

	require_once 'lang.php';
	require_once "WSResponseCode.php";
	
	//Load required files
	require_once '../notification_v1/notification.php';
	require_once '../notification_v1/functions.php';

	$credential = array(
						"userId" => $_POST['userId'],
						"token" => $_POST['token']
						);
	
	//Validate user access
	if (ValidateAccess($credential)) {

		$data = array(
					"hazardActionId" => $_POST['hazardActionId'],
					"userId" => $_POST['userId'],
					"entityId" => $_POST['entityId'],
					"time" => GetCurrentTimeStamp(),
					"timezone" => $_POST['timezone']
					);
		
		$sql = "UPDATE hazardAction SET resolvedBy = :userId,resolvedByTime = :time,resolvedByTimezone = :timezone,resolvedByEntityId = :entityId 
				WHERE hazardActionId = :hazardActionId";
		$query = sbexeculteQueryWithData($sql,$data);

		if ($query) {
			
			//Set resolved the pending push
			UpdatePushStatus($data['userId'],$data['hazardActionId'],$data['timezone'],5);

			$array = array(
					"status" => '1',
					"msgStatus" => 'success',
					"message" => "$msgHazardActionClosedSuccess",
					"WSResponseCode" => "$WSCodeHazardActionClosedSuccess"
				);

				echo json_encode($array, JSON_PRETTY_PRINT);

		}else{

			$array = array(
					"status" => '0',
					"msgStatus" => 'fail',
					"message" => "$msgHazardActionClosedFail",
					"WSResponseCode" => "$WSCodeHazardActionClosedFail"
				);

				echo json_encode($array, JSON_PRETTY_PRINT);

		}
		
	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}


/**
	Method to share hazard via email
*/
function ShareHazard() {

	require_once 'lang.php';
	require_once "WSResponseCode.php";

	$inputJSON = file_get_contents('php://input');
	$arrayShare = json_decode($inputJSON, TRUE);	

	$credential = array(
						"userId" => $arrayShare['userId'],
						"token" => $arrayShare['token']
						);
	
	//Validate user access
	if (ValidateAccess($credential)) {

		//Insert into Answers table
		foreach ($arrayShare['share'] as $obj) {
			
			$data = array(
					"hazardId" => $arrayShare['hazardId'],
					"personName" => array_key_exists('personName',$obj)?$obj['personName']:'',
					"personEmail" => array_key_exists('personEmail',$obj)?$obj['personEmail']:''
					);

			$sql = "INSERT INTO hazardShare (hazardId,personName,personEmail) 
					VALUES (:hazardId,:personName,:personEmail)";
			$query = sbexeculteQueryWithData($sql,$data);

			$hazardDetail = array(
								"hazardId" => $arrayShare['hazardId'],
								"personName" => array_key_exists('personName',$obj)?$obj['personName']:'',
								"personEmail" => array_key_exists('personEmail',$obj)?$obj['personEmail']:'',
								"siteName" => array_key_exists('siteName',$obj)?$obj['siteName']:''
							);

			//Send email code
			EmailHazardShare($hazardDetail);

		}

		if ($query) {
			
			$array = array(
					"status" => '1',
					"msgStatus" => 'success',
					"message" => "$msgHazardShareSuccess",
					"WSResponseCode" => "$WSCodeHazardShareSuccess"
				);

				echo json_encode($array, JSON_PRETTY_PRINT);

		}else{

			$array = array(
					"status" => '0',
					"msgStatus" => 'fail',
					"message" => "$msgHazardShareFail",
					"WSResponseCode" => "$WSCodeHazardShareFail"
				);

				echo json_encode($array, JSON_PRETTY_PRINT);

		}
		
	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
	
}

?>