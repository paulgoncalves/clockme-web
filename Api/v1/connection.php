<?


/** 
	Method that retrieve all users by entity id
	Also retrieve my connection and return if has connection or not
*/
function GetAllUsers(){
		
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {
		
		$entityId = $_POST['entityId'];

		$sql = "SELECT * FROM entityMap 
				INNER JOIN user on entityMap.userId = user.userId 
				WHERE entityMap.entityId = '$entityId' 
				AND user.active = 1 AND entityMap.userId <> ".$credential['userId']." 
				AND (accessEnd > UNIX_TIMESTAMP() OR accessEnd IS NULL)";

		$query = sbexeculteQuery($sql);
		$exist = $query->rowCount();

		if($exist != 0){
			
			$arrayConnection = array();
			$loop = array();
			$i = 0;

			//Fetch leader id for user
			$sqlConnection = "	SELECT * FROM reportTo 
								WHERE (leaderId = ".$credential['userId']." OR followerId = ".$credential['userId'].")
								AND active <> 0
								AND entityId = '$entityId'";

			$queryConnection = sbexeculteQuery($sqlConnection);

			while ($fetchConnection = $queryConnection->fetch()) {
				
				$arrayConnection[$i]['reportToId'] = $fetchConnection->reportToId;
				$arrayConnection[$i]['followerId'] = $fetchConnection->followerId;
				$arrayConnection[$i]['leaderId'] = $fetchConnection->leaderId;
				$arrayConnection[$i]['requestBy'] = $fetchConnection->requestBy;
				$arrayConnection[$i]['permission'] = $fetchConnection->permission;
				$arrayConnection[$i]['active'] = $fetchConnection->active;
				$i++;
			}
			
			$i = 0;
			while($fetch = $query->fetch()){
				
				$loop[$i]['entityId'] = $fetch->entityId;
				$loop[$i]['userId'] = $fetch->userId;
				$loop[$i]['accessEnd'] = $fetch->accessEnd;
				$loop[$i]['accessLevel'] = $fetch->accessLevel;
				$loop[$i]['firstName'] = $fetch->firstName;
				$loop[$i]['lastName'] = $fetch->lastName;
				$loop[$i]['email'] = $fetch->email;
				$loop[$i]['mobile'] = $fetch->mobile;
				$loop[$i]['active'] = $fetch->active;
				$loop[$i]['isConnected'] = "0";
				$loop[$i]['pendingRequest'] = "0";
				///test
				$x = 0;
				$connection = array();

				//Check if leaderId exist
				foreach ($arrayConnection as $arr) {
					
					if ($fetch->userId == $arr['leaderId'] || $fetch->userId == $arr['followerId']) {

						$connection[$x]['reportToId'] = $arr['reportToId'];
						$connection[$x]['followerId'] = $arr['followerId'];
						$connection[$x]['leaderId'] = $arr['leaderId'];
						$connection[$x]['requestBy'] = $arr['requestBy'];
						$connection[$x]['permission'] = $arr['permission'];
						$connection[$x]['active'] = $arr['active'];

						//Check if user friend is pending
						if ($arr['active'] == 1) {
							$loop[$i]['pendingRequest'] = "0";
						}else{
							$loop[$i]['pendingRequest'] = "1";
						}

						$x++;
					}
				}
				
				if (count($connection) != 0) {
					$loop[$i]['isConnected'] = "1";	
					$loop[$i]['connectionDetail'] = $connection;
					$connection = null;
				}

				$i++;
			}

			$array = array(
				"status" => '1',
				"msgStatus" => "success",
				"message" => "$msgConnectionFetchSuccess",
				"WSResponseCode" => "$WSCodeConnectionFetchSuccess",
				"connection" => $loop
			);
			
			echo json_encode($array, JSON_PRETTY_PRINT);	
			
		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => "fail",
				"message" => "$msgConnectionNoExist",
				"WSResponseCode" => "$WSCodeConnectionNoExist"
			);

			echo json_encode($array, JSON_PRETTY_PRINT);	
		}

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}


/** 
	Method that retrieve all user connections
*/
function GetUserConnection(){
		
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {
		
		$entityId = $_POST['entityId'];
		$connectionFor = $_POST['connectionFor'];

		//Get who is the user following
		if ($connectionFor) {
			$connectedTo = $connectionFor;
		}else{
			$connectedTo = $credential['userId'];
		}
		
		$query = GetAllConnections($connectedTo,$entityId);
		$exist = $query->rowCount();
		
		if($exist){
			$i=$j=$k=$l=0;
			$pending = array();
			$leader = array();
			$follower = array();
			$member = array();

			while($fetch = $query->fetch()){
				
				//Check if user friend is pending
				if ($fetch->request == 2) {
				
						$pending[$i]['reportToId'] = $fetch->reportToId;
						$pending[$i]['followerId'] = $fetch->followerId;
						$pending[$i]['leaderId'] = $fetch->leaderId;
						$pending[$i]['entityId'] = $fetch->entityId;
						$pending[$i]['permission'] = $fetch->permission;
						$pending[$i]['accessLevel'] = $fetch->accessLevel;
						$pending[$i]['requestBy'] = $fetch->requestBy;
						$pending[$i]['userId'] = $fetch->userId;
						$pending[$i]['firstName'] = $fetch->firstName;
						$pending[$i]['lastName'] = $fetch->lastName;
						$pending[$i]['email'] = $fetch->email;
						$pending[$i]['mobile'] = $fetch->mobile;
						$pending[$i]['active'] = $fetch->active;
						$pending[$i]['pendingRequest'] = "1";
						$pending[$i]['isConnected'] = "0";
						$i++;										
				}else{					

					//Array with member
					if ($fetch->permission == 1) {
						
						$insert = true;
						foreach ($member as $arr) {
							if (($arr['leaderId'] == $connectedTo && $arr['followerId'] == $fetch->userId) || ($arr['leaderId'] == $fetch->userId && $arr['followerId'] == $connectedTo)) {
								$insert = false;
								break;
							}
						}

						if ($insert) {
							$member[$l]['reportToId'] = $fetch->reportToId;
							$member[$l]['followerId'] = $fetch->followerId;
							$member[$l]['leaderId'] = $fetch->leaderId;
							$member[$l]['entityId'] = $fetch->entityId;
							$member[$l]['permission'] = $fetch->permission;
							$member[$l]['accessLevel'] = $fetch->accessLevel;
							$member[$l]['requestBy'] = $fetch->requestBy;
							$member[$l]['userId'] = $fetch->userId;
							$member[$l]['firstName'] = $fetch->firstName;
							$member[$l]['lastName'] = $fetch->lastName;
							$member[$l]['email'] = $fetch->email;
							$member[$l]['mobile'] = $fetch->mobile;
							$member[$l]['active'] = $fetch->active;
							$member[$l]['pendingRequest'] = "0";
							$member[$l]['isConnected'] = "1";
							$l++;		
						}
						

					//Array with follower
					}else if ($fetch->permission == 2) {
						
						if ($fetch->followerId == $connectedTo) {
							$leader[$j]['reportToId'] = $fetch->reportToId;
							$leader[$j]['followerId'] = $fetch->followerId;
							$leader[$j]['leaderId'] = $fetch->leaderId;
							$leader[$j]['entityId'] = $fetch->entityId;
							$leader[$j]['permission'] = $fetch->permission;
							$leader[$j]['accessLevel'] = $fetch->accessLevel;
							$leader[$j]['requestBy'] = $fetch->requestBy;
							$leader[$j]['userId'] = $fetch->userId;
							$leader[$j]['firstName'] = $fetch->firstName;
							$leader[$j]['lastName'] = $fetch->lastName;
							$leader[$j]['email'] = $fetch->email;
							$leader[$j]['mobile'] = $fetch->mobile;
							$leader[$j]['active'] = $fetch->active;
							$leader[$j]['pendingRequest'] = "0";
							$leader[$j]['isConnected'] = "1";
							$j++;	
						}else{
							$follower[$k]['reportToId'] = $fetch->reportToId;
							$follower[$k]['followerId'] = $fetch->followerId;
							$follower[$k]['leaderId'] = $fetch->leaderId;
							$follower[$k]['entityId'] = $fetch->entityId;
							$follower[$k]['permission'] = $fetch->permission;
							$follower[$k]['accessLevel'] = $fetch->accessLevel;
							$follower[$k]['requestBy'] = $fetch->requestBy;
							$follower[$k]['userId'] = $fetch->userId;
							$follower[$k]['firstName'] = $fetch->firstName;
							$follower[$k]['lastName'] = $fetch->lastName;
							$follower[$k]['email'] = $fetch->email;
							$follower[$k]['mobile'] = $fetch->mobile;
							$follower[$k]['active'] = $fetch->active;
							$follower[$k]['pendingRequest'] = "0";
							$follower[$k]['isConnected'] = "1";
							$k++;							
						}

					}
					
				}

			}

			$array = array(
				"status" => '1',
				"msgStatus" => "success",
				"message" => "$msgConnectionUserFetchSuccess",
				"WSResponseCode" => "$WSCodeConnectionUserFetchSuccess"
			);

			$x = 0;
			if (count($leader) != 0) {
				$array['myConnections']['Leader'] = $leader;
				$x++;
			}
			if (count($follower) != 0) {
				$array['myConnections']['Follower'] = $follower;
				$x++;
			}
			if (count($member) != 0) {
				$array['myConnections']['Member'] = $member;
				$x++;
			}
			if (count($pending) != 0) {
				$array['myConnections']['Pending'] = $pending;
				$x++;
			}

			echo json_encode($array, JSON_PRETTY_PRINT);

		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => "fail",
				"message" => "$msgConnectionUserNoExist",
				"WSResponseCode" => "$WSCodeConnectionUserNoExist"
			);

			echo json_encode($array, JSON_PRETTY_PRINT);
		}

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}

/** 
	Method that retrieve user connection with specify user
*/
function GetUserSingleConnection(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {
		
		$connectedTo = $_POST['connectedTo'];
		$entityId = $_POST['entityId'];
		$accessLevel = $_POST['accessLevel'];

		//If user is admin grant access to everyone's information
		if ($accessLevel == 1) {
			
			$sql = "SELECT * FROM user 
					INNER JOIN entityMap ON entityMap.userId = user.userId AND entityMap.entityId = '$entityId' AND (accessEnd > UNIX_TIMESTAMP() OR accessEnd IS NULL)
					WHERE user.userId = '$connectedTo'";

		}else{

			$sql = "SELECT *,reportTo.active AS request FROM reportTo 
					INNER JOIN user ON user.userId = 
						CASE
							WHEN reportTo.followerId = ".$credential['userId']." 
								THEN reportTo.leaderId 
							ELSE reportTo.followerId 
						END
					INNER JOIN entityMap ON entityMap.userId = user.userId AND entityMap.entityId = '$entityId' AND (accessEnd > UNIX_TIMESTAMP() OR accessEnd IS NULL)
					WHERE ((reportTo.followerId = ".$credential['userId']." AND reportTo.leaderId = '$connectedTo') 
					OR (reportTo.followerId = '$connectedTo' AND reportTo.leaderId = ".$credential['userId']."))
					AND reportTo.active = 1";
		}
		

		$query = sbexeculteQuery($sql);
		$exist = $query->rowCount();
		
		if($exist){
			
			$i=$j=$k=$l=0;
			$pending = array();
			$leader = array();
			$follower = array();
			$member = array();
			
			while($fetch = $query->fetch()){
				
				if ($accessLevel == 1) {
					
					$follower[$k]['reportToId'] = $credential['userId'];
					$follower[$k]['followerId'] = $connectedTo;
					$follower[$k]['leaderId'] = $credential['userId'];
					$follower[$k]['entityId'] = $entityId;
					$follower[$k]['permission'] = "1";
					$follower[$k]['accessLevel'] = $fetch->accessLevel;
					$follower[$k]['requestBy'] = $credential['userId'];
					$follower[$k]['userId'] = $fetch->userId;
					$follower[$k]['firstName'] = $fetch->firstName;
					$follower[$k]['lastName'] = $fetch->lastName;
					$follower[$k]['email'] = $fetch->email;
					$follower[$k]['mobile'] = $fetch->mobile;
					$follower[$k]['status'] = $fetch->status;
					$follower[$k]['active'] = $fetch->active;
					$follower[$k]['pendingRequest'] = "0";
					$follower[$k]['isConnected'] = "1";

					//Get user last action if he is in danger
					if ($fetch->status > 3 && $fetch->status < 7) {
						
						$action = UserLastAction($fetch->userId);
						
						$follower[$k]['siteName'] = $action->name;
						$follower[$k]['batteryLevel'] = $action->batteryLevel;
						$follower[$k]['time'] = $action->time;
					}

					$k++;		

				}else{

					//Check if user friend is pending
					if ($fetch->request == 2) {
					
							$pending[$i]['reportToId'] = $fetch->reportToId;
							$pending[$i]['followerId'] = $fetch->followerId;
							$pending[$i]['leaderId'] = $fetch->leaderId;
							$pending[$i]['entityId'] = $fetch->entityId;
							$pending[$i]['permission'] = $fetch->permission;
							$pending[$i]['accessLevel'] = $fetch->accessLevel;
							$pending[$i]['requestBy'] = $fetch->requestBy;
							$pending[$i]['userId'] = $fetch->userId;
							$pending[$i]['firstName'] = $fetch->firstName;
							$pending[$i]['lastName'] = $fetch->lastName;
							$pending[$i]['email'] = $fetch->email;
							$pending[$i]['mobile'] = $fetch->mobile;
							$pending[$i]['status'] = $fetch->status;
							$pending[$i]['active'] = $fetch->active;
							$pending[$i]['pendingRequest'] = "1";
							$pending[$i]['isConnected'] = "0";

							//Get user last action if he is in danger
							if ($fetch->status > 3 && $fetch->status < 7) {
						
								$action = UserLastAction($fetch->userId);

								$pending[$i]['siteName'] = $action->name;
								$pending[$i]['batteryLevel'] = $action->batteryLevel;
								$pending[$i]['time'] = $action->time;

							}

							$i++;										
					}else{					

						//Array with member and request user is not admin
						if ($fetch->permission == 1) {
							
							$insert = true;
							foreach ($member as $arr) {
								if (($arr['leaderId'] == $connectedTo && $arr['followerId'] == $fetch->userId) || ($arr['leaderId'] == $fetch->userId && $arr['followerId'] == $connectedTo)) {
									$insert = false;
									break;
								}
							}

							if ($insert) {
								$member[$l]['reportToId'] = $fetch->reportToId;
								$member[$l]['followerId'] = $fetch->followerId;
								$member[$l]['leaderId'] = $fetch->leaderId;
								$member[$l]['entityId'] = $fetch->entityId;
								$member[$l]['permission'] = $fetch->permission;
								$member[$l]['accessLevel'] = $fetch->accessLevel;
								$member[$l]['requestBy'] = $fetch->requestBy;
								$member[$l]['userId'] = $fetch->userId;
								$member[$l]['firstName'] = $fetch->firstName;
								$member[$l]['lastName'] = $fetch->lastName;
								$member[$l]['email'] = $fetch->email;
								$member[$l]['mobile'] = $fetch->mobile;
								$member[$l]['status'] = $fetch->status;
								$member[$l]['active'] = $fetch->active;
								$member[$l]['pendingRequest'] = "0";
								$member[$l]['isConnected'] = "1";

								//Get user last action if he is in danger
								if ($fetch->status > 3 && $fetch->status < 7) {
						
									$action = UserLastAction($fetch->userId);

									$member[$l]['siteName'] = $action->name;
									$member[$l]['batteryLevel'] = $action->batteryLevel;
									$member[$l]['time'] = $action->time;

								}

								$l++;		
							}
							

						//Array with follower
						}else if ($fetch->permission == 2) {
							
							if ($fetch->followerId == $connectedTo) {

								$leader[$j]['reportToId'] = $fetch->reportToId;
								$leader[$j]['followerId'] = $fetch->followerId;
								$leader[$j]['leaderId'] = $fetch->leaderId;
								$leader[$j]['entityId'] = $fetch->entityId;
								$leader[$j]['permission'] = $fetch->permission;
								$leader[$j]['accessLevel'] = $fetch->accessLevel;
								$leader[$j]['requestBy'] = $fetch->requestBy;
								$leader[$j]['userId'] = $fetch->userId;
								$leader[$j]['firstName'] = $fetch->firstName;
								$leader[$j]['lastName'] = $fetch->lastName;
								$leader[$j]['email'] = $fetch->email;
								$leader[$j]['mobile'] = $fetch->mobile;
								$leader[$j]['status'] = $fetch->status;
								$leader[$j]['active'] = $fetch->active;
								$leader[$j]['pendingRequest'] = "0";
								$leader[$j]['isConnected'] = "1";

								//Get user last action if he is in danger
								if ($fetch->status > 3 && $fetch->status < 7) {
						
									$action = UserLastAction($fetch->userId);

									$leader[$j]['siteName'] = $action->name;
									$leader[$j]['batteryLevel'] = $action->batteryLevel;
									$leader[$j]['time'] = $action->time;

								}

								$j++;	

							}else{
								
								$follower[$k]['reportToId'] = $fetch->reportToId;
								$follower[$k]['followerId'] = $fetch->followerId;
								$follower[$k]['leaderId'] = $fetch->leaderId;
								$follower[$k]['entityId'] = $fetch->entityId;
								$follower[$k]['permission'] = $fetch->permission;
								$follower[$k]['accessLevel'] = $fetch->accessLevel;
								$follower[$k]['requestBy'] = $fetch->requestBy;
								$follower[$k]['userId'] = $fetch->userId;
								$follower[$k]['firstName'] = $fetch->firstName;
								$follower[$k]['lastName'] = $fetch->lastName;
								$follower[$k]['email'] = $fetch->email;
								$follower[$k]['mobile'] = $fetch->mobile;
								$follower[$k]['status'] = $fetch->status;
								$follower[$k]['active'] = $fetch->active;
								$follower[$k]['pendingRequest'] = "0";
								$follower[$k]['isConnected'] = "1";

								//Get user last action if he is in danger
								if ($fetch->status > 3 && $fetch->status < 7) {
						
									$action = UserLastAction($fetch->userId);

									$follower[$k]['siteName'] = $action->name;
									$follower[$k]['batteryLevel'] = $action->batteryLevel;
									$follower[$k]['time'] = $action->time;

								}

								$k++;							
							}

						}
					}
				}
			}


			$array = array(
				"status" => '1',
				"msgStatus" => "success",
				"message" => "$msgConnectionUserSingleFetchSuccess",
				"WSResponseCode" => "$WSCodeConnectionUserSingleFetchSuccess",
				);

			$x = 0;
			if (count($leader) != 0) {
				$array['myConnections']['Leader'] = $leader;
				$x++;
			}
			if (count($follower) != 0) {
				$array['myConnections']['Follower'] = $follower;
				$x++;
			}
			if (count($member) != 0) {
				$array['myConnections']['Member'] = $member;
				$x++;
			}
			if (count($pending) != 0) {
				$array['myConnections']['Pending'] = $pending;
				$x++;
			}

			echo json_encode($array, JSON_PRETTY_PRINT);

		}else{

			$loop = array();

			$sqlUser = "SELECT * FROM entityMap 
						INNER JOIN user on entityMap.userId = user.userId 
						AND user.active = 1 AND entityMap.userId = '$connectedTo'
						AND (accessEnd > UNIX_TIMESTAMP() OR accessEnd IS NULL)";

			$queryUser = sbexeculteQuery($sqlUser);
			$rowUser = $queryUser->rowCount();

			if ($rowUser) {
				
				$fetchUser = $queryUser->fetch();
				
				$loop['entityId'] = $fetchUser->entityId;
				$loop['userId'] = $fetchUser->userId;
				$loop['accessEnd'] = $fetchUser->accessEnd;
				$loop['accessLevel'] = $fetchUser->accessLevel;
				$loop['firstName'] = $fetchUser->firstName;
				$loop['lastName'] = $fetchUser->lastName;
				$loop['email'] = $fetchUser->email;
				$loop['mobile'] = $fetchUser->mobile;
				$loop['status'] = $fetchUser->status;
				$loop['active'] = $fetchUser->active;
				$loop['isConnected'] = "0";
				$loop['pendingRequest'] = "0";	

				$array = array(
					"status" => '1',
					"msgStatus" => "success",
					"message" => "$msgConnectionUserSingleFetchSuccess",
					"WSResponseCode" => "$WSCodeConnectionUserSingleFetchSuccess",
					"myConnections" => $loop
				);

				echo json_encode($array, JSON_PRETTY_PRINT);

			}else{

				$array = array(
					"status" => '0',
					"msgStatus" => "fail",
					"message" => "$msgConnectionUserNoExist",
					"WSResponseCode" => "$WSCodeConnectionUserNoExist"
				);

				echo json_encode($array, JSON_PRETTY_PRINT);

			}
			
		}

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}

/**
	Method to add person to connection
*/
function AddConnection(){
	
	require 'lang.php';
	require "WSResponseCode.php";
	//Load required files
	require '../notification_v1/notification.php';
	require '../notification_v1/functions.php';

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$data = array(
			"userId" => $_POST['userId'],
			"userConnection" => $_POST['userConnection'],
			"permission" => $_POST['permission'],
			"entityId" => $_POST['entityId'],
			"requestBy" => $_POST['userId']
		);

		if ($data['permission'] == 3) {
			$sql = "SELECT * FROM reportTo WHERE ((followerId = ".$data['userId'].") AND (leaderId = ".$data['userConnection'].")) AND entityId = ".$data['entityId']." AND active = 1";
			$check = sbexeculteQuery($sql);	
		}else{
			$sql = "SELECT * FROM reportTo WHERE ((followerId = ".$data['userConnection'].") AND (leaderId = ".$data['userId'].")) AND entityId = ".$data['entityId']." AND active = 1";
			$check = sbexeculteQuery($sql);
		}

		//Get user info
		$userDetail = GetUserInfo($data['userId']);
		$time = GetCurrentTimeStamp();
		$timezone = isset($_POST['tz'])?$_POST['tz']:GetCurrentTimeStamp();

		if ($data['permission'] == 3) {					
			$message = $userDetail->firstName . ' ' . $userDetail->lastName . " sent a request for you to be their leader.";
		}else if($data['permission'] == 2){
			$message = $userDetail->firstName . ' ' . $userDetail->lastName . " sent a request to be your leader.";
		}else{
			$message = $userDetail->firstName . ' ' . $userDetail->lastName . " sent a team membership request.";
		}
		
		$fetch = $check->fetch();
		$row = $check->rowCount();

		if ($row == 1) {
			print_r($fetch);
			return;	
			if ($data['permission'] == 3) {
				$data['permission'] = 2;
				$sql = "UPDATE reportTo 
						SET followerId = ".$data['userId'].", leaderId = ".$data['userConnection'].", requestBy = ".$data['userId'].", active = '2',permission = ".$data['permission']."
						WHERE reportToId = '$fetch->reportToId'";
				$query = sbexeculteQuery($sql);			
			}else{
				$sql = "UPDATE reportTo 
						SET followerId = ".$data['userConnection'].", leaderId = ".$data['userId'].", requestBy = ".$data['userId'].", active = '2',permission = ".$data['permission']."
						WHERE reportToId = '$fetch->reportToId'";
				$query = sbexeculteQuery($sql);
			}
			
			$notiRef = $fetch->reportToId;

		}else{

			//Depending on what permission, change who is follower and who is leader
			//Team leader or follower has the permission = 2 just the followerId and leaderId get change
			if ($data['permission'] == 3) {
				//Request to be follower
				$data['permission'] = 2;
				$sql = "INSERT INTO reportTo (followerId,leaderId,requestBy,entityId,permission) 
						VALUES (:userId,:userConnection,:requestBy,:entityId,:permission)";
				$notiRef = sbexeculteQueryWithDataReturnId($sql,$data);
			}else {
				//Request to be follower
				$sql = ("INSERT INTO reportTo (followerId,leaderId,requestBy,entityId,permission) 
						VALUES (:userConnection,:userId,:requestBy,:entityId,:permission)");
				$notiRef = sbexeculteQueryWithDataReturnId($sql,$data);
			}

		}

		//If success procced to sent push and return the json
		if($notiRef){

			//Send push to user that receive the invite
			PushConnectionRequest($data['userId'],$data['userConnection'],$message,$notiRef,$data['entityId'],$time,$timezone);

			$array = array(
				"status" => '1',
				"msgStatus" => "success",
				"message" => "$msgConnectionAddSuccess",
				"WSResponseCode" => "$WSCodeConnectionAddSuccess",
				"connection" => GetConnectionForUser($data['userId'],$data['userConnection'])
			);
		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => "fail",
				"message" => "$msgConnectionAddFail",
				"WSResponseCode" => "$WSCodeConnectionAddFail"
			);
		}

		echo json_encode($array, JSON_PRETTY_PRINT);

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}

/**
	Method to Remove person from reportTo
*/
function RemoveConnection(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	//Load required files
	require '../notification_v1/notification.php';
	require '../notification_v1/functions.php';

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$userId = $_POST['userId'];
		$reportToId = $_POST['reportToId'];
		$userConnection = $_POST['userConnection'];
		$timezone = $_POST['tz'];

		$sql = "UPDATE reportTo 
				SET active = '0' 
				WHERE (leaderId = '$userId' AND followerId = '$userConnection') 
				OR (leaderId = '$userConnection' AND followerId = '$userId') ";

		$update = sbexeculteQuery($sql);
		
		if($update){
			
			//Set resolved the pending push
			UpdatePushStatus_v1($userId,$reportToId,$timezone,1);

			$array = array(
				"status" => '1',
				"msgStatus" => "success",
				"message" => "$msgConnectionRemoveSuccess",
				"WSResponseCode" => "$WSCodeConnectionRemoveSuccess",
				"connection" => GetConnectionForUser($userId,$userConnection)
			);

		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => "fail",
				"message" => "$msgConnectionRemoveFail",
				"WSResponseCode" => "$WSCodeConnectionRemoveFail"
			);
		}	

		echo json_encode($array, JSON_PRETTY_PRINT);

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}

/**
	Method to accept connection request
*/
function AcceptConnection(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	//Load required files
	require '../notification_v1/notification.php';
	require '../notification_v1/functions.php';

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$userId = $_POST['userId'];
		$reportToId = $_POST['reportToId'];
		$timezone = $_POST['tz'];
		
		$sql = "SELECT * FROM reportTo WHERE reportToId = '$reportToId'";
		$check = sbexeculteQuery($sql);
		$row = $check->rowCount();

		if ($row == 1) {
			
			$fetch = $check->fetch();

			if ($fetch->active == 2) {

				//Accept user as friend
				$sqlAccept = "	UPDATE reportTo 
								SET active = '1'
								WHERE reportToId = '$reportToId'";
				
				$query = sbexeculteQuery($sqlAccept);

				if($query){

					//Add second connection if doesnt exist
					$sqlAddConnection = "SELECT * FROM reportTo WHERE reportToId = '$reportToId'";
					$queryUser = sbexeculteQuery($sqlAddConnection);
					$fetchUser = $queryUser->fetch();

					$sqlConn = "SELECT * FROM reportTo 
								WHERE ((followerId = '$fetchUser->followerId' AND leaderId = '$fetchUser->leaderId') 
								OR (followerId = '$fetchUser->leaderId' AND leaderId = '$fetchUser->followerId')) 
								AND active <> 0";

					$queryConn = sbexeculteQuery($sqlConn);
					$qntConn = $queryConn->rowCount();

					//Insert if exist only 1 connection between leader and follower
					//It will invert the order of who is leader or follower to create the connection the oposite direction
					if ($qntConn < 2) {

						$data = array(
							"followerId" => $fetch->leaderId,
							"leaderId" => $fetch->followerId,
							"requestBy" => $userId,
							"entityId" => $fetch->entityId,
							"permission" => 1,
							"active" =>1
							);
						
						$sqlInsert = "	INSERT INTO reportTo (followerId,leaderId,requestBy,entityId,permission,active) 
										VALUES (:followerId,:leaderId,:requestBy,:entityId,:permission,:active)";
						$query = sbexeculteQueryWithData($sqlInsert,$data);
					}					

					$notiType = 21; 			
					$time = GetCurrentTimeStamp();
					$notiRef = $fetch->reportToId;

					//Get user info
					$userDetail = GetUserInfo($userId);
					$message = $userDetail->firstName . ' ' . $userDetail->lastName . " accepted your team membership request.";					

					//Send push to user that sent request
					PushConnectionAccept($userDetail,$fetch->requestBy,$message,$notiRef,$fetch->entityId,$time,$timezone);

					//Set resolved the pending push
					UpdatePushStatus_v1($userId,$notiRef,$timezone,1);
					
					$array = array(
						"status" => '1',
						"msgStatus" => "success",
						"message" => "$msgConnectionAcceptSuccess",
						"WSResponseCode" => "$WSCodeConnectionAcceptSuccess"
					);
				}else{
					$array = array(
						"status" => '0',
						"msgStatus" => "fail",
						"message" => "$msgConnectionAcceptFail",
						"WSResponseCode" => "$WSCodeConnectionAcceptFail"
					);
				}

			}else if($fetch->active == 1){
				
				//User is friend already
				$array = array(
					"status" => '0',
					"msgStatus" => "fail",
					"message" => "$msgConnectionPreviousAccepted",
					"WSResponseCode" => "$WSCodeConnectionPreviousAccepted"
				);

			}else{

				//User rejected the 
				$array = array(
					"status" => '0',
					"msgStatus" => "fail",
					"message" => "$msgConnectionAcceptFail",
					"WSResponseCode" => "$WSCodeConnectionAcceptFail"
				);				

			}

		}else{

			$array = array(
				"status" => '0',
				"msgStatus" => "fail",
				"message" => "$msgConnectionAcceptFail",
				"WSResponseCode" => "$WSCodeConnectionAcceptFail"
			);

		}

		echo json_encode($array, JSON_PRETTY_PRINT);

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}

/**
	Method to reject connection request
*/
function RejectConnection(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	//Load required files
	require '../notification_v1/notification.php';
	require '../notification_v1/functions.php';

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$userId = $_POST['userId'];
		$reportToId = $_POST['reportToId'];
		
		//Temporary solution for this issue
		if (isset($_POST['tz'])) {
			$timezone = $_POST['tz'];	
		}else{
			$timezone = 'Australia/Melbourne';
		}
		

		$sqlCheck = "SELECT * FROM reportTo WHERE reportToId = '$reportToId'";
		$check = sbexeculteQuery($sqlCheck);
		$row = $check->rowCount();

		if ($row == 1) {
			
			$fetch = $check->fetch();

			if ($fetch->active == 2) {

				//Accept user as friend
				$sql = "UPDATE reportTo SET active = '0' WHERE reportToId = '$reportToId'";
				$query = sbexeculteQuery($sql);

				//Set resolved the pending push
				UpdatePushStatus_v1($userId,$fetch->reportToId,$timezone,1);

				if($query){
					$array = array(
						"status" => '1',
						"msgStatus" => "success",
						"message" => "$msgConnectionRejectSucess",
						"WSResponseCode" => "$WSCodeConnectionRejectSuccess"
					);
				}else{
					$array = array(
						"status" => '0',
						"msgStatus" => "fail",
						"message" => "$msgConnectionRejectFail",
						"WSResponseCode" => "$WSCodeConnectionRejectFail"
					);
				}

			}else if($fetch->active == 1){
				
				//User is friend already
				$array = array(
					"status" => '0',
					"msgStatus" => "fail",
					"message" => "$msgConnectionPreviousAccepted",
					"WSResponseCode" => "$WSCodeConnectionPreviousAccepted"
				);

			}else{

				//User rejected the request
				$array = array(
					"status" => '0',
					"msgStatus" => "fail",
					"message" => "$msgConnectionNoExist",
					"WSResponseCode" => "$WSCodeConnectionNoExist"
				);				

			}

		}else{

			$array = array(
				"status" => '0',
				"msgStatus" => "fail",
				"message" => "$msgFriendRequestNoExist",
				"WSResponseCode" => "$WSCodeConnection"
			);

		}

		echo json_encode($array, JSON_PRETTY_PRINT);

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}


?>