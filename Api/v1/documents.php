<?php

/** 
	Method that retrieve all the site based on entity id
*/
function DocumentByEntity(){
		
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$entityId = $_POST['entityId'];
		
		$directory = '/var/zpanel/hostdata/zadmin/public_html/assets/documents/'.$entityId.'/';
		
		if (file_exists($directory)) {
    		
    		$documents = array_diff(scandir($directory,1), array('..', '.'));
	
			if (count($documents)) {
					
				$array = array(
					"status" => '1',
					"msgStatus" => 'success',
					"message" => "$msgDocumentsFetchSuccess",
					"WSResponseCode" => "$WSCodeDocumentsFetchSuccess",
					"documents" => $documents
				);
				
				echo json_encode($array, JSON_PRETTY_PRINT);	
			
			}else{

				$array = array(
					"status" => '0',
					"msgStatus" => 'fail',
					"message" => "$msgDocumentsFetchFail",
					"WSResponseCode" => "$WSCodeDocumentsFetchFail"
				);

				echo json_encode($array, JSON_PRETTY_PRINT);	

			}
	
		}else{

				$array = array(
					"status" => '0',
					"msgStatus" => 'fail',
					"message" => "$msgDocumentsFetchFail",
					"WSResponseCode" => "$WSCodeDocumentsFetchFail"
				);

				echo json_encode($array, JSON_PRETTY_PRINT);	
		}

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}

/** 
	Method that retrieve all the site based on entity id
*/
function DocumentByEntity_v1(){
		
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$entityId = $_POST['entityId'];
		$sql = "SELECT * FROM docs 
				WHERE entityId = '$entityId' 
				OR userId = ".$credential['userId']."";

		$query = sbexeculteQuery($sql);
		$exist = $query->rowCount();

		if($exist != 0){
			
			$arrayDocuments = array();
			$i = 0;

			while ($fetch = $query->fetch()) {
				
				$arrayDocuments[$i]['docId'] = $fetch->docId;
				$arrayDocuments[$i]['title'] = $fetch->title;
				$arrayDocuments[$i]['document'] = $fetch->document;
				$arrayDocuments[$i]['refTypeId'] = $fetch->refTypeId;
				$arrayDocuments[$i]['refId'] = $fetch->refId;
				$arrayDocuments[$i]['entityId'] = $fetch->entityId;
				$arrayDocuments[$i]['userId'] = $fetch->userId;
				$i++;
			}
	
			if (count($arrayDocuments)) {
					
				$array = array(
					"status" => '1',
					"msgStatus" => 'success',
					"message" => "$msgDocumentsFetchSuccess",
					"WSResponseCode" => "$WSCodeDocumentsFetchSuccess",
					"documents" => $arrayDocuments
				);
				
				echo json_encode($array, JSON_PRETTY_PRINT);	
			
			}else{

				$array = array(
					"status" => '0',
					"msgStatus" => 'fail',
					"message" => "$msgDocumentsFetchFail",
					"WSResponseCode" => "$WSCodeDocumentsFetchFail"
				);

				echo json_encode($array, JSON_PRETTY_PRINT);	

			}
	
		}else{

				$array = array(
					"status" => '0',
					"msgStatus" => 'fail',
					"message" => "$msgDocumentsFetchFail",
					"WSResponseCode" => "$WSCodeDocumentsFetchFail"
				);

				echo json_encode($array, JSON_PRETTY_PRINT);	
		}

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}

?>