<?

/** 
	Method that retrieve all the site based on entity id
*/
function GetAllEntitySite(){
		
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$entityId = $_POST['entityId'];
		$userId = $_POST['userId'];
		$lat = floatval($_POST['lat']);
		$lng = floatval($_POST['lng']);
		$time = GetCurrentTimeStamp();
		
		$distance = 200;

		$sql = "SELECT * FROM site 
				LEFT JOIN projectStatus ON site.projectStatusId = projectStatus.projectStatusId
				WHERE (acos(sin($lat) * sin(lat) + cos($lat) * cos(lat) * cos(lng - ($lng))) * 6371 <= $distance) 
				AND site.entityId = '$entityId' AND site.active = 1";

		$query = sbexeculteQuery($sql);
		$row = $query->rowCount();	
		
		if($row != 0){

			$i = 0;
			$loop = array();
			
			while($fetch = $query->fetch()){

				$loop[$i]['siteId'] = $fetch->siteId;
				$loop[$i]['entityId'] = $fetch->entityId;
				$loop[$i]['name'] = $fetch->name;
				$loop[$i]['lat'] = $fetch->lat;
				$loop[$i]['lng'] = $fetch->lng;
				$loop[$i]['radius'] = $fetch->radius;
				$loop[$i]['stNumber'] = $fetch->stNumber;
				$loop[$i]['stName'] = $fetch->stName;
				$loop[$i]['suburb'] = $fetch->suburb;
				$loop[$i]['state'] = $fetch->state;
				$loop[$i]['safeZone'] = $fetch->safeZone;
				$loop[$i]['blackspot'] = $fetch->blackspot;
				$loop[$i]['createdBy'] = $fetch->createdBy;
				$loop[$i]['active'] = $fetch->active;
				$loop[$i]['entityId'] = $fetch->entityId;
				$loop[$i]['projectName'] = $fetch->projectName;

				//Set project id
				if ($fetch->projectId) {
					$projectId = $fetch->projectId;
				}else{
					$projectId = "";
				}

				$loop[$i]['projectId'] = $projectId;

				$i++;
			}

			//Get sites that were joined
			$sqlJoin = "SELECT * FROM siteMap 
						INNER JOIN siteCode ON siteMap.siteId = siteCode.siteId 
						INNER JOIN site ON siteMap.siteId = site.siteId 
						LEFT JOIN projectStatus ON site.projectStatusId = projectStatus.projectStatusId
						WHERE siteMap.userId = '$userId' AND siteMap.entityId = '$entityId'";
			
			$queryJoin = sbexeculteQuery($sqlJoin);
			
			while($fetch = $queryJoin->fetch()){
				
				//Check the time
				if ($fetch->accessEnd > $time || is_null($fetch->accessEnd)) {

					$loop[$i]['siteId'] = $fetch->siteId;
					$loop[$i]['entityId'] = $fetch->entityId;
					$loop[$i]['name'] = $fetch->name;
					$loop[$i]['lat'] = $fetch->lat;
					$loop[$i]['lng'] = $fetch->lng;
					$loop[$i]['radius'] = $fetch->radius;
					$loop[$i]['stNumber'] = $fetch->stNumber;
					$loop[$i]['stName'] = $fetch->stName;
					$loop[$i]['suburb'] = $fetch->suburb;
					$loop[$i]['state'] = $fetch->state;
					$loop[$i]['safeZone'] = $fetch->safeZone;
					$loop[$i]['blackspot'] = $fetch->blackspot;
					$loop[$i]['createdBy'] = $fetch->createdBy;
					$loop[$i]['active'] = $fetch->active;
					$loop[$i]['entityId'] = $fetch->entityId;
					$loop[$i]['projectName'] = $fetch->projectName;

					//Set project id
					if ($fetch->projectId) {
						$projectId = $fetch->projectId;
					}else{
						$projectId = "";
					}
					
					$loop[$i]['projectId'] = $projectId;
					
					$i++;

				}

			}

			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgSiteFetchSuccess",
				"WSResponseCode" => "$WSCodeSiteFetchSuccess",
				"site" => $loop
			);
			
			echo json_encode($array, JSON_PRETTY_PRINT);	
			
		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgSiteFetchFail",
				"WSResponseCode" => "$WSCodeSiteFetchFail"
			);

			echo json_encode($array, JSON_PRETTY_PRINT);	
		}

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}

/** 
	Method that retrieve all the site based on entity id
*/
function GetSiteById(){
		
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$siteId = floatval($_POST['siteId']);

		$sql = "SELECT * FROM site 
				LEFT JOIN projectStatus ON site.projectStatusId = projectStatus.projectStatusId
				WHERE siteId = '$siteId'";

		$query = sbexeculteQuery($sql);
		$row = $query->rowCount();	
		
		if($row != 0){

			$i = 0;
			$loop = array();
			
			while($fetch = $query->fetch()){

				$loop[$i]['siteId'] = $fetch->siteId;
				$loop[$i]['entityId'] = $fetch->entityId;
				$loop[$i]['name'] = $fetch->name;
				$loop[$i]['lat'] = $fetch->lat;
				$loop[$i]['lng'] = $fetch->lng;
				$loop[$i]['radius'] = $fetch->radius;
				$loop[$i]['stNumber'] = $fetch->stNumber;
				$loop[$i]['stName'] = $fetch->stName;
				$loop[$i]['suburb'] = $fetch->suburb;
				$loop[$i]['state'] = $fetch->state;
				$loop[$i]['safeZone'] = $fetch->safeZone;
				$loop[$i]['blackspot'] = $fetch->blackspot;
				$loop[$i]['createdBy'] = $fetch->createdBy;
				$loop[$i]['active'] = $fetch->active;
				$loop[$i]['entityId'] = $fetch->entityId;
				$loop[$i]['projectId'] = $fetch->projectId;
				$loop[$i]['projectName'] = $fetch->projectName;
				$i++;
			}

			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgSiteFetchSuccess",
				"WSResponseCode" => "$WSCodeSiteFetchSuccess",
				"site" => $loop
			);
			
			echo json_encode($array, JSON_PRETTY_PRINT);	
			
		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgSiteFetchFail",
				"WSResponseCode" => "$WSCodeSiteFetchFail"
			);

			echo json_encode($array, JSON_PRETTY_PRINT);	
		}

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}

/**
	Method tha create a new site
*/
function CreateSite(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$data = array(
			"name" => $_POST['name'],
			"lat" => $_POST['lat'],
			"lng" => $_POST['lng'],
			"radius" => $_POST['radius'],
			"stNumber" => $_POST['stNumber'],
			"stName" => $_POST['stName'],
			"suburb" => $_POST['suburb'],
			"state" => $_POST['state'],
			"postcode" => $_POST['postcode'],
			"country" => $_POST['country'],
			"safeZone" => $_POST['safeZone'],
			"blackspot" => $_POST['blackspot'],
			"createdBy" => $_POST['userId'],
			"entityId" => $_POST['entityId'],
			"projectStatusId" => $_POST['projectId'],
			"active" => 1
		);
		
		$canCreate = false;

		$sqlEntity = "	SELECT * FROM setting 
						INNER JOIN package ON setting.packageId = package.pkgId 
						WHERE setting.entityId = ".$data['entityId']."";

		$queryEntity = sbexeculteQuery($sqlEntity);
		$fetch = $queryEntity->fetch();

		//Validate is entity has permission to create another site
		if ($fetch->siteLimit == null || EntitySiteLimit($data['entityId'],$fetch->siteLimit)) {
			
			$canCreate = true;

		}else{

			//check entity auto update
			if ($fetch->autoUpgrade == 1) {
				
				//Upgrade teh entity
				UpgradeEntity($data['entityId'],$credential['userId'],$fetch->pkgOrder + 1);
				
				//Save log to describe what happen
				WriteToAccountLog($data['entityId'],$credential['userId'],$logEntityUpdateBySite);

				$canCreate = true;
			}

		}

		if ($canCreate) {

			//Save site
			$sqlSave = "INSERT INTO site (name,lat,lng,radius,stNumber,stName,suburb,state,postcode,country,safeZone,blackspot,createdBy,entityId,projectStatusId,active) 
						VALUES (:name,:lat,:lng,:radius,:stNumber,:stName,:suburb,:state,:postcode,:country,:safeZone,:blackspot,:createdBy,:entityId,:projectStatusId,:active)";
			$siteId = sbexeculteQueryWithDataReturnId($sqlSave,$data);

			if ($siteId) {

				$array = array(
					"status" => '1',
					"msgStatus" => 'success',
					"message" => "$msgSiteCreateSuccess",
					"WSResponseCode" => "$WSCodeSiteCreateSuccess",
					"siteId" => $siteId
				);		

			}else{

				$array = array(
					"status" => '0',
					"msgStatus" => 'fail',
					"message" => "$msgSiteCreateFail",
					"WSResponseCode" => "$WSCodeSiteCreateFail"
				);
			}			

		}else{

			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgSiteCreateFailUpgrade",
				"WSResponseCode" => "$WSCodeSiteCreateFailUpgrade"
			);

		}
		
		echo json_encode($array, JSON_PRETTY_PRINT);

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}



/**
	Method that update site detail 
*/
function UpdateSite(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {
	
		$data = array(
			"siteId" => $_POST['siteId'],
			"name" => $_POST['name'],
			"lat" => $_POST['lat'],
			"lng" => $_POST['lng'],
			"radius" => $_POST['radius'],
			"stNumber" => $_POST['stNumber'],
			"stName" => $_POST['stName'],
			"suburb" => $_POST['suburb'],
			"state" => $_POST['state'],
			"safeZone" => $_POST['safeZone'],
			"blackspot" => $_POST['blackspot'],
			"projectId" => $_POST['projectId']
		);
		
		$sql = "UPDATE site SET name = :name, lat = :lat, lng = :lng, radius = :radius, stNumber = :stNumber, stName = :stName, suburb = :suburb, state = :state, safeZone = :safeZone, blackspot = :blackspot, projectStatusId = :projectId 
				WHERE siteId = :siteId";

		$query = sbexeculteQueryWithData($sql,$data);

		if($query){

			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgSiteUpdateSuccess",
				"WSResponseCode" => "$WSCodeSiteUpdateSuccess"
			);

		}else{

			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgSiteUpdateFail",
				"WSResponseCode" => "$WSCodeSiteUpdateFail"
			);
		}	

		echo json_encode($array, JSON_PRETTY_PRINT);

	}else{
		
		$array = array(
			"status" => 'fail',
			"message" => '$msgLoginAccessDenied'
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}

/**
	Method that joing a site
*/
function JoinSite(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$code = $_POST['code'];
		$userId = $_POST['userId'];
		$entityId = $_POST['entityId'];
		$time = GetCurrentTimeStamp();
		
		//Check for the site code
		$sql = "SELECT * FROM siteCode 
				INNER JOIN site on siteCode.siteId = site.siteId 
				WHERE siteCode.code = '$code'";

		$query = sbexeculteQuery($sql);
		$row = $query->rowCount();

		if ($row != 0) {
						
			$fetch = $query->fetch();

			if ($fetch->entityId != $entityId) {
				
				//Check the time
				if ($fetch->accessEnd > $time || is_null($fetch->accessEnd)) {

					//insert into siteMap table the grant access to user if it doesnt yet exist
					SetUserForSite($userId,$fetch->siteId,$fetch->siteCodeId,$entityId);

					$loop = array();

					$loop['siteId'] = $fetch->siteId;
					$loop['entityId'] = $fetch->entityId;
					$loop['name'] = $fetch->name;
					$loop['lat'] = $fetch->lat;
					$loop['lng'] = $fetch->lng;
					$loop['radius'] = $fetch->radius;
					$loop['stNumber'] = $fetch->stNumber;
					$loop['stName'] = $fetch->stName;
					$loop['suburb'] = $fetch->suburb;
					$loop['state'] = $fetch->state;
					$loop['safeZone'] = $fetch->safeZone;
					$loop['blackspot'] = $fetch->blackspot;
					$loop['createdBy'] = $fetch->createdBy;
					$loop['active'] = $fetch->active;
					$loop['entityId'] = $fetch->entityId;

					$array = array(
						"status" => '1',
						"msgStatus" => 'success',
						"message" => "$msgSiteJoinSuccess",
						"WSResponseCode" => "$WSCodeSiteJoinSuccess",
						"site" => $loop
					);
				
					echo json_encode($array, JSON_PRETTY_PRINT);	
			
				}else{
					
					$array = array(
						"status" => '0',
						"msgStatus" => 'fail',
						"message" => "$msgSiteJoinExpired",
						"WSResponseCode" => "$WSCodeSiteJoinExpired"
					);

					echo json_encode($array, JSON_PRETTY_PRINT);
				}

			}else{
				$array = array(
					"status" => '0',
					"msgStatus" => 'fail',
					"message" => "$msgSiteJoinSameEntity",
					"WSResponseCode" => "$WSCodeSiteJoinSameEntity"
				);

				echo json_encode($array, JSON_PRETTY_PRINT);
			}
			
		}else{

			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgSiteJoinNoMatch",
				"WSResponseCode" => "$WSCodeSiteJoinNoMatch"
			);

			echo json_encode($array, JSON_PRETTY_PRINT);
		}

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}

/**
	Method that deactivate site detail 
*/
function DeleteSite(){
	
	require 'lang.php';
	require "WSResponseCode.php";
	
	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$siteId = $_POST['siteId'];
		
		$sql = "UPDATE site SET active = 0 WHERE siteId = '$siteId'";

		$query = sbexeculteQuery($sql);
	
		if($query){

			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgSiteDeleteSuccess",
				"WSResponseCode" => "$WSCodeSiteDeleteSuccess"
			);

		}else{

			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => '$msgSiteDeleteFail',
				"WSResponseCode" => "$WSCodeSiteDeleteFail"
			);

		}	

		echo json_encode($array, JSON_PRETTY_PRINT);

	}else{
		
		$array = array(
			"status" => 'fail',
			"message" => '$msgLoginAccessDenied'
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}

/** 
	Method that retrieve all emergency numbers for site
*/
function GetSiteEmergency($entityId,$siteId){
				
	$sql = "SELECT *,emergency.name as placeName FROM emergency 
			LEFT JOIN emergencyType ON emergency.emergencyTypeId = emergencyType.emergencyTypeId 
			WHERE ((emergency.refTypeId = '2' AND emergency.refId = '$siteId')  OR (emergency.refTypeId = '3' AND emergency.refId = '$entityId')) 
			AND emergency.active = 1";

	$query = sbexeculteQuery($sql);
	$row = $query->rowCount();
	
	$emergency = array();

	if($row != 0){			
		$i = 0;
		
		while($fetch = $query->fetch()){
			$emergency[$i]['name'] = $fetch->name;
			$emergency[$i]['placeName'] = $fetch->placeName;
			$emergency[$i]['phone'] = $fetch->phone;
			$emergency[$i]['address'] = $fetch->address;
			$emergency[$i]['priority'] = $fetch->priority;
			$emergency[$i]['type'] = $fetch->emergencyTypeId;
			$emergency[$i]['lat'] = $fetch->lat;
			$emergency[$i]['lng'] = $fetch->lng;
			$i++;
		}
		
	}

	return $emergency;

}

/** 
	Method that retrieve all emergency numbers for site
*/
function GetUserEmergencyLeaders($entityId,$userId){
				
	$sql = "SELECT * FROM reportTo 
			INNER JOIN user ON reportTo.leaderId = user.userId 
			WHERE followerId = '$userId' AND permission = 2 AND entityId = '$entityId' AND (user.active = 1 AND reportTo.active = 1)
			GROUP BY reportTo.leaderId";

	$query = sbexeculteQuery($sql);
	$row = $query->rowCount();
	
	$teamEmergency = array();

	if($row != 0){			
		
		$i = 0;
		
		while($fetch = $query->fetch()){
			$teamEmergency[$i]['userId'] = $fetch->leaderId;
			$teamEmergency[$i]['name'] = $fetch->firstName . " " .$fetch->lastName;
			$teamEmergency[$i]['email'] = $fetch->email;
			$teamEmergency[$i]['mobile'] = $fetch->mobile;
			$i++;
		}
		
	}

	return $teamEmergency;

}


/**
	Method to create hazard actopm
*/
function CreateSiteEmergency() {

	require_once 'lang.php';
	require_once "WSResponseCode.php";

	$inputJSON = file_get_contents('php://input');
	$arrayEmergency = json_decode($inputJSON, TRUE);	

	$credential = array(
						"userId" => $arrayEmergency['userId'],
						"token" => $arrayEmergency['token']
						);
	
	//Validate user access
	if (ValidateAccess($credential)) {

		//Insert into Answers table
		foreach ($arrayEmergency['emergency'] as $obj) {
			
			$data = array(
					"emergencyTypeId" => $obj['type'],
					"refTypeId" => "2",
					"refId" => $arrayEmergency['siteId'],
					"name" => $obj['name'],
					"phone" => $obj['phoneNumber'],
					"priority" => "1",
					"entityId" => $arrayEmergency['entityId'],
					"lat" => $obj['lat'],
					"lng" => $obj['lng']
					);

			$sql = "INSERT INTO emergency (emergencyTypeId,refTypeId,refId,name,phone,priority,entityId,lat,lng) 
					VALUES (:emergencyTypeId,:refTypeId,:refId,:name,:phone,:priority,:entityId,:lat,:lng)";
			$saved = sbexeculteQueryWithDataReturnId($sql,$data);

		}
		
		if ($saved) {
			
			$array = array(
					"status" => '1',
					"msgStatus" => 'success',
					"message" => "$msgSiteEmergencyCreateSuccess",
					"WSResponseCode" => "$WSCodeSiteEmergencyCreateSuccess"
				);

				echo json_encode($array, JSON_PRETTY_PRINT);

		}else{

			$array = array(
					"status" => '0',
					"msgStatus" => 'fail',
					"message" => "$msgSiteEmergencyCreateFail",
					"WSResponseCode" => "$WSCodeSiteEmergencyCreateFail"
				);

				echo json_encode($array, JSON_PRETTY_PRINT);

		}
		
	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied",
			"sending" => $inputJSON
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}

}

/** 
	Method that retrieve the site statistics
*/
function GetSiteLog(){
		
	require 'lang.php';
	require "WSResponseCode.php";
	
	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$siteId = $_POST['siteId'];

		if (isset($_POST['checkinId'])) {

			$checkinIdFrom = $_POST['checkinId'];

			//Get older
			$sql = "SELECT * FROM checkin AS ck
					INNER JOIN user AS u ON ck.userId = u.userId
					WHERE ck.siteId = '$siteId' AND ck.checkinId < '$checkinIdFrom' AND ck.checkinTypeId = 1
					ORDER BY ck.checkinId DESC
					LIMIT 20";
		}else{

			//Get last 20 people
			$sql = "SELECT * FROM checkin AS ck
					INNER JOIN user AS u ON ck.userId = u.userId
					WHERE ck.siteId = '$siteId' AND ck.checkinTypeId = 1
					ORDER BY ck.checkinId DESC
					LIMIT 20";

		}
		
		$query = sbexeculteQuery($sql);
		$row = $query->rowCount();
		
		if($row != 0){			
			$i = 0;
			$loop = array();
			
			while($fetch = $query->fetch()){
				$loop[$i]['checkinId'] = $fetch->checkinId;
				$loop[$i]['time'] = $fetch->time;
				$loop[$i]['timezone'] = $fetch->timezone;
				$loop[$i]['checkinTypeId'] = $fetch->checkinTypeId;
				$loop[$i]['checkinCategoryId'] = $fetch->checkinCategoryId;
				$loop[$i]['userEntityId'] = $fetch->userEntityId;
				$loop[$i]['userId'] = $fetch->userId;
				$loop[$i]['firstName'] = $fetch->firstName;
				$loop[$i]['lastName'] = $fetch->lastName;
				$loop[$i]['email'] = $fetch->email;
				$loop[$i]['mobile'] = $fetch->mobile;
				$loop[$i]['status'] = $fetch->status;
				
				//Get the user length on site
				$loop[$i]['length'] = getUserLengthForSite($fetch);

				$i++;
			}

			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgSiteLogFetchSuccess",
				"WSResponseCode" => "$WSCodeSiteLogFetchSuccess",
				"siteLog" => $loop
			);
			
			echo json_encode($array, JSON_PRETTY_PRINT);	
			
		}else{
			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgSiteLogFetchNoLog",
				"WSResponseCode" => "$WSCodeSiteLogFetchNoLog"
			);

			echo json_encode($array, JSON_PRETTY_PRINT);	
		}

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}

}

/** 
	Fetch all site notes
*/
function SiteNote(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$siteId = $_POST['siteId'];
		
		$sql = "SELECT *,checkinNote.time as checkinNoteTime, checkinNote.timezone as checkinNoteTimezone FROM checkinNote
		    	INNER JOIN checkin ON checkinNote.checkinId = checkin.checkinId
		      	INNER JOIN site ON checkin.siteId = site.siteId
		      	INNER JOIN user ON checkin.userId = user.userId
		      	WHERE checkin.siteId = '$siteId' AND checkinNote.active = 1 AND checkinNote.isNote = 1
		      	ORDER BY checkinNote.time DESC";
	
		$query = sbexeculteQuery($sql);
		$row = $query->rowCount();

		if ($row != 0) {

            $i = 0;
            $loop = array();

            while($fetch = $query->fetch()){

            	//CheckinNote information
                $loop[$i]['checkinNoteId'] = $fetch->checkinId;
                $loop[$i]['checkinId'] = $fetch->checkinId;
                $loop[$i]['note'] = $fetch->note;
                $loop[$i]['time'] = $fetch->checkinNoteTime;
                $loop[$i]['timezone'] = $fetch->checkinNoteTimezone;
                
                //Site information
                $arraySiteInfo = array();
                $arraySiteInfo['siteId'] = $fetch->siteId;
                $arraySiteInfo['siteName'] = $fetch->name;
				$loop[$i]['siteInfo'] = $arraySiteInfo;

                //User information
                $arrayUserInfo = array();
                $arrayUserInfo['userId'] = $fetch->userId;
                $arrayUserInfo['name'] = $fetch->firstName . ' ' . $fetch->lastName;
                $arrayUserInfo['email'] = $fetch->email;
                $arrayUserInfo['mobile'] = $fetch->mobile;
                $loop[$i]['userInfo'] = $arrayUserInfo;

                $i++;
            }

            $array = array(
                "status" => '1',
                "msgStatus" => 'success',
                "message" => "$msgSiteNoteSuccess",
                "WSResponseCode" => "$WSCodeSiteNoteSuccess",
                "siteNote" => $loop
            );

            echo json_encode($array, JSON_PRETTY_PRINT);
		}else{

			$array = array(
	            "status" => '0',
	            "msgStatus" => 'fail',
	            "message" => "$msgSiteNoteNoNote",
	            "WSResponseCode" => "$WSCodeSiteNoteNoNote"
        	);

			echo json_encode($array, JSON_PRETTY_PRINT);
		}

    }else{

        $array = array(
            "status" => '0',
            "msgStatus" => 'fail',
            "message" => "$msgLoginAccessDenied",
            "WSResponseCode" => "$WSCodeLoginAccessDenied"
        );

        echo json_encode($array, JSON_PRETTY_PRINT);
    }
}

/**
Save note to checkinNote
 */
function CreateSiteNote(){

    require 'lang.php';
    require "WSResponseCode.php";

    $credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$userId = $_POST['userId'];

		$data = array(
		    "checkinId" => $_POST['checkinId'],
		    "note" => $_POST['note'],
			"time" => $_POST['ts'],
			"timezone" => $_POST['tz'],
			"isNote" => 1
		);

		$sqlExist = "SELECT * FROM checkinNote WHERE checkinId = ".$data['checkinId']." AND isNote = 1 ";
		$queryExist = sbexeculteQuery($sqlExist);
        $row = $queryExist->rowCount();

        if ($row == 0) {
            $sql = "INSERT INTO checkinNote (checkinId,note,time,timezone,isNote) VALUES (:checkinId,:note,:time,:timezone,:isNote)";
        }else{
            $sql = "UPDATE checkinNote SET note = :note, time = :time, timezone = :timezone, isNote = :isNote WHERE checkinId = :checkinId";
        }

        $query = sbexeculteQueryWithData($sql,$data);

        if ($query) {

            $array = array(
                "status" => '1',
                "msgStatus" => 'success',
                "message" => "$msgSiteNoteSaveSuccess",
                "WSResponseCode" => "$WSCodeSiteNoteSaveSuccess"
            );

        }else{

            $array = array(
                "status" => '0',
                "msgStatus" => "fail",
                "message" => "$msgSiteNoteSaveFail",
                "WSResponseCode" => "$WSCodeSiteNoteSaveFail"
            );

        }

    }else{

        $array = array(
            "status" => '0',
            "msgStatus" => 'fail',
            "message" => "$msgLoginAccessDenied",
            "WSResponseCode" => "$WSCodeLoginAccessDenied"
        );
    }

    echo json_encode($array, JSON_PRETTY_PRINT);
}

/** 
	Method get project status by projectId
*/
function GetAllProject(){

	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {
		
		$sql = "SELECT * FROM projectStatus WHERE active = 1 ORDER BY projectId";
		
		$query = sbexeculteQuery($sql);
		$row = $query->rowCount();
		
		if ($row != 0) {
			
			$loop = array();
			$i = 0;	

			while($fetch = $query->fetch()){
				$loop[$i]['projectStatusId'] = $fetch->projectStatusId;
				$loop[$i]['projectId'] = $fetch->projectId;
				$loop[$i]['projectName'] = $fetch->projectName;
				$loop[$i]['budget'] = $fetch->budget;
				$loop[$i]['completion'] = $fetch->completion;
				$loop[$i]['variation'] = $fetch->variation;
				$loop[$i]['manager'] = $fetch->manager;
				$loop[$i]['leftOnJob'] = $fetch->leftOnJob;
				$loop[$i]['active'] = $fetch->active;
				$i++;
			}

			$array = array(
				"status" => '1',
				"msgStatus" => "success",
				"message" => "$msgSiteProjectFetchSuccess",
				"WSResponseCode" => "$WSCodeSiteProjectFetchSuccess",
				"projectStatus" => $loop
			);
			
			echo json_encode($array, JSON_PRETTY_PRINT);		

		}else{

			$array = array(
				"status" => '0',
				"msgStatus" => "fail",
				"message" => "$msgSiteProjectFail",
				"WSResponseCode" => "$WSCodeSiteProjectFetchFail"
			);
			
			echo json_encode($array, JSON_PRETTY_PRINT);	
		}
		
			
	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}

}


?>