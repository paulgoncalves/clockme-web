<?php

/** 
Method that will checkin user
*/
function CheckIn(){

	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$success = $_POST['success'];
		
		$action = UserLastAction($credential['userId']);
		//If user last action is cheching or reportIn add a checkout
		if ($action && ($action->checkinTypeId == 1 || $action->checkinTypeId == 2)) {

			$dataCheckout = array(
				"time" => $_POST['ts'],
				"timezone" => $_POST['timezone'],
				"lat" => $_POST['lat'],
				"lng" => $_POST['lng'],
				"userId" => $_POST['userId'],
				"siteId" => $action->siteId,
				"checkinTypeId" => 1,
				"checkinCategoryId" => $_POST['checkinCategoryId'],
				"userEntityId" => $_POST['userEntityId'],
				"checkinBy" => $_POST['userId'],
				"batteryLevel" => $_POST['batteryLevel']
			);

			$sql = "INSERT INTO checkin (time,timezone,lat,lng,userId,siteId,checkinTypeId,checkinCategoryId,userEntityId,checkinBy,batteryLevel) 
					VALUES (:time,:timezone,:lat,:lng,:userId,:siteId,:checkinTypeId,:checkinCategoryId,:userEntityId,:checkinBy,:batteryLevel)";
			
			sbexeculteQueryWithData($sql,$dataCheckout);

		}

		//Validate if user has answer the questions correctly or if failed
		if ($success == 0) {
			
			$data = array(
				"time" => $_POST['ts'],
				"timezone" => $_POST['timezone'],
				"lat" => $_POST['lat'],
				"lng" => $_POST['lng'],
				"userId" => $_POST['userId'],
				"siteId" => $_POST['siteId'],
				"checkinTypeId" => 15,
				"checkinCategoryId" => $_POST['checkinCategoryId'],
				"userEntityId" => $_POST['userEntityId'],
				"checkinBy" => $_POST['userId'],
				"batteryLevel" => $_POST['batteryLevel']
			);

			//print_r($data);
			$sql = "INSERT INTO checkin (time,timezone,lat,lng,userId,siteId,checkinTypeId,checkinCategoryId,checkinBy,batteryLevel,userEntityId) 
					VALUES (:time,:timezone,:lat,:lng,:userId,:siteId,:checkinTypeId,:checkinCategoryId,:checkinBy,:batteryLevel,:userEntityId)";
			
			$query = sbexeculteQueryWithData($sql,$data);
			
		}else{

			$data = array(
				"time" => $_POST['ts'],
				"timezone" => $_POST['timezone'],
				"lat" => $_POST['lat'],
				"lng" => $_POST['lng'],
				"userId" => $_POST['userId'],
				"siteId" => $_POST['siteId'],
				"checkinTypeId" => 1,
				"checkinCategoryId" => $_POST['checkinCategoryId'],
				"userEntityId" => $_POST['userEntityId'],
				"checkinBy" => $_POST['userId'],
				"batteryLevel" => $_POST['batteryLevel']
			);

			$sql = "INSERT INTO checkin (time,timezone,lat,lng,userId,siteId,checkinTypeId,checkinCategoryId,checkinBy,batteryLevel,userEntityId) 
					VALUES (:time,:timezone,:lat,:lng,:userId,:siteId,:checkinTypeId,:checkinCategoryId,:checkinBy,:batteryLevel,:userEntityId)";
			
		}
		
		//Get checkin id
		$checkinId = sbexeculteQueryWithDataReturnId($sql,$data);

		if($checkinId){
			
			if ($success <> 0) {
				
				//Load required files
				require '../notification_v1/notification.php';
				require '../notification_v1/functions.php';

				//Update user status
				UpdateUserStatus($data['userId'],1);

				//Set resolved the pending push with category
				//2 = checkin
				UpdateActionPushStatus($data['userId'],$data['timezone'],2);

				//Get project status
				$projectStatus = GetProjectStatus($data['siteId']);
				$hazard = GetHazardBySiteId($data['siteId']);
				$emergency = GetSiteEmergency($data['userEntityId'],$data['siteId']);
				$leaders = GetUserEmergencyLeaders($data['userEntityId'],$data['userId']);

				if (count($projectStatus) != 0 && count($hazard) != 0) {
					
					$array = array(
						"status" => '1',
						"msgStatus" => 'success',
						"message" => "$msgActionCheckinSuccess",
						"WSResponseCode" => "$WSCodeActionCheckinSuccess",
						"checkinId" => "$checkinId",
						"emergency" => $emergency,
						"leaders" => $leaders,
						"projectStatus" => $projectStatus,
						"hazard" => $hazard
					);

				}else if(count($projectStatus) != 0){

					$array = array(
						"status" => '1',
						"msgStatus" => 'success',
						"message" => "$msgActionCheckinSuccess",
						"WSResponseCode" => "$WSCodeActionCheckinSuccess",
						"checkinId" => "$checkinId",
						"emergency" => $emergency,
						"leaders" => $leaders,
						"projectStatus" => $projectStatus
					);

				}else if(count($hazard) != 0){

					$array = array(
						"status" => '1',
						"msgStatus" => 'success',
						"message" => "$msgActionCheckinSuccess",
						"WSResponseCode" => "$WSCodeActionCheckinSuccess",
						"checkinId" => "$checkinId",
						"emergency" => $emergency,
						"leaders" => $leaders,
						"hazard" => $hazard
					);

				}else{
					$array = array(
						"status" => '1',
						"msgStatus" => 'success',
						"message" => "$msgActionCheckinSuccess",
						"WSResponseCode" => "$WSCodeActionCheckinSuccess",
						"checkinId" => "$checkinId",
						"emergency" => $emergency,
						"leaders" => $leaders,
					);	
				}

			}else{

				$array = array(
						"status" => '0',
						"msgStatus" => 'fail',
						"message" => "$msgActionCheckinFail",
						"WSResponseCode" => "$WSCodeActionCheckinFail",
						"checkinId" => "$checkinId"
					);
			}
			
			
		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => "fail",
				"message" => "$msgCheckinFail",
				"WSResponseCode" => "$WSCodeActionCheckinFail"
			);
		}	

		echo json_encode($array, JSON_PRETTY_PRINT);	
		
	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}

/** 
Method that will checkin user
*/
function ReportIn(){

	require 'lang.php';
	require "WSResponseCode.php";

	//Load required files
	require '../notification_v1/notification.php';
	require '../notification_v1/functions.php';

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$checkinId = $_POST['checkinId'];

		$data = array(
			"time" => $_POST['ts'],
			"timezone" => $_POST['timezone'],
			"lat" => $_POST['lat'],
			"lng" => $_POST['lng'],
			"userId" => $_POST['userId'],
			"siteId" => $_POST['siteId'],
			"checkinTypeId" => 2,
			"userEntityId" => $_POST['userEntityId'],
			"checkinBy" => $_POST['userId'],
			"batteryLevel" => $_POST['batteryLevel']
		);

		//Get last user checkin id in case the app is v0.4.1 b14 and bellow
		if (!$checkinId) {
			$sqlCheckin =  "SELECT * FROM checkin WHERE userId = ".$data['userId']." ORDER BY checkinId DESC LIMIT 1";
			$getCheckin = sbexeculteQuery($sqlCheckin);
			$fetchCheckin = $getCheckin->fetch();
			$checkinId = $fetchCheckin->checkinId;
		}

		$sql = "INSERT INTO checkin (time,timezone,lat,lng,userId,siteId,checkinTypeId,userEntityId,checkinBy,batteryLevel) 
				VALUES (:time,:timezone,:lat,:lng,:userId,:siteId,:checkinTypeId,:userEntityId,:checkinBy,:batteryLevel)";
		$reportInId = sbexeculteQueryWithDataReturnId($sql,$data);
		
		if($reportInId != 0){
						
			//Verify if user require send push to other users
			if (RequireNotification($data['userId'],$checkinId)) {
								
				$userDetail = GetUserInfo($data['userId']);
				//print_r($userDetail);
				$message = $userDetail->firstName . ' ' . $userDetail->lastName . " has reported in, your urgent attention is no longer required.";
				
				//Check and send push if user has been notified because of user late status
				PushUserReportin($userDetail,$message,$checkinId,$data['time'],$data['timezone'],$data['userEntityId']);

				//Set resolved the pending push with category
				//2 = checkin
				UpdatePushStatus_v1($data['userId'],$checkinId,$data['timezone'],2);
				//Set resolved the pending push with category 
				//6 = location track
				//UpdatePushStatus_v1($data['userId'],$checkinId,$data['timezone'],6);

			}
			
			//Update user status
			UpdateUserStatus($data['userId'],1);

			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgActionReportinSuccess",
				"reportInId" => "$reportInId",
				"WSResponseCode" => "$WSCodeActionReportinSuccess"
			);
		
		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => "fail",
				"message" => "$msgActionReportinFail",
				"WSResponseCode" => "$WSCodeActionReportinFail"
			);
		}	

		echo json_encode($array, JSON_PRETTY_PRINT);

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}

/** 
Method that will checkin user
*/
function CheckOut(){

	require 'lang.php';
	require "WSResponseCode.php";

	//Load required files
	require '../notification_v1/notification.php';
	require '../notification_v1/functions.php';

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$checkinId = $_POST['checkinId'];
		$done = $_POST['done'];
		
		$onRadius = $_POST['onRadius'];
		//Check if user is on radius
		if ($onRadius == 1) {
			$checkinTypeId = 3;
		}else{
			$checkinTypeId = 14;
		}

		$data = array(
			"time" => $_POST['ts'],
			"timezone" => $_POST['timezone'],
			"lat" => $_POST['lat'],
			"lng" => $_POST['lng'],
			"userId" => $_POST['userId'],
			"siteId" => $_POST['siteId'],
			"checkinTypeId" => $checkinTypeId,
			"userEntityId" => $_POST['userEntityId'],
			"checkinBy" => $_POST['userId'],
			"batteryLevel" => $_POST['batteryLevel'],
			"onRadius" => $_POST['onRadius']
		);
		
		//Get last user checkin id in case the app is v0.4.1 b14 and bellow
		if (!$checkinId) {
			$sqlCheckin =  "SELECT * FROM checkin WHERE userId = ".$data['userId']." ORDER BY checkinId DESC LIMIT 1";
			$getCheckin = sbexeculteQuery($sqlCheckin);
			$fetchCheckin = $getCheckin->fetch();
			$checkinId = $fetchCheckin->checkinId;
		}

		$sql = "INSERT INTO checkin (time,timezone,lat,lng,userId,siteId,checkinTypeId,userEntityId,checkinBy,batteryLevel,onRadius) 
				VALUES (:time,:timezone,:lat,:lng,:userId,:siteId,:checkinTypeId,:userEntityId,:checkinBy,:batteryLevel,:onRadius)";
		$query = sbexeculteQueryWithDataReturnId($sql,$data);

		if($query != 0){
			
			//Verify if user require send push to other users
			if (RequireNotification($data['userId'],$checkinId)) {
								
				$userDetail = GetUserInfo($data['userId']);
				$message = $userDetail->firstName . ' ' . $userDetail->lastName . " has checked out, your urgent attention is no longer required.";

				//Check and send push if user has been notified because of user late status
				PushUserCheckout($userDetail,$message,$checkinId,$data['time'],$data['timezone'],$data['userEntityId']);

				//Set resolved the pending push with category
				//2 = checkin
				UpdatePushStatus_v1($data['userId'],$checkinId,$data['timezone'],2);
				//Set resolved the pending push with category 
				//6 = location track
				UpdatePushStatus_v1($data['userId'],$checkinId,$data['timezone'],6);


			}

			if ($done == 1) {
				//Update user status
				UpdateUserStatus($data['userId'],0);
			}else{
				//Update user status
				UpdateUserStatus($data['userId'],8);
			}
			

			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgActionCheckoutSuccess",
				"WSResponseCode" => "$WSCodeActionCheckoutSuccess"
			);
		
		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => "fail",
				"message" => "$msgActionCheckoutFail",
				"WSResponseCode" => "$WSCodeActionCheckoutFail"
			);
		}	

		echo json_encode($array, JSON_PRETTY_PRINT);	

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}

/** 
Method that will checkin user
*/
function SendPulse(){

	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$data = array(
			"time" => $_POST['ts'],
			"timezone" => $_POST['timezone'],
			"lat" => $_POST['lat'],
			"lng" => $_POST['lng'],
			"userId" => $_POST['userId'],
			"checkinTypeId" => $_POST['checkinTypeId'],
			"userEntityId" => $_POST['userEntityId'],
			"checkinBy" => $_POST['userId'],
			"batteryLevel" => $_POST['batteryLevel']
		);
		
		$sql = "INSERT INTO checkin (time,timezone,lat,lng,userId,checkinTypeId,userEntityId,checkinBy,batteryLevel) 
				VALUES (:time,:timezone,:lat,:lng,:userId,:checkinTypeId,:userEntityId,:checkinBy,:batteryLevel)";
		$query = sbexeculteQueryWithDataReturnId($sql,$data);
		
		if($query){
			
			//Update user status
			//If pulse typeId is equal to 12 "Done Working" it update user status to inactive
			//8 when send from pulse menu
			//12 when send form action request view
			if ($data['checkinTypeId'] == 12 || $data['checkinTypeId'] == 8) {
				$changeStatus = UpdateUserStatus($data['userId'],0);	
			}else{
				$changeStatus = UpdateUserStatus($data['userId'],8);	
			}
			
			if ($changeStatus) {
				$array = array(
					"status" => '1',
					"msgStatus" => 'success',
					"message" => "$msgActionPulseSuccess",
					"WSResponseCode" => "$WSCodeActionPulseSuccess"
				);	
			}else{
				$array = array(
					"status" => '0',
					"msgStatus" => 'fail',
					"message" => "$msgActionPulseFail",
					"WSResponseCode" => "$WSCodeActionPulseFail"
				);	
			}
			
		
		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => "fail",
				"message" => "$msgActionPulseFail",
				"WSResponseCode" => "$WSCodeActionPulseFail"
			);
		}	

		echo json_encode($array, JSON_PRETTY_PRINT);	
		
	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
	
}

/** 
	Method to report user safe
*/
function UserSafe(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$data = array(
			"time" => $_POST['ts'],
			"timezone" => $_POST['timezone'],
			"lat" => $_POST['lat'],
			"lng" => $_POST['lng'],
			"userActionId" => $_POST['userActionId'],
			"checkinTypeId" => $_POST['action'],
			"userId" => $_POST['userId'],
			"batteryLevel" => $_POST['batteryLevel']
		);

		//Check if user still with status different then 1 to continue
		$fetch = GetStatusByUser($data['userActionId']);
		$userName = $fetch->firstName ." ". $fetch->lastName;
		
		$data["userEntityId"] = $fetch->entityId;
		$data["siteId"] = $fetch->siteId;

		if ($fetch->status == 1 || $fetch->status == 0) {
			
			$description = $_POST['description'];

			//Add note to checkinNote
			AddCheckinNote($fetch->checkinId,$description,$data['time'],$data['timezone']);
						
			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$userName $msgActionUserIsSafe",
				"WSResponseCode" => "$WSCodeActionUserIsSafe"
			);	

		}else {

			//Load required files
			require '../notification_v1/notification.php';
			require '../notification_v1/functions.php';

			//Update if user status is more then 2 and less then 6
			//On those status a leader is notified because user is in danger
			if ($fetch->status > 1 && $fetch->status < 7) {
				//User is safe, change user status to stop scalating the notification
				if ($data['checkinTypeId'] == 10) {
					//Update user status
					UpdateUserStatus($data['userActionId'],7);
				}
			}

			//CheckinTypeId = 9 means "user safe"
			$sql = "INSERT INTO checkin (time,timezone,lat,lng,userId,siteId,checkinTypeId,userEntityId,checkinBy,batteryLevel) 
					VALUES (:time,:timezone,:lat,:lng,:userActionId,:siteId,:checkinTypeId,:userEntityId,:userId,:batteryLevel)";
			$checkinId = sbexeculteQueryWithDataReturnId($sql,$data);

			//Verify if user require send push to other users
			if (RequireNotification($data['userActionId'],$fetch->checkinId)) {
								
				$userDetail = GetUserInfo($data['userActionId']);
				$msg = $userDetail->firstName . ' ' . $userDetail->lastName . " team leader had confirmed he is safe, your urgent attention is no longer required.";

				//Check and send push if user has been notified because of user late status
				PushUserIsSafe($userDetail,$msg,$fetch->checkinId,$data['time'],$data['timezone'],$fetch->userEntityId);

				//Set resolved the pending push with category
				//2 = checkin
				UpdatePushStatus_v1($data['userActionId'],$fetch->checkinId,$data['timezone'],2);
				//Set resolved the pending push with category 
				//6 = location track
				UpdatePushStatus_v1($data['userActionId'],$fetch->checkinId,$data['timezone'],6);

			}

			if (is_numeric($checkinId)) {
				
				$description = $_POST['description'];

				//Add note to checkinNote
				AddCheckinNote($checkinId,$description,$data['time'],$data['timezone']);

				$array = array(
					"status" => '1',
					"msgStatus" => 'success',
					"message" => "$msgActionUserSafeSuccess",
					"WSResponseCode" => "$WSCodeActionUserSafeSuccess"
				);
			
			}else{

				$array = array(
					"status" => '0',
					"msgStatus" => 'fail',
					"message" => "$userName $msgActionUserSafeFail",
					"WSResponseCode" => "$WSCodeActionUserSafeFail"
				);
			
			}

		}
		
	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	}

	echo json_encode($array, JSON_PRETTY_PRINT);

}

?>
