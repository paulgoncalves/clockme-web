<? 

/**
	Method get user log.
	Limiting the quantity of data retrieve to 10.
*/
function GetUserLog() {

	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {
	
		$sql = "SELECT * FROM checkin
				INNER JOIN user ON checkin.userId = user.userId
				INNER JOIN checkinType ON checkin.checkinTypeId = checkinType.checkinTypeId 
				LEFT JOIN site on checkin.siteId = site.siteId 
				WHERE checkin.userId = ".$credential['userId']." 
				ORDER BY checkinId
				DESC LIMIT 10";

		$query = sbexeculteQuery($sql);
		$row = $query->rowCount();
		
		if($row != 0){

			$i = 0;
			$loop = array();

			while($fetch = $query->fetch()){

				if ($fetch->isPulse == 1) {
					$siteName = "Pulse";
					$siteId = "";
				}else{
					$siteName = $fetch->name;
					$siteId = $fetch->siteId;
				}
				
				$loop[$i]['checkinId'] = $fetch->checkinId;
				$loop[$i]['isPulse'] = $fetch->isPulse;
				$loop[$i]['userId'] = $fetch->userId;
				$loop[$i]['time'] = $fetch->time;
				$loop[$i]['timezone'] = $fetch->timezone;
				$loop[$i]['lat'] = $fetch->lat;
				$loop[$i]['lng'] = $fetch->lng;
				$loop[$i]['siteId'] = $siteId;
				$loop[$i]['desc'] = $fetch->desc;
				$loop[$i]['checkinTypeId'] = $fetch->checkinTypeId;
				$loop[$i]['name'] = $siteName;
				$loop[$i]['status'] = $fetch->status;
				$loop[$i]['onRadius'] = $fetch->onRadius;

				$i++;
			}
			
				$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgLogUserSuccess",
				"WSResponseCode" => "$WSCodeLogUserSuccess",
				"log" => $loop
			);

			echo json_encode($array, JSON_PRETTY_PRINT);

		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgLogUserNoLog",
				"WSResponseCode" => "$WSCodeLogNoLog"
			);
			
			echo json_encode($array, JSON_PRETTY_PRINT);
		}

		if (!$query) {
			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgLogUserFail",
				"WSResponseCode" => "$WSCodeLogUserFail"
			);
			
			echo json_encode($array, JSON_PRETTY_PRINT);	
		}

		
	}else{

		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}

/**
	Method get user log.
	Limiting the quantity of data retrieve to 10.
*/
function GetUserLogByEntity() {

	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"userId" => $_POST['userId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$entityId = $_POST['entityId'];
		$userLog = $_POST['userLog'];
		
		$sql = "SELECT * FROM checkin
				INNER JOIN user ON checkin.userId = user.userId
				INNER JOIN checkinType ON checkin.checkinTypeId = checkinType.checkinTypeId 
				LEFT JOIN site on checkin.siteId = site.siteId 
				WHERE checkin.userId = '$userLog' AND checkin.userEntityId = '$entityId' 
				ORDER BY checkinId 
				DESC LIMIT 10";

		$query = sbexeculteQuery($sql);
		$row = $query->rowCount();
		
		if($row != 0){

			$i = 0;
			$loop = array();

			while($fetch = $query->fetch()){
				
				if ($fetch->isPulse == 1) {
					$siteName = "Pulse";
					$siteId = "";
				}else{
					$siteName = $fetch->name;
					$siteId = $fetch->siteId;
				}
				
				$loop[$i]['checkinId'] = $fetch->checkinId;
				$loop[$i]['isPulse'] = $fetch->isPulse;
				$loop[$i]['userId'] = $fetch->userId;
				$loop[$i]['time'] = $fetch->time;
				$loop[$i]['timezone'] = $fetch->timezone;
				$loop[$i]['lat'] = $fetch->lat;
				$loop[$i]['lng'] = $fetch->lng;
				$loop[$i]['siteId'] = $siteId;
				$loop[$i]['desc'] = $fetch->desc;
				$loop[$i]['checkinTypeId'] = $fetch->checkinTypeId;
				$loop[$i]['name'] = $siteName;
				$loop[$i]['status'] = $fetch->status;
				$loop[$i]['onRadius'] = $fetch->onRadius;
				
				$i++;
			}
			
			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgLogUserSuccess",
				"WSResponseCode" => "$WSCodeLogUserSuccess",
				"log" => $loop
			);

			echo json_encode($array, JSON_PRETTY_PRINT);

		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgLogUserNoLog",
				"WSResponseCode" => "$WSCodeLogNoLog"
			);
			
			echo json_encode($array, JSON_PRETTY_PRINT);
		}

		if (!$query) {
			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgLogUserFail",
				"WSResponseCode" => "$WSCodeLogUserFail"
			);
			
			echo json_encode($array, JSON_PRETTY_PRINT);	
		}

		
	}else{

		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
		echo json_encode($array, JSON_PRETTY_PRINT);

	}
}

/**
	Method to write a log to database to save when entity is upgraded
*/
function WriteToAccountLog($entityId,$userId,$message){
	
	//Get timeStamp from function.php
	$time = GetCurrentTimeStamp();

	//Save log to describe what happen
	$data = array(
		"entityId" => $entityId,
		"time" => $time,
		"message" => $message,
		"userId" => $userId
		);

	$sql = "INSERT INTO accountLog (entityId,time,description,userId) VALUES(:entityId,:time,:message,:userId)";
	sbexeculteQueryWithData($sql,$data);
	
}

/**
	Method to save notification sent
*/
function WriteLog(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	$message = $_POST['message'];
	$file = 'log.txt';
	// Open the file to get existing content
	$current = file_get_contents($file);
	// Write the contents back to the file
	$current .= "$message \n";
	file_put_contents($file, $current);

	$array = array(
			"status" => '1',
			"msgStatus" => 'success',
			"message" => "$logSuccess",
			"WSResponseCode" => "$WSCodeLogSuccess"
		);
	
	echo json_encode($array, JSON_PRETTY_PRINT);
}

/**
	Method to save notification sent
*/
function WriteToActionLog(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	$data = array(
			"actionCode" => $_POST['actionCode'],
			"actionFrom" => $_POST['actionFrom'],
			"actionTo" => $_POST['actionTo'],
			"tableName" => $_POST['tableName'],
			"time" => GetCurrentTimeStamp(),
			"timezone" => $_POST['tz'],
			"note" => $_POST['note'],
			"entityId" => $_POST['entityId']
		);

	$sql = "INSERT INTO actionLog (actionCode,actionFrom,actionTo,tableName,time,timezone,note,entityId) 
			VALUES(:actionCode,:actionFrom,:actionTo,:tableName,:time,:timezone,:note,:entityId)";
	$query = sbexeculteQueryWithData($sql,$data);
	
	if ($query) {
	
		$array = array(
			"status" => '1',
			"msgStatus" => 'success',
			"message" => "$logSuccess",
			"WSResponseCode" => "$WSCodeLogSuccess"
		);		

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$logFail",
			"WSResponseCode" => "$WSCodeLogFail"
		);

	}
	
	echo json_encode($array, JSON_PRETTY_PRINT);
}


/**
	Method to save notification sent
*/
function LogString($message){
	
	require 'lang.php';

	$file = 'log.txt';
	// Open the file to get existing content
	$current = file_get_contents($file);
	// Write the contents back to the file
	$current .= "$message \n";
	file_put_contents($file, $current);

}

?>